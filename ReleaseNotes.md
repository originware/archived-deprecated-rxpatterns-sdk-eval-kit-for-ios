---
# Release Notes For The  [Originware](http://www.originware.com)   RxPatterns SDK Evaluation Kit v1.2 For iOS.
---

Highlights in this version:

* Updates for the Swift 3.1.1 language. 
* The @inline attributes have been removed as Swift still has not formalised inline declarations.

# Release Notes For The  [Originware](http://www.originware.com)   RxPatterns SDK Evaluation Kit v1.1 For iOS.
---

Highlights in this version:

* RxDevice technology integration with the Eval Kit POI Demo app.
* The app JSON handler has been migrated to Originware's open source CrispJSON parser.

### RxDevice Integration:

This integration is intended to demonstrate the effectiveness of RxDevice in both high level operational management and low level device control. The POI Demonstation iPad app source has been modified for: 
  
    * RxDevice management of high level operational states.  
       
       The app now has three operational states: StartUp, Presentation and Application. Startup handles pure initialisation, Presentation handles graphical operation and Application handles pure application business logic.
       
    
	* RxDevice handling Protocol Stacks: The POI Portal network handling code has been re-engineered as a protocol stack. 
    
    	Each component of the stack is a Service design with HTTP control implemented as an output RxDevice (the HTTPService) and the upstream components are implemented as RxDevOperators. The operators include: Google Places protocol handling (the GooglePlacesService) and a POI request management service (the POIPortalService).
    	
    * Single device control:  The GPS location Service is implemented as an input RxDevice.
    
    	Note: this was implemented in version 1.0 for demonstration in this version.	
    	
This release coincides with the publication of the [RxDevice For Apps Product Brief](http://www.originware.com/doc/RxDevice%20For%20Apps%20Product%20Brief.pdf)    	

---    
    
# Release Notes For The  [Originware](http://www.originware.com)   RxPatterns SDK Evaluation Kit v1.0 For iOS.
---
Highlights in this version:

* Migration to the Swift 3.0 language and Xcode 8.0.
* The RxDevice concept has been formalised into the SDK. (RxDevice supports bi-way communications between sources, operators and observers (i.e. devices). This is intended for complex device control.) 
* RxDevice has only been partially integrated into the Evaluation kit. Future versions will have the Demo App fullying utilising RxDevice for its ViewController to View and HTTP interactions.   


### Changes to the RxPatterns SDK:

    * Migration to the Swift 3.0 language, including:    
     
        * Migration to the Swift 3.0 version of Libdispatch
    	* Major renaming of methods to be more inline with the new Swift 3.0 naming guidelines.
	    * Overhaul of Swift language class permissions. 
	    
    * Formalisation of the RxDevice concept into the SDK. 
### Changes to the RxFoundation Library:

    * Migration to Swift 3.0.
    * Implementation of the selectMany observable and unit test.
    * Additonal Unit Tests.
    
### Changes to the Demo App:

    * Migration to Swift 3.0.
    * Integration of RxDevice to the Location Adapter.
   
-----
# Release Notes For The  [Originware](http://www.originware.com)   RxPatterns SDK Evaluation Kit v0.92 For iOS.
---
Highlights in the version:

* Updates swift compatibility to version 2.2
* Introduces the **RxDevice Platform**: A device management platform for IOT which employs **RxPatterns SDK**.
* Introduces some new concepts to the **RxPatterns SDK** (The **switch** design pattern, **processing notifiers** and **manipulators** using the **<-** operator).

### 1. Evaluation kit dependencies.
The Evaluation Kit requires Xcode 7.3 and conforms to the Swift 2.2 language version.

### 2. Changes from version 0.91 to version 0.92

* RxFoundation changes:

  * Rename the existing library for standard sources, observables and observers library to the **RxFoundationLibrary** which is resident in the **RxPatternsLib**
  * Introduce the Device Platform** into the **RxPatternsLib**
  * The Switch design pattern was extracted from the previous Gate design pattern to differentiate between situations where the all the input notification queues are required to assess output notifications (gating) and where only specific single queues are required to assess output notifications (switching).
  * The Processing Notifiers concept introduced. These are queue based Notifiers which check if their targets can process a notifiication and if not, queue them for later processing.
  * Scheduling Processing Notifiers added. These receive schedulable item notifications and process (emit to targets) at their scheduled times.
  * RxWindow and RxTimer refactored to be controlled by manipulator-associated-enums via the <- operator.
  * Object tags changed to be from right to left in flow with a '/' separator rather than '.'.
  * RxRelTime introduced to cater for a lighter-weight time object (typealiased from Double).
  * Tracing uses RxRelTime rather than RxTime.
  * RxAtomicObject (class based) introduced to get around struct problems of RxAtomic being copied in Arrays and Dictionaries.
  * Autoincrement and Autodecrement (sadly) removed.
  * More RxWindow unit tests added.
 <p>
  
* RxDevice Platform introduction:
	* Introduces the platform, sample code that uses the platform will come in the next version.	  

### 3. Work in progress, to be completed for version 1.0

* RxFoundation Lib:

   * Implement the remaining Observables:
 
     * selectMany
     * join
     * groupJoin
   
   * Implement the remaining RxPatternsLib Unit Tests.
   * Determine an appropriate Exception Handling mechanism (See note 1).
   * Perform additional performance and memory fine tuning.
   * Add a "test" cold configuration scenario to the demo app.
   * Author a User Guide for the RxPatterns SDK.
<p>

 * RxDevice Platform:
  
   * Add sample code that simulates an instrument workbench being controlled by the platform.
   * Add sample code that simulates a device bus akin to a CBus device interface.

## Version 0.91 Release Notes
---
This is a functional release, there are intermittent bugs and race conditions that are being tracked down. 

* Note: Some time dependant unit tests (such as the merge observable) may occasionally fail due to cpu overloading changing the timing of notifications.

### 1. Evaluation Kit Dependencies.

The Evaluation Kit requires Xcode 7.2 and conforms to the Swift 2.1 language version. 

Projects and runnable code only perform in the safe sandboxed environment of the iOS simulator. Please contact Originware if you require native device support for your evaluation (you will require a temporary license key).

### 2. Supplied In This Release Package

* Frameworks (Located in the Framework folder):
	* The **RxPatternsSDK** as a fat Framework 
* Xcode Projects:	
	* The **RxPattermsLib** Library, an implementation of the classic Reactive Extensions library with full source.
	* The iPad POI (Point Of Interest) **Demonstration App** with full source (Demonstrates the use of RxPatterns).
	* A swift **Playground** for accessing the RxPatternsSDK and RxPatternsLib.

* Documentation:
	* The SDK and Lib Generated **API Documentation** as a HTML set.
	* The **RxPatterns White Paper**.

### 3. Work In Progress, to be Completed for Version 1.0

* Implement the remaining Observables:
	* selectMany
	* join
	* groupJoin
    * Implement the remaining RxPatternsLib Unit Tests.
    * Determine an appropriate Exception Handling mechanism (See note 1).
    * Perform additional performance and memory fine tuning.
    * Add a "test" cold configuration scenario to the demo app.
    * Author a User Guide for the RxPatterns SDK.

Note 1:

The **RxPatterns** error protocol: **IRxError** already inherits from the Swift exception error type: **Error**. So developer code in the RxEvalNode delegates can be wrapped in try/catch blocks which can then pass
the exception error on as a notification using notifyCompleted(exception). The question is whether the delegates should be automatically wrapped in a try/catch block during the Evaluation Engine build phase and how
does this affect evaluation performance. So the intention is to quantify this during performance analysis and decide on either of these two options: 
* Delegates to be automatically wrapped (if the performance effect is negligible). 
* Add another option to the **eRxEvalNodeSupport_Option** that will allow the developer to choose whether wrapping is included in the Eval Engine build process or not.   

### 5. Known bugs in this release.

* Trace Monitoring currently only uses the first trace tag it encounters to prefix the trace log message.
* There has been the odd unit test failure in the order of 1 in 10000 unit tests. It is not clear yet whether it was a true SDK failure or the test system being slow and causing notifications to be out of order.

### 6. Change Log

#### Changes from version 0.90 to version 0.91

##### Enhancements:

* RxEvalQueue was fitted with a deadlock prevention mechanism. The dispatchSync method now will not deadlock, even when calling it a thread nested call chain.
* The evaluation completion detection mechanism was enhanced to determine when the associated RxEvalQueue has finished running and is fully stopped.
* Unit Tests are now fitted with iteration loop control so that they can be run a selected number of times.
* RxEvalNode was fitted with a buffered input notifier which captures received notifications before the RxEvalNode comes active. 
* Monitoring was enhanced to properly detect and timestamp the actual time of RxObject construction. 
* RxSubscription was enhanced with a state property. On the event of subscription failure the state is marked as failed and given the associated error.
* The Publish mechanism was implemented and unit tested.
* The ResubscribingObserver was implemented - this allows an observer to manage unsubscription rather than it being automatically performed on a completed notification.
* RxSubject was enhanced to handle both Cold and Hot subscriptions. The Cold RxSubject was used to unit test the publish mechanism.
* The replay observable was simplified by allowing it to be supplied with a replay buffer, rather than it creating and owning it itself. So the given buffer can be externally set with the desired constraints of items counts, item expiry etc.
* Exceptions thrown in the running of RxEvalOps will now be caught and the error will be passed to the error contained in the RxSubscription. The subscription is then marked with a failed state.
* RxMon now instruments deferred subscriptions to indicate the point Evaluation begins by a state change to eRxEvalState_EvalBegin.
* The RxTrace class has been renamed to RxLog.
* Tracing has been re-engineered so that at chaining time RxObservables are tagged with their traceTag property by the expression traceAll() method. This means that the traceTag property is appropriately set during the Evaluation construction time and RxEvalOps can make use of the property to trace dependants.
* RxNotifier_NotifyConsumers now have support for thunking. This means the implementation of the RxNotifier can be set. So rather than having a number of RxNotifiers in series, the implementation of the single thunked RxNotifier can be set to perform the equivalent behaviour and provide better performance.

##### Bugs fixed:
* Monitoring now appropriately frees the object-tracking-token associated with an object when the object is destructed.
* Running unit tests over large iterations (> 10000) uncovered a large number of conditional instabilities in the areas of (which were subsequently fixed):
** Tracing and Logging were intermittently crashing with uncoordinated state changes.
** Enumerators were crashing due to queueing synchronisation failures.
** Race conditions were discovered for subscription and evaluation construction. 

### 7. Contact Details:

Please direct questions, comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com


