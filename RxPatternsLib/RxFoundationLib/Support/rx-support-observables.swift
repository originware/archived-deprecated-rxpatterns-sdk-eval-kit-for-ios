// rx-support-observables.swift
// RxPatternsLib
//
// Created by Terry Stillone on 3/10/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Markable: Marking Item Type for the mark operator.
///

public enum Markable<ItemType, MarkType>
{
    case eMark(MarkType)
    case eItem(ItemType)
    case eTerminator(IRxError?)
}

extension RxObservableMap
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// mark: Emit marks into the inline stream, when indicated by a given mark notifier.
    ///

    public final func mark<MarkType>(_ markNotifier: RxNotifier_NotifyDelegates<Markable<ItemOutType, MarkType>>) -> RxObservableMap<ItemOutType, Markable<ItemOutType, MarkType>>
    {
        typealias TargetItemType = Markable<ItemType, MarkType>

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            let outNotifier = evalNode.getSharedOutNotifier(forSync: false, withBuffering: true)
            var termination : eTerminationType? = nil
            var receivedTerminationMark = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<TargetItemType>) in

                outNotifier.notify(item: Markable.eItem(item))
            }

            evalNode.completedDelegate = { (error : IRxError?, notifier: ARxNotifier<TargetItemType>) in

                if receivedTerminationMark
                {
                    outNotifier.notifyCompleted(error: error)
                }
                else
                {
                    termination = eTerminationType(error: error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case .eNewSubscriptionInSubscriptionThread:

                        if evalNode.subscriptionCount == 0
                        {
                            markNotifier.onItem = { (mark) in
                                outNotifier.notify(item: mark)
                            }

                            markNotifier.onCompleted = { (error) in

                                outNotifier.notify(item: Markable.eTerminator(error))

                                if let termination = termination
                                {
                                    outNotifier.notify(termination: termination)
                                }
                                else
                                {
                                    receivedTerminationMark = true
                                }
                            }
                        }

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "mark", evalOp: evalOp).chainObservable(self)
    }
}