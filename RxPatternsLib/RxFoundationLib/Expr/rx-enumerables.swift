// rx-enumerables.swift
// RxPatternsLib
//
// Created by Terry Stillone (http://www.originware.com) on 4/07/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxObservableEnumerable: An enumerator of a collection of Observables.
///

open class RxObservableEnumerable<DataItemType> : IRxEnumerable, CustomStringConvertible
{
    public typealias EnumeratorIteratorType = AnyIterator<ARxProducer<DataItemType>>

    /// CustomStringConvertible compliance.
    open var  description :          String                 { return "RxObservableEnumerable" }
    
    /// The generator of RxObservables.
    fileprivate var m_iterator:            EnumeratorIteratorType

    /// Indicator of termination state.
    fileprivate var m_termination : eTerminationType? = nil

    /// The current stream subscription.
    fileprivate var m_currentSubscription: RxSubscription? = nil

    /// Initialise with the streams to enumerate over.
    /// - Parameter streams: The ARxProducer to enumerate over.
    public init(streams : [ARxProducer<DataItemType>])
    {
        var nextIndex : Int = 0
        let count = streams.count

        m_iterator = AnyIterator
        {
            if nextIndex < count
            {
                let currentIndex = nextIndex

                nextIndex += 1

                return streams[currentIndex]
            }
            else
            {
                return nil
            }
        }
    }

    /// Initialise with an Observable which will emit the ARxProducer-s to enumerate over.
    /// - Parameter streams: The RxObservables to enumerate over.
    public init(streams : ARxProducer<ARxProducer<DataItemType>>)
    {
        m_iterator = streams.toEnumerable().makeIterator()
    }

    /// Sequence conformance.
    open func makeIterator() -> EnumeratorIteratorType
    {
        return m_iterator
    }

    /// Get the next RxObservable.
    /// - Returns: The next RxObservable.
    open func next() -> ARxProducer<DataItemType>?
    {
        return m_iterator.next()
    }

    /// Set terminate the enumerable.
    open func terminate(_ termination : eTerminationType)
    {
        m_termination = termination
        m_currentSubscription?.unsubscribe()
    }

    /// Send the notifications sent to the enumerator to the notifier.
    /// - Parameter sendCompleted: emit a completed notification to the notifier.
    open func sendToNotifier(notifier : ARxNotifier<DataItemType>, sendCompleted : Bool)
    {
        while let stream = next()
        {
            if m_termination != nil { break }

            let streamNotifier = RxNotifier_NotifyDelegates<DataItemType>(tag : notifier.tag + "/sendToNotifier")

            streamNotifier.onItem = { (item : DataItemType) in

                notifier.notify(item: item)
            }

            streamNotifier.onCompleted = { (error : IRxError?) in

                if let error = error
                {
                    self.m_termination = .eError(error)
                }
            }

            let subscription = stream.subscribe(streamNotifier)

            m_currentSubscription = subscription

            let _ = subscription.waitForDisposal()
        }

        if let termination = m_termination
        {
            notifier.notify(termination: termination)
        }
        else if sendCompleted
        {
            notifier.notifyCompleted()
        }
    }
}
