// rx-observers.swift
// RxPatternsLib
//
// Created by Terry Stillone (http://www.originware.com) on 26/12/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxResubscribingObserver: An Observer that resubscribes to a given observable on receiving an error notification.
///

open class RxResubscribingObserver<StreamItemType> : ARxObserverManagingSubscription<StreamItemType>
{
    let m_retryObservable : RxObservable<StreamItemType>
    let m_iteration :       Int
    let m_count :           Int?
    let m_notifier:         ARxNotifier<StreamItemType>

    var m_resubscription: RxSubscription? = nil

    public init(observable: RxObservable<StreamItemType>, iteration: Int, count: Int?, outNotifier: ARxNotifier<StreamItemType>)
    {
        self.m_retryObservable = observable
        self.m_iteration = iteration
        self.m_count = count
        self.m_notifier = outNotifier

        super.init(tag: "\(iteration).ResubscribingObserver")
    }

    open override func notify(item: StreamItemType, subscription : RxSubscription)
    {
        //  We don't need to pass item notifications through.
        // The assumption is that the main subscription will be passing item notifications through.
        // This observer is only monitoring completed notifications.
    }

    open override func notifyCompleted(error: IRxError?, subscription : RxSubscription)
    {
        if error == nil
        {
            m_notifier.notifyCompleted()
            subscription.unsubscribe()
        }
        else if (m_count == nil) || (m_iteration < m_count!)
        {
            let resubscriptionObserver = RxResubscribingObserver<StreamItemType>(observable: m_retryObservable, iteration: m_iteration + 1, count: m_count, outNotifier: m_notifier)

            m_resubscription = m_retryObservable.subscribe(resubscriptionObserver)
        }
        else
        {
            m_notifier.notifyCompleted(error: error)
            subscription.unsubscribe()
        }
    }
}




