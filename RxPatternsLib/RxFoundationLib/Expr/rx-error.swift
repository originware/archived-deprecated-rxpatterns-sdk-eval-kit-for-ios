// rx-error.swift
// RxPatternsLib
//
//  Created by Terry Stillone on 10/09/2015.
//  Copyright © 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxLibErrorType: RxPatternsLib Standard errors and their description.
///

public enum eRxLibErrorType : String, Error, Equatable, CustomStringConvertible, CustomDebugStringConvertible
{
    /// An item was request at a specified sequence index, which does not exist.
    case eElementAtIndexOutOfRange      = "Element at index out of range"

    /// An item was requested which does not exist.
    case eNoSuchElement                 = "No such element"

    /// An operation was requested which was invalid.
    case eInvalidOperation                = "Invalid Operation"

    /// During the operation of a group observable a group key of an item could not be generated.
    case eGroupByInvalidGroupKey        = "Group by invalid group key"

    /// A timeout has occurred which has resulted in an error.
    case eTimeout                       = "Timeout"

    /// The time period specified was invalid.
    case eInvalidTimePeriod             = "Invalid time period"

    /// The mapping giving is invalid.
    case eInvalidMap                    = "Invalid map"

    /// The timestamp on an item was invalid.
    case eInvalidTimestamp              = "Invalid timestamp"

    /// The notification item was invalid.
    case eInvalidNotificationItem       = "Invalid notification item"

    /// The evalOp is invalid and cannot be used.
    case eInvalidEvalOp                 = "Invalid EvalOp"

    /// The subscription type is not hot.
    case eSubscriptionTypeIsNotHot      = "Expected subscription to be hot"

    /// A custom error that can be used for any purpose.
    case eCustomError                   = "Custom error"

    /// An error that has an associated NSError.
    case eNSError                       = "NSError"

    /// CustomStringConvertible Protocol conformance.
    public var description: String                          { return self.rawValue }
    
    /// CustomDebugStringConvertible Protocol conformance.
    public var debugDescription: String                     { return self.description }
}

/// eRxLibErrorType Equatable operator.
public func ==(lhs: eRxLibErrorType, rhs: eRxLibErrorType) -> Bool
{
    return lhs.rawValue == rhs.rawValue
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxErrorContext: The Error context representation.
///

public struct RxErrorContext : Equatable, CustomStringConvertible, CustomDebugStringConvertible
{
    public typealias ErrorInfo = [String : AnyObject]

    /// The dictionary of context details.
    fileprivate let m_infoDict: ErrorInfo

    /// Initialise by the context dictionary details.
    public init(_ info : ErrorInfo)
    {
        m_infoDict = info
    }

    /// The context dict details.
    public var infoDict: [String : AnyObject]               { return m_infoDict }

    /// Subscript by the context details (String).
    public subscript(key : String) -> AnyObject?            { return m_infoDict[key] }

    /// CustomStringConvertible Protocol conformance.
    public var description: String                          { return m_infoDict.description }

    /// CustomDebugStringConvertible Protocol conformance.
    public var debugDescription: String                     { return self.description }
}

/// Equatable operator for class RxErrorContext.
public func ==(lhs: RxErrorContext, rhs: RxErrorContext) -> Bool
{
    if lhs.infoDict.count != rhs.infoDict.count
    {
        return false
    }
    
    for (key, value) in lhs.infoDict
    {
        if let strValueLhs = value as? String, let strValueRhs = rhs.infoDict[key] as? String
        {
            if strValueLhs != strValueRhs
            {
                return false
            }
        }
        else if rhs.infoDict[key] !== value
        {
            return false
        }
    }
    
    return true
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxLibError: Error representation for the RxPatternsLib.
///

public struct RxLibError : IRxError, Equatable, CustomStringConvertible, CustomDebugStringConvertible
{
    /// The type of error.
    public let errorType : eRxLibErrorType
    
    /// The optional error context.
    public var context : RxErrorContext?                { return m_errorContext }
    
    /// CustomStringConvertible Protocol conformance.
    public var description: String
    {
        let contextDescription : String = context != nil ? "(\(context!.description))" : ""
        let errorDescription = m_description ?? errorType.description

        return errorDescription + contextDescription
    }
    
    /// CustomDebugStringConvertible Protocol conformance.
    public var debugDescription: String                     { return self.description }

    /// The context for the cause of the error.
    fileprivate let m_errorContext : RxErrorContext?

    /// The description of the error.
    fileprivate let m_description : String?

    /// Initialise the error with error type and optional context.
    /// - Parameter errorType: Indicates the error type.
    /// - Parameter context: The optional error context.
    public init(_ errorType: eRxLibErrorType, context : RxErrorContext? = nil, description : String? = nil)
    {
        self.errorType = errorType
        self.m_errorContext = context
        self.m_description = description
    }
    
    /// Equator function that references the internal error context.
    /// - Parameter other: The error to compare to.
    /// - Returns: The comparison result.
    public func isEqual(_ other : IRxError) -> Bool
    {
        guard let otherError = other as? RxLibError, errorType == otherError.errorType else { return false }

        switch (context, otherError.context)
        {
        case (nil, nil):
            return true

        case (let selfContext?, let otherContext?):
            return selfContext == otherContext

        default:
            return false
        }
    }
}

/// Equatable operator for class RxLibError.
public func ==(lhs: RxLibError, rhs: RxLibError) -> Bool
{
    return lhs.isEqual(rhs)
}

/// The Matching operator for Error and eRxLibErrorType.
public func ~=(lhs: Error, rhs: eRxLibErrorType) -> Bool
{
    guard let error = lhs as? RxLibError else { return false }

    return error.errorType == rhs
}

/// The Matching operator for eRxLibErrorType and Error.
public func ~=(lhs: eRxLibErrorType, rhs: Error) -> Bool
{
    guard let error = rhs as? RxLibError else { return false }

    return error.errorType == lhs
}



/// Equatable operator for the RxNotification<[ItemType]> class.
public func ==<ItemType : Equatable>(lhs: RxNotification<ItemType?>, rhs: RxNotification<Optional<ItemType>>) -> Bool
{
    if RxSDK.error.TimestampComparator != nil
    {
        switch (lhs.timeStamp, rhs.timeStamp)
        {
            case (nil, nil):
                break

            case (nil, _?), (_?, nil):
                return false

            case (_?, _?):

                return RxSDK.error.TimestampComparator!(lhs.timeStamp!, rhs.timeStamp!)
        }
    }

    switch (lhs.notifyType, rhs.notifyType)
    {
        case (.eNotification_Item(let lhsItem), .eNotification_Item(let rhsItem)):

            return lhsItem == rhsItem

        case (.eNotification_Termination(let lhsTermination), .eNotification_Termination(let rhsTermination)):
            return lhsTermination == rhsTermination

        default:
            return false
    }
}
