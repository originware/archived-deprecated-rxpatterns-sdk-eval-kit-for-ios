// rx-subject.swift
// RxPatternsLib
//
// Created by Terry Stillone (http://www.originware.com) on 8/04/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSubject: The classic RxSubject class. Performs as both an Observer and Observable (of the same generic ItemType).
///
/// - Useful for unit testing expressions.
/// - The notify methods are synchronous and return only after the notification has been delivered.
///

open class RxSubject<ItemType> : RxSource<ItemType>, IRxSubject
{
    public typealias ObserverItemType = ItemType

    /// Indicates that there are active subscriptions.
    open var isActive : Bool          { return outNotifier != nil }

    /// The notifier to emit from the subject.
    open var outNotifier: ARxNotifier<ItemType>? = nil

    /// The barrier to delay any notifications being received, to accommodate pending subscriptions.
    fileprivate let m_pendingSubscriptionBarrier = RxBarrier()

    /// The RxObject type for this class.
    open override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter subscriptionType: The subscription type (Hot/Cold).
    /// - Parameter evalOp: The behaviour of the source encapsulated as an RxEvalOp.
    public override init(tag: String, subscriptionType : eRxSubscriptionType = .eHot, evalOp: RxEvalOp? = nil)
    {
        // Note: Takes the evalOp from the createEvalOpDelegate() func.
        super.init(tag: tag, subscriptionType: subscriptionType, evalOp: nil)
    }

    /// Notify reception of Item.
    /// - Parameter item: The item being notified.
    open func notify(item: ItemType)
    {
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            // Emit the given item notification.
            self.outNotifier!.notify(item: item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    open func notifyCompleted(error: IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            self.outNotifier!.notifyCompleted(error: error)
        }
    }

    /// Notify reception of Eval State Change.
    /// - Parameter stateChange: The new eval state.
    open func notify(stateChange : eRxEvalStateChange)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyStateChange(self, stateChange))
            #endif

            // Emit the given state change notification.
            self.outNotifier!.notify(stateChange: stateChange)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    open override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode : RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in

                switch self.subscriptionType
                {
                    case .eHot:
                        self.hotSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)

                    case .eCold:
                        self.coldSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)
                }
            }
        }
    }

    /// The eval state change handler made access able to classes that inherit from RxSubject.
    open func hotSubjectStateChangeHandler(_ evalNode: RxEvalNode<ItemType, ItemType>, stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>)
    {
        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                if self.outNotifier == nil
                {
                    self.outNotifier = evalNode.getSharedOutNotifier()
                }

            case eRxEvalStateChange.eEvalEnd:

                self.outNotifier = nil

            default:
                break
        }
    }

    /// The eval state change handler made access able to classes that inherit from RxSubject.
    open func coldSubjectStateChangeHandler(_ evalNode: RxEvalNode<ItemType, ItemType>, stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>)
    {
        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                // Increment subscription barrier pending count
                self.m_pendingSubscriptionBarrier.enterBlock()

            case eRxEvalStateChange.eSubscriptionBegin:

                // As a cold subscription will create a separate Eval Engine per subscription, the evalNode.syncOutNotifier will be
                //   different for each subscription we must append each notifier in order to emit to each subscription eval node.
                if let outNotifier = self.outNotifier as? RxNotifier_NotifyConsumers
                {
                    outNotifier.appendConsumer(consumer: evalNode.getSharedOutNotifier())
                }
                else
                {
                    let outNotifier = RxNotifier_NotifyConsumers<ItemType>(tag: self.tag + "/notifier")

                    outNotifier.appendConsumer(consumer: evalNode.getSharedOutNotifier())

                    self.outNotifier = outNotifier
                }

            case eRxEvalStateChange.eEvalBegin:

                // Decrement subscription barrier pending count.
                self.m_pendingSubscriptionBarrier.exitBlock()

            case eRxEvalStateChange.eEvalEnd:

                // On the last subscription, nil out the reference to the RxEvalNode emit notifier.
                if (self.outNotifier != nil) && (self.outNotifier!.count == 0)
                {
                    self.outNotifier = nil
                }

            case eRxEvalStateChange.eSubscriptionEnd:

                // Remove notifier:
                if let outNotifier = self.outNotifier as? RxNotifier_NotifyConsumers
                {
                    let _ = outNotifier.removeConsumer(consumer: evalNode.getSharedOutNotifier())
                }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ReplaySubject: Replay saved notifications to any subsequent subscriptions.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#ReplaySubject )
/// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/hh211810(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

open class RxReplaySubject<DataItemType> : RxSubject<DataItemType>
{
    /// The notifications to be replayed.
    open let replayNotifications: RxNotificationConstrainedSequence<DataItemType>
    fileprivate var m_queuer : RxEvalQueue? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag: String)
    {
        self.replayNotifications = RxNotificationConstrainedSequence<DataItemType>(tag: tag + "/replayNotifications")

        super.init(tag: tag)
    }

    /// Initialise with tag and max count of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter maxItemCount: The maximum number of items to replay.
    public convenience init(tag: String, maxItemCount: RxCountType)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemCount = maxItemCount
    }

    /// Initialise with tag and duration of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter duration: The maximum duration back in time of notifications to replay.
    public convenience init(tag: String, duration: RxDuration)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemTimeSpan = duration
    }

    /// Initialise with tag, max count of items and duration of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter maxItemCount: The maximum number of items to replay.
    /// - Parameter duration: The maximum duration back in time of notifications to replay.
    public convenience init(tag: String, maxItemCount: RxCountType, duration: RxDuration)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemTimeSpan = duration
        self.replayNotifications.maxItemCount = maxItemCount
    }

    /// Notify reception of Item
    /// - Parameter item: The item being notified.
    open override func notify(item: DataItemType)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive && (m_queuer != nil)
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            m_queuer!.dispatch(sync: {
                // queue the item to replay to future subscriptions.
                self.replayNotifications.queueItem(item)
            })

            // Emit the given item notification.
            self.outNotifier!.notify(item: item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    open override func notifyCompleted(error: IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            outNotifier!.notifyCompleted(error: error)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    open override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<DataItemType, DataItemType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<DataItemType>) in

                self.hotSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)

                switch stateChange
                {
                    case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                        if self.m_queuer == nil
                        {
                            self.m_queuer = evalNode.evalQueue
                        }

                    case eRxEvalStateChange.eSubscriptionBegin:

                        // Replay the previous items to the new subscription.
                        if (self.replayNotifications.itemCount > 0) && self.isActive
                        {
                            self.replayNotifications.sendItemsToNotifyHandlerNonDestructive(notifier)
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_queuer = nil

                    default:
                        break
                }
            }
        }
    }

    /// Clear the replay notifications buffer.
    open func clear()
    {
        self.replayNotifications.removeAll()
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// BehaviourSubject: Replay the last notification to subsequent subscriptions.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#BehaviorSubject )
/// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211949(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

open class RxBehaviourSubject<ItemType> : RxSubject<ItemType>
{
    /// The last item received.
    fileprivate var m_lastItem: ItemType? = nil

    /// The EvalNode's EvalQueue, used for synchronisation.
    fileprivate var m_evalQueue : RxEvalQueue? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag : String, defaultItem: ItemType)
    {
        self.m_lastItem = defaultItem

        super.init(tag: tag)
    }

    /// Notify reception of Item
    /// - Parameter item: The item being notified.
    open override func notify(item: ItemType)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            m_evalQueue?.dispatch(sync: {
                m_lastItem = item
            })

            // Emit the given item notification.
            outNotifier!.notify(item: item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    open override func notifyCompleted(error: IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        let _ = m_pendingSubscriptionBarrier.wait()

        m_lastItem = nil

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            outNotifier!.notifyCompleted(error: error)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    open override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode : RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in

                self.hotSubjectStateChangeHandler(evalNode, stateChange: stateChange, notifier: notifier)

                switch stateChange
                {
                    case .eNewSubscriptionInSubscriptionThread:

                        if evalNode.subscriptionCount == 0
                        {
                            self.m_evalQueue = evalNode.evalQueue
                        }

                    case .eSubscriptionBegin:

                        if let item = self.m_lastItem
                        {
                            notifier.notify(item: item)
                        }

                    case .eSubscriptionEnd:

                        if evalNode.subscriptionCount == 1
                        {
                            self.m_evalQueue = nil
                        }

                    default:
                        break
                }
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AsyncSubject: Notify methods are asynchronous and return before notification finishes.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#AsyncSubject )
/// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229363(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

open class RxAsyncSubject<ItemType> : RxSubject<ItemType>
{
    fileprivate var m_lastItem : ItemType? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag : String)
    {
        super.init(tag: tag)
    }

    /// Notify the reception of an Item
    /// - Parameter item: The item being notified.
    open override func notify(item: ItemType)
    {
        #if RxMonEnabled
            RxMon.monObservable(.eObservable_notifyItem(self, item))
        #endif

        if outNotifier != nil
        {
            // Save the last consumed item.
            m_lastItem = item
        }
    }

    /// Notify the reception of a Completed/Error.
    /// - Parameter error: The error or nil if completed.
    open override func notifyCompleted(error: IRxError? = nil)
    {
        #if RxMonEnabled
            RxMon.monObservable(.eObservable_notifyCompleted(self, error))
        #endif

        if let outNotifier = outNotifier
        {
            if let item = m_lastItem
            {
                // Emit the last consumed item.
                outNotifier.notify(item: item)
            }

            // Emit completion.
            outNotifier.notifyCompleted(error: error)
        }

        m_lastItem = nil
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxRelaySubjectMap: An asynchronous notifications mapping relay. Relays from its ARxConsumer style interface to subscribers.
///

open class RxRelaySubjectMap<ItemInType, ItemOutType> : RxObservableMap<ItemInType, ItemOutType>
{
    /// Indicates that there are active subscriptions.
    open var isActive : Bool          { return outNotifier != nil }

    /// The notifier to emit from the subject.
    open var outNotifier: ARxNotifier<ItemOutType>? = nil

    /// The RxObject type for this class.
    open override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with a tag.
    /// - Parameter tag: The RxObject tag.
    public init(tag : String)
    {
        super.init(sourceTag: tag, subscriptionType: eRxSubscriptionType.eHot, evalOp: nil)
    }

    /// Notify reception of an item notification.
    /// - Parameter item: The item of the notification.
    open func notify(item : ItemInType)
    {
        if isActive
        {
            if let outItem = item as? ItemOutType
            {
                self.outNotifier?.notify(item: outItem)
            }
            else
            {
                assert(false, "Could not convert item to type \(ItemOutType.self)")
            }
        }
    }

    /// Notify reception of a completed notification.
    /// - Parameter error: Optional that indicates whether the sequence completed with an error.
    open func notifyCompleted(error: IRxError? = nil)
    {
        outNotifier?.notifyCompleted(error: error)
    }

    /// Notify reception of a eval state change notification.
    /// - Parameter stateChange: The new eval state.
    open func notify(stateChange : eRxEvalStateChange)
    {
        outNotifier?.notify(stateChange: stateChange)
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    open override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode: RxEvalNode<ItemInType, ItemOutType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.outNotifier = evalNode.getSharedOutNotifier(forSync: false)

                    case eRxEvalStateChange.eEvalEnd:

                        self.outNotifier = nil

                    default:
                        break
                }
            }
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxRelaySubject: An asynchronous notifications relay. Relays from a consumer interface to subscribers.
///

open class RxRelaySubject<ItemType> : RxSource<ItemType>, IRxConsumer
{
    var m_notifier: ARxNotifier<ItemType>? = nil

    /// The RxObject type for this class.
    open override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with a tag.
    /// - Parameter tag: The RxObject tag.
    public init(tag : String)
    {
        super.init(tag: tag, subscriptionType: eRxSubscriptionType.eHot, evalOp: nil)
    }

    /// Notify reception of an item notification.
    /// - Parameter item: The item of the notification.
    open func notify(item : ItemType)
    {
        self.m_notifier?.notify(item: item)
    }

    /// Notify reception of a completed notification.
    /// - Parameter error: Optional that indicates whether the sequence completed with an error.
    open func notifyCompleted(error: IRxError? = nil)
    {
        m_notifier?.notifyCompleted(error: error)
    }

    /// Notify reception of a eval state change notification.
    /// - Parameter stateChange: The new eval state.
    open func notify(stateChange : eRxEvalStateChange)
    {
        m_notifier?.notify(stateChange: stateChange)
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    open override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode: RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.m_notifier = evalNode.getSharedOutNotifier(forSync: false)

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_notifier = nil

                    default:
                        break
                }
            }
        }
    }
}
