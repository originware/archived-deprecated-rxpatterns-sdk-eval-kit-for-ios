// tx-testcase.swift
// RxPatternsLib Tests
//
// Created by Terry Stillone (http://www.originware.com) on 27/09/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

import RxPatternsSDK
import RxPatternsLib

class TxTestCase: XCTestCase
{
    var error : RxLibError = TxFactory.createError("<Generated Error>")

    var m_stringTable = [Int : String]()
    var m_traceBuffer = [(Int, Int)]()
    var m_lastTraceMessage = RxTime()
    var m_aliveCount : Int = 0
    var m_isTicking = false

    @inline(__always) func isInTenPercentile(_ count : Int, _ iteration : Int) -> Bool
    {
        return  (count <= 10) || (iteration == count - 1) || (iteration % (count / 10) == 0)
    }

    @inline(__always) func percentage( _ count : Int, _ iteration : Int) -> String
    {
        if count <= 10
        {
            return String(iteration * 10)
        }
        else if iteration == count - 1
        {
            return "100"
        }
        else
        {
            let floatPercentage : Float = Float(100 * iteration) / Float(count)

            return String(Int(round(floatPercentage)))
        }
    }

    final func trace(_ message : String)
    {
        let hash = message.hashValue

        m_stringTable[hash] = message
        m_traceBuffer.append((0, hash))
    }

    final func trace(_ lod : Int, _ message : String)
    {
        let hash = message.hashValue

        m_stringTable[hash] = message
        m_traceBuffer.append((lod, hash))

        if lod <= TxConfig.TraceThreshold
        {
            if m_isTicking { RxLog.log("\n") }
            
            RxLog.log(message)
            m_lastTraceMessage = RxTime()
            m_aliveCount = 0
        }
        else if -m_lastTraceMessage.timeIntervalSinceNow > 5
        {
            m_lastTraceMessage = RxTime()
            
            if m_aliveCount == 0
            {
                RxLog.log("\n\t5 Second Ticks >> ")
                m_aliveCount += 1
                m_isTicking = true
            }

            RxLog.log(" %3d", m_aliveCount)
            m_aliveCount += 1
        }

        if (m_aliveCount != 0) && (m_aliveCount % 40 == 0)
        {
            RxLog.log("\n\t5 Second Ticks >> ")
            m_aliveCount += 1
        }
    }

    override func setUp()
    {
        super.setUp()
        
        RxSDK.control.start()

        RxSDK.error.TimestampComparator = { (lhs: eRxTime, rhs: eRxTime) -> Bool in

            return lhs.diff(rhs)! < TxConfig.TestTickUnit * 1.5
        }
    }

    override func tearDown()
    {
        RxSDK.control.shutdown()

        super.tearDown()
    }
}
