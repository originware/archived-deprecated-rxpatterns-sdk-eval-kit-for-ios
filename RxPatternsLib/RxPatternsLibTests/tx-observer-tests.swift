// tx-observer-tests.swift
// RxPatternsLib Tests
//
// Created by Terry Stillone (http://www.originware.com) on 15/04/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

import RxPatternsSDK
@testable import RxPatternsLib

class TxTestRig_ObserverThreadChecker<DataItemType : Hashable> : ARxObserver<eTxToken<DataItemType>>
{
    typealias RigTokenType = eTxToken<DataItemType>
    
    var notifications :                 RxNotificationSequence<RigTokenType>
    var enableThreadChecking :          Bool
    var subscription :                  RxSubscription? {

        didSet {

            subscription!.evalQueue!.setInfo()
        }
    }

    lazy var subscriptionEvalQueue :    RxEvalQueue = self.subscription!.evalQueue!
    lazy var subscriptionEvalQueueInfo: RxEvalQueueInfo = self.subscriptionEvalQueue.info

    init(tag: String, enableThreadChecking : Bool = false)
    {
        self.enableThreadChecking = enableThreadChecking
        self.notifications = RxNotificationSequence<RigTokenType>(tag: tag)
        self.subscription = nil

        super.init(tag: tag)

        RxSDK.evalQueue.UIThreadQueue.setInfo()
    }

    override func notify(item: ConsumerItemType)
    {
        if enableThreadChecking { checkIsRunningInEvalQueue() }

        notifications.queueItem(item)
    }

    override func notifyCompleted(error : IRxError? = nil)
    {
        if enableThreadChecking { checkIsRunningInEvalQueue() }

        notifications.queueItem(eTxToken.eCompleted(error))
    }

    override func notify(stateChange : eRxEvalStateChange)
    {
        if enableThreadChecking
        {
            switch stateChange
            {
                case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:
                    checkIsRunningInUIThread()

                case eRxEvalStateChange.eSubscriptionBegin:
                    checkIsNotRunningInUIThread()

                default:
                    checkIsRunningInEvalQueue()
            }
        }

        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:
                notifications.queueItem(.eNewSubscriptionToken)
                break

            case eRxEvalStateChange.eSubscriptionBegin:
                notifications.queueItem(.eSubscribeToken)
                break

            case eRxEvalStateChange.eEvalBegin:
                notifications.queueItem(.eBeginToken)
                break

            case eRxEvalStateChange.eEvalEnd:
                notifications.queueItem(.eEndToken)

            case eRxEvalStateChange.eSubscriptionEnd:
                notifications.queueItem(.eUnSubscribeToken)
                break
        }
    }

    func checkIsRunningInEvalQueue()
    {
        XCTAssertTrue(self.subscriptionEvalQueue.identity == RxThreadTrackableIdentifier.getThreadIndentity(), "Expected the EvalQueueInfo for the subscription Eval Queue")
        XCTAssertTrue(subscriptionEvalQueue.isRunningInEvalQueue(), "Expected method not to be running in the Subscription Eval Queue")
        XCTAssertFalse(RxSDK.evalQueue.UIThreadQueue.isRunningInEvalQueue(), "Expected method not to be running in the UI Thread")
    }

    func checkIsNotRunningInUIThread()
    {
        XCTAssertTrue(RxSDK.evalQueue.UIThreadQueue.identity != RxThreadTrackableIdentifier.getThreadIndentity(), "Expected the UI thread to not be running")
        XCTAssertFalse(RxSDK.evalQueue.UIThreadQueue.isRunningInEvalQueue(), "Expected the UI thread to not be running")
    }

    func checkIsRunningInUIThread()
    {
        XCTAssertTrue(RxSDK.evalQueue.UIThreadQueue.identity == RxThreadTrackableIdentifier.getThreadIndentity(), "Expected the UI thread to running")
        XCTAssertTrue(RxSDK.evalQueue.UIThreadQueue.isRunningInEvalQueue(), "Expected the UI thread to running")
    }
}

class TxTestRig_ControllingObserverChecker<DataItemType : Hashable> : RxControllingObserver<eTxToken<DataItemType>>
{
    typealias TokenType = eTxToken<DataItemType>
    
    var notifications : RxNotificationSequence<TokenType>
    var expectedSubscriptions = [RxSubscription]()
    
    override init(tag: String)
    {
        self.notifications = RxNotificationSequence<TokenType>(tag: tag)

        super.init(tag: tag)
    }

    override func notify(item: ConsumerItemType)
    {
        XCTAssertTrue(subscriptions == expectedSubscriptions, "Expected subscription to not change")

        notifications.queueItem(item)
        
        if item == .eUnSubscribeToken
        {
            subscriptions.first!.unsubscribe()
        }
    }

    override func notifyCompleted(error : IRxError? = nil)
    {
        XCTAssertTrue(subscriptions == expectedSubscriptions, "Expected subscription to not change")

        notifications.queueItem(eTxToken.eCompleted(error))
    }

    override func notify(stateChange : eRxEvalStateChange)
    {
        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread(let subscription):
                subscriptions.append(subscription)
                notifications.queueItem(.eNewSubscriptionToken)

            case eRxEvalStateChange.eSubscriptionBegin(let subscription):
                subscriptions.append(subscription)
                notifications.queueItem(.eSubscribeToken)

            case eRxEvalStateChange.eEvalBegin:

                expectedSubscriptions = subscriptions
                notifications.queueItem(.eBeginToken)

            case eRxEvalStateChange.eEvalEnd:
                notifications.queueItem(.eEndToken)

            case eRxEvalStateChange.eSubscriptionEnd:
                notifications.queueItem(.eUnSubscribeToken)
        }
    }
}

class RxObserverTests : TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testObserver()
    {
        let testObservableName = "observer"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TokenType = eTxToken<ItemType>
            typealias RunEntry = (runName:String, source:RxSource<TokenType>, expectedResult:RxNotificationQueue<TokenType>)

            let testName = TxFactory.createTestName(#function)

            func I(_ value : ItemType) -> TokenType
            {
                return eTxToken<String>.eItem(value)
            }

            let emptySource = RxSource<TokenType>.empty()
            let baseTokenList = [I("one"), I("two"), I("three"), I("four"), I("five")]
            let baseExpectedTokenList = [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, I("one"), I("two"), I("three"), I("four"), I("five"), .eCompleted(nil), .eEndToken]
            let baseExpectedErrorTokenList = [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, I("one"), I("two"), I("three"), I("four"), I("five"), .eCompleted(error), .eEndToken]
            let source1 = RxSource<TokenType>.fromArray(baseTokenList)
            let source1WithError = RxSource<TokenType>.fromArray(baseTokenList, termination: .eError(error))
            let emptySourceWithError = RxSource<TokenType>.fromArray([], termination: .eError(error))

            let run1: RunEntry = (

                    runName:            "Test normal item operation",
                    source:             source1,
                    expectedResult:     RxNotificationQueue<TokenType>(items: baseExpectedTokenList + [.eUnSubscribeToken])
            )

            let run2: RunEntry = (

                    runName:            "Test empty source",
                    source:             emptySource,
                    expectedResult:     RxNotificationQueue<TokenType>(items: [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, .eCompleted(nil), .eEndToken, .eUnSubscribeToken])
            )

            let run3: RunEntry = (

                    runName:            "Test source with items and error",
                    source:             source1WithError,
                    expectedResult:     RxNotificationQueue<TokenType>(items: baseExpectedErrorTokenList + [.eUnSubscribeToken])
            )

            let run4: RunEntry = (

                    runName:            "Test empty source with error",
                    source:             emptySourceWithError,
                    expectedResult:     RxNotificationQueue<TokenType>(items: [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, .eCompleted(error), .eEndToken, .eUnSubscribeToken])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ] {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    var subscription : RxSubscription? = nil
                    let observer = TxTestRig_ObserverThreadChecker<ItemType>(tag: runName, enableThreadChecking: true)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    subscription = source.deferredSubscribe(observer)

                    observer.subscription = subscription

                    subscription!.runAsync()

                    TxDisposalTestRigs.waitForDisposal(subscription!)
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testControllingObserver()
    {
        let testObservableName = "controlling observer"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TokenType = eTxToken<ItemType>
            typealias RunEntry = (runName:String, source:RxSource<TokenType>, expectedResult:RxNotificationQueue<TokenType>)

            let testName = TxFactory.createTestName(#function)

            func I(_ value : String) -> TokenType
            {
                return eTxToken.eItem(value)
            }

            let emptySource = RxSource<TokenType>.empty()
            let baseTokenList = [I("one"), I("two"), I("three"), I("four"), I("five")]
            let baseExpectedTokenList = [.eBeginToken, I("one"), I("two"), I("three"), I("four"), I("five"), .eCompleted(nil), .eEndToken]
            let baseExpectedErrorTokenList = [.eBeginToken, I("one"), I("two"), I("three"), I("four"), I("five"), .eCompleted(error), .eEndToken]
            let source1 = RxSource<TokenType>.fromArray(baseTokenList)
            let source1WithError = RxSource<TokenType>.fromArray(baseTokenList, termination: .eError(error))
            let emptySourceWithError = RxSource<TokenType>.fromArray([], termination: .eError(error))

            func generateExpectedTokenList(_ error: IRxError?) -> [TokenType]
            {
                return [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, I("one"), I("two"), I("three"), I("four"), I("five"), .eCompleted(error), .eEndToken, .eUnSubscribeToken]
            }

            let run1: RunEntry = (

                    runName:            "Test normal item operation",
                    source:             source1,
                    expectedResult:     RxNotificationQueue<TokenType>(items: generateExpectedTokenList(nil))
            )

            let run2: RunEntry = (

                    runName:            "Test empty source",
                    source:             emptySource,
                    expectedResult:     RxNotificationQueue<TokenType>(items: [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, .eCompleted(nil), .eEndToken, .eUnSubscribeToken])
            )

            let run3: RunEntry = (

                    runName:            "Test source with items and error",
                    source:             source1WithError,
                    expectedResult:     RxNotificationQueue<TokenType>(items: generateExpectedTokenList(error))
            )

            let run4: RunEntry = (

                    runName:            "Test empty source with error",
                    source:             emptySourceWithError,
                    expectedResult:     RxNotificationQueue<TokenType>(items: [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, .eCompleted(error), .eEndToken, .eUnSubscribeToken])
            )

            let run5: RunEntry = (

                    runName:            "Test unsubscription",
                    source:             RxSource<TokenType>.fromArray([I("one"), I("two"), .eUnSubscribeToken, I("four"), I("five")]),
                    expectedResult:     RxNotificationQueue<TokenType>(items: [.eNewSubscriptionToken, .eSubscribeToken, .eBeginToken, I("one"), I("two"), .eUnSubscribeToken, .eEndToken, .eUnSubscribeToken])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let observer = TxTestRig_ControllingObserverChecker<String>(tag : runName)
                    let notifications : RxNotificationSequence<TokenType> = observer.notifications
                    let reportFunc = reportGenerator(runName, expectedResult, notifications)
                    let subscription = source.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(subscription == observer.subscriptions.first!, "Expected subscription to be correct")
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")

            runAll()

            RxMon.shutdown()
        }
    }
}
