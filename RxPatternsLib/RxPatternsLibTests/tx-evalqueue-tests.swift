// tx-evalqueue-tests.swift
// RxPatternsLib Tests
//
// Created by Terry Stillone (http://www.originware.com) on 28/11/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

import RxPatternsSDK
@testable import RxPatternsLib

class RxEvalQueueTests: TxTestCase
{
    typealias ItemType = Int
    typealias RunEntry = (runName:String, observable:ARxProducer<ItemType>, expectedEvalQueueCount:Int, expected:RxNotificationQueue<ItemType>)
    typealias SourceRunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

#if RxQueuePriorityEnabled

    func testEvalQueuePriority()
    {
        let testObservableName = "fromArray"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias ItemFactoryType = () -> ItemType
            typealias RunEntry = (runName:String, items:[ItemType], eTerminationType, expected:RxNotificationQueue<ItemType>, priority : eRxEvalQueuePriority?)

            let run1: RunEntry = (

                    runName:        "Test no priority given",
                    items:          [0, 1],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1]),
                    priority:       nil
            )

            let run2: RunEntry = (

                    runName:        "Test background priority",
                    items:          [0, 1, 2],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1, 2]),
                    priority:       eRxEvalQueuePriority.eBackground(0)
            )

            let run3: RunEntry = (

                    runName:        "Test high priority",
                    items:          [2, 4],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [2, 4]),
                    priority:       eRxEvalQueuePriority.eHigh(2)
            )

            let run4: RunEntry = (

                    runName:        "Test low priority",
                    items:          [5],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [5]),
                    priority:       eRxEvalQueuePriority.eLow(4)
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, items, termination, expected, priority) = runSpec

                func runTest()
                {
                    let observable = RxSource.fromArray(items, termination : termination, tag: "source")
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let notifier = RxNotifier_NotifyDelegates<ItemType>(tag : "extract notifier")

                    let property2 = eRxExprProperty.eEvalQueuePriority(eRxEvalQueuePriority.eLow(1))
                    let tagInfoProperty = eRxExprProperty.eInfoTagEvalQueue(true)

                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    if let priority = priority
                    {
                        let property  = eRxExprProperty.eEvalQueuePriority(priority)

                        notifier.onItem = { (item : ItemType) in

                            let info = RxThreadTrackableObject<RxEvalQueueInfo>.getRunningInfo()

                            XCTAssertTrue(info!.evalQueuePriority == priority, "Unexpected RxEvalQueue priority")
                        }

                        let subscription = observable.extractPat("extract", extractConsumer: notifier).setProperty(property2).setProperty(property).setProperty(tagInfoProperty).subscribe(observer)

                        TxDisposalTestRigs.waitForDisposal(subscription)

                        XCTAssertTrue(expected == observer.notifications, reportFunc())
                    }
                    else
                    {
                        notifier.onItem = { (item : ItemType) in

                            let info = RxThreadTrackableObject<RxEvalQueueInfo>.getRunningInfo()

                            XCTAssertTrue(info!.evalQueuePriority == eRxEvalQueuePriority.eInherit, "Unexpected RxEvalQueue priority")
                        }

                        let subscription = observable.extractPat("extract", extractConsumer: notifier).setProperty(tagInfoProperty).subscribe(observer)

                        TxDisposalTestRigs.waitForDisposal(subscription)

                        XCTAssertTrue(expected == observer.notifications, reportFunc())
                    }

                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
#endif

}
