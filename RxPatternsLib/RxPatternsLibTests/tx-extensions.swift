//
//  tx-Extensions.swift
//  RxPatternsLib tTsts
//
//  Created by Terry Stillone on 30/08/2015.
//  Copyright © 2015 Originware. All rights reserved.
//

import Foundation

import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Set extensions
//

extension Set
{
    init(notifications: RxNotificationQueue<Element>)
    {
        self.init()
        
        for item in notifications
        {
            self.insert(item)
        }
    }
    
    init(notifications: RxNotificationSequence<Element>)
    {
        self.init()
        
        for notification in notifications
        {
            if case .eNotification_Item(let item) = notification.notifyType
            {
                self.insert(item)
            }
        }
    }
}
