// tx-subscription-tests.swift
// RxPatternsLib Tests
//
// Created by Terry Stillone (http://www.originware.com) on 19/05/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

import RxPatternsSDK
@testable import RxPatternsLib

/// Custom Unit Test Exception.
struct CustomUnitTestErrorType: Error
{

}

extension RxObservable
{
    func failEvalOp(_ value : ItemType) -> RxObservable<ItemType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) throws -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemType>) in

                notifier.notify(item: value)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemType>) in

                notifier.notifyCompleted(error: nil)
            }

            throw(CustomUnitTestErrorType())
        }

        return RxObservable<ItemType>(tag: "failEvalOp", evalOp: evalOp).chainObservable(self)
    }
}

class RxSubscriptionTests: TxTestCase
{
    typealias ItemType = Int
    typealias RunEntry = (runName:String, observable:ARxProducer<ItemType>, expectedEvalQueueCount: Int, expected:RxNotificationQueue<ItemType>)
    typealias SourceRunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func createRun(_ index : Int) -> RunEntry
    {
        switch index
        {
            case 0:

                let source = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])

                return (

                    runName:                "Take test",
                    observable:             source.take(2).appendTag("1").take(2).appendTag("2").take(2).appendTag("3"),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21])
                )

            case 1:

                let source = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])

                return (
                    runName:                "Skip test",
                    observable:             source.skip(2),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [35, 44, 50])
                    )

            case 2:

                let emptySource = RxSource<ItemType>.empty()

                return (

                    runName:                "Empty source test",
                    observable:             emptySource.take(2),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [])
                )

            case 3:

                let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eError(error))

                return (

                    runName:                "Error test",
                    observable:             source1WithError.take(4),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44])
                )

            case 4:

                let source = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])

                return (

                runName:                "Not enough elements test",
                observable:             source.take(10),
                expectedEvalQueueCount: 1,
                expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
                )

            case 5:

                let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eError(error))

                return (

                    runName:                "Not enough elements with error test",
                    observable:             source1WithError.take(10),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eError(error))
                )

            case 6:

                let source1 = RxSource<ItemType>.fromArray([110, 121, 135, 144, 150])
                let source2 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])

                let zipSelector = { (item1 : ItemType, item2 : ItemType) -> ItemType? in

                    return item1 + item2
                }

                return (

                    runName:                "Zip test",
                    observable:             source1.zip(source2, selector : zipSelector),
                    expectedEvalQueueCount: 2,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [120, 142, 170, 188, 200])
                )

            case 7:

                let source = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])

                return (

                    runName:                "Debounce test",
                    observable:             source.debounce(TxConfig.TestTickUnit),
                    expectedEvalQueueCount: 1,
                    expected:               RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 50])
                )

            default:

                fatalError("Unexpected index")
        }
    }

    func testSubscriptionState()
    {
        let subscriptionFailureError = RxError(.eSubscriptionFailure("Expected the subscription state to be eShutdown with error"))

        func subscriptionStateForObserver()
        {
            for index in 0...7
            {
                var currentRunName : String = ""

                func runTest()
                {
                    do
                    {
                        let (runName, observable, _, _) = createRun(index)

                        let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("subscription test observer")

                        trace(2, "= run begin ==> \(runName) : \(String(describing: name)) <==\n\n")

                        do
                        {
                            let subscription = observable.subscribe(observer)
                            let stateSet : Set<eRxSubscriptionState> = [.eEvaluating, .eConstructingEvalEngine, .eUnsubscribing, .eShutdown(nil)]

                            if !stateSet.contains(subscription.state)
                            {
                                XCTFail("Expected the subscription state to be eEvaluating")
                            }

                            TxDisposalTestRigs.waitForDisposal(subscription)

                            if case .eShutdown(let error) = subscription.state
                            {
                                XCTAssertTrue(error == nil, "Expected the subscription state to be eShutdown with no error")
                            }
                            else
                            {
                                XCTFail("Expected the subscription state to be eShutdown")
                            }
                        }

                        currentRunName = runName
                    }
                }

                runTest()

                trace(2, "= run end   ==> \(currentRunName) : \(String(describing: name)) <==\n\n")
            }
        }

        func subscriptionStateForObserverManagingSubscription()
        {
            for index in 0...7
            {
                var currentRunName : String = ""

                func runTest()
                {
                    do
                    {
                        let (runName, observable, _, _) = createRun(index)

                        let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("subscription test observer")

                        trace(2, "= run begin ==> \(runName) : \(String(describing: name)) <==\n\n")

                        do
                        {
                            let subscription = observable.subscribe(observer)
                            let stateSet : Set<eRxSubscriptionState> = [.eEvaluating, .eConstructingEvalEngine, .eUnsubscribing, .eShutdown(nil)]

                            if !stateSet.contains(subscription.state)
                            {
                                XCTFail("Expected the subscription state to be eEvaluating")
                            }

                            TxDisposalTestRigs.waitForDisposal(subscription)

                            if case .eShutdown(let error) = subscription.state
                            {
                                XCTAssertTrue(error == nil, "Expected the subscription state to be eShutdown with no error")
                            }
                            else
                            {
                                XCTFail("Expected the subscription state to be eShutdown")
                            }
                        }

                        currentRunName = runName
                    }
                }

                runTest()

                trace(2, "= run end   ==> \(currentRunName) : \(String(describing: name)) <==\n\n")
            }
        }

        func subscriptionFailureForObserver()
        {
            let observable = RxObservable<ItemType>(tag: "Unchained Observable")
            let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("subscription test observer")

            let expected = RxNotificationQueue<ItemType>(items: [])
            let reportFunc = reportGenerator("subscriptionFailureForObserver", expected, observer.notifications)

            let subscription = observable.subscribe(observer)

            if case .eShutdown(let error) = subscription.state
            {
                XCTAssertTrue(error == subscriptionFailureError, "Expected the subscription state to be eShutdown with error")
            }
            else
            {
                XCTFail("Expected the subscription state to be eShutdown")
            }

            TxDisposalTestRigs.waitForDisposal(subscription)

            XCTAssertTrue(expected == observer.notifications, reportFunc())
        }

        func subscriptionFailureForNoObserver()
        {
            let observable = RxObservable<ItemType>(tag: "Unchained Observable")

            let subscription = observable.subscribe()

            if case .eShutdown(let error) = subscription.state
            {
                XCTAssertTrue(error == subscriptionFailureError, "Expected the subscription state to be eShutdown with error")
            }
            else
            {
                XCTFail("Expected the subscription state to be eShutdown")
            }

            TxDisposalTestRigs.waitForDisposal(subscription)
        }

        func subscriptionFailureForObserverManagingSubscription()
        {
            let observable = RxObservable<ItemType>(tag: "Unchained Observable")
            let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("subscription test observer")

            let expected = RxNotificationQueue<ItemType>(items: [])
            let reportFunc = reportGenerator("subscriptionFailureForNoObserver", expected, observer.notifications)

            let subscription = observable.subscribe(observer)

            if case .eShutdown(let error) = subscription.state
            {
                XCTAssertTrue(error == subscriptionFailureError, "Expected the subscription state to be eShutdown with error")
            }
            else
            {
                XCTFail("Expected the subscription state to be eShutdown")
            }

            TxDisposalTestRigs.waitForDisposal(subscription)

            XCTAssertTrue(expected == observer.notifications, reportFunc())
        }

        func subscriptionFailureForCustomAction()
        {
            let observable = RxObservable<ItemType>(tag: "Unchained Observable")

            let subscription = observable.subscribe(itemAction: { (item : ItemType) in })

            if case .eShutdown(let error) = subscription.state
            {
                XCTAssertTrue(error == subscriptionFailureError, "Expected the subscription state to be eShutdown with error")
            }
            else
            {
                XCTFail("Expected the subscription state to be eShutdown")
            }

            TxDisposalTestRigs.waitForDisposal(subscription)
        }

        func subscriptionFailureForEvalOp()
        {
            let runName = "subscriptionFailureForEvalOp"

            trace(2, "= run begin ==> \(runName) : \(String(describing: name)) <==\n\n")

            let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("subscription test observer")
            let source = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let subscription = source.failEvalOp(39).subscribe(observer)

            if case .eShutdown(let error) = subscription.state
            {
                XCTAssertTrue(error == subscriptionFailureError, "Expected the subscription state to be eShutdown with error")
            }
            else
            {
                XCTFail("Expected the subscription state to be eShutdown")
            }

            TxDisposalTestRigs.waitForDisposal(subscription)

            trace(2, "= run end ==> \(runName) : \(String(describing: name)) <==\n\n")
        }

        let testObservableName = "subscription state"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            let defaultErrorHandler = RxSDK.error.handler

            RxSDK.error.handler = { _ in }

            subscriptionStateForObserver()
            subscriptionStateForObserverManagingSubscription()
            subscriptionStateForObserver()
            subscriptionFailureForObserver()
            subscriptionFailureForNoObserver()
            subscriptionFailureForObserverManagingSubscription()
            subscriptionFailureForCustomAction()
            subscriptionFailureForEvalOp()

            RxSDK.error.handler = defaultErrorHandler
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSubscriptionDeallocation()
    {
        let testObservableName = "subscription"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            for index in 0 ... 7
            {
                var currentRunName:               String = ""
                var currenExpectedEvalQueueCount: Int    = 0

                func runTest()
                {
                    do
                    {
                        let (runName, observable, expectedEvalQueueCount, expected) = createRun(index)

                        let observer   = TxTestRigs<ItemType>.fullCheckingTestObserver("subscription test observer")
                        let reportFunc = reportGenerator(runName, expected, observer.notifications)

                        trace(2, "= run begin ==> \(runName) : \(String(describing: name)) <==\n\n")

                        do
                        {
                            let subscription = observable.subscribe(observer)

                            TxDisposalTestRigs.waitForDisposal(subscription)
                        }

                        trace(2, "\n>>>>------------------------- disposal end\n\n")

                        XCTAssertTrue(expected == observer.notifications, reportFunc())

                        currentRunName = runName
                        currenExpectedEvalQueueCount = expectedEvalQueueCount
                    }
                }

                RxSDK.control.start()

                let instanceMon = RxMonInstances()

                let evalQueueMon = RxMonEvalQueue()

                RxMon.addMonitor("instanceMon", monitor: instanceMon)
                RxMon.addMonitor("evalQueueMon", monitor: evalQueueMon)

                runTest()

                trace(2, "\n>>>>------------------------- test end: \(instanceMon.objectCount) not deallocated\n\n")

                trace(2, "----- shutdown eval queues begin -----------------\n")
                RxSDK.evalQueue.stopAll()
                trace(2, "----- shutdown eval queues end   ----------------- \(instanceMon.objectCount) not deallocated\n")

                // Lock the monitor so that any subsequent hooks will be flagged.
                instanceMon.locked = true

                RxEvalQueueManager.shutdown()

                let report                = instanceMon.reportNonDeallocatedObjects()
                let deallocatedCount: Int = instanceMon.objectCount

                if deallocatedCount != 0
                {
                    trace(0, "\n>>>>------------------------- non deallocated begin \(deallocatedCount)\n")

                    trace(0, report)
                }

                // Check that all objects were deallocated.
                XCTAssertTrue(deallocatedCount == 0, report)

                // Remove monitors.
                RxMon.removeMonitor("instanceMon")
                RxMon.removeMonitor("evalQueueMon")

                trace(2, "= run end   ==> \(currentRunName) : \(String(describing: name)) <==\n\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSubscribeForNotificationDeallocation()
    {
        let testObservableName = "subscription"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            for index in 0...7
            {
                var currentRunName : String = ""
                var currenExpectedEvalQueueCount : Int = 0

                func runTest()
                {
                    let (runName, observable, expectedEvalQueueCount, expected) = createRun(index)

                    trace(2, "\n= run begin ==> \(runName) : \(String(describing: name)) <==\n")

                    let observer = TxTestRigs<ItemType>.simpleCheckingTestObserver("subscription test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    do
                    {
                        let notifier = RxNotifier_NotifyDelegates<ItemType>(directedToConsumer: observer, tag : "toObserver")

                        let subscription = observable.subscribe(notifier)

                        TxDisposalTestRigs.waitForDisposal(subscription)
                    }

                    trace(2, "\n>>>>------------------------- disposal end\n\n")

                    XCTAssertTrue(expected == observer.notifications, reportFunc())

                    currentRunName = runName
                    currenExpectedEvalQueueCount = expectedEvalQueueCount
                }

                RxSDK.control.start()

                let instanceMon = RxMonInstances()
                let evalQueueMon = RxMonEvalQueue()

                RxMon.addMonitor("instanceMon", monitor: instanceMon)
                RxMon.addMonitor("evalQueueMon", monitor: evalQueueMon)

                runTest()

                trace(2, "\n>>>>------------------------- test end: \(instanceMon.objectCount) not deallocated\n\n")


                //RxSDK.shutdown()

                trace(2, "----- shutdown eval queues begin -----------------\n")
                RxSDK.evalQueue.stopAll()
                trace(2, "----- shutdown eval queues end   ----------------- \(instanceMon.objectCount) not deallocated\n")

                // Lock the monitor so that any subsequent hooks will be flagged.
                instanceMon.locked = true

                if evalQueueMon.createdEvaluationRxEvalQueueCount != currenExpectedEvalQueueCount
                {
                    trace(0, evalQueueMon.reportEvaluationRxEvalQueueUsage())
                }

                // Check the expected created evalution RxEvalQueue count.
                XCTAssertTrue(evalQueueMon.createdEvaluationRxEvalQueueCount == currenExpectedEvalQueueCount, "Expected only one EvalQueue to be created.")

                RxEvalQueueManager.shutdown()

                let report = instanceMon.reportNonDeallocatedObjects()
                let deallocatedCount : Int = instanceMon.objectCount

                if deallocatedCount != 0
                {
                    trace(0, "\n>>>>------------------------- non deallocated begin \(deallocatedCount)\n")

                    trace(0, report)
                }

                // Check that all objects were deallocated.
                XCTAssertTrue(deallocatedCount == 0, "\n\n-------- \(currentRunName) -----\n\(report)")

                // Remove monitors.
                RxMon.removeMonitor("instanceMon")
                RxMon.removeMonitor("evalQueuMon")

                // Shutdown Monitors.
                RxMon.shutdown()

                trace(2, "= run end   ==> \(currentRunName) : \(String(describing: name)) <==\n\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func createSourceRunSet(_ subscriptionType : eRxSubscriptionType) -> [SourceRunEntry]
    {
        let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], subscriptionType: subscriptionType)
        let source2 = RxSource<ItemType>.fromArray([110, 121, 135, 144, 150])

        let run1: SourceRunEntry = (

                runName:        "Normal Source",
                source:         source1,
                expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
        )

        let run2: SourceRunEntry = (

                runName:        "Empty Source",
                source:         source2,
                expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
        )

        return [

                run1,
                run2
        ]
    }
}
