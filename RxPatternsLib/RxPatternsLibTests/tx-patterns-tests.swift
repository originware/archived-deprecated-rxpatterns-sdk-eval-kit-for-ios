// tx-patterns-tests.swift
// RxPatternsLib Tests
//
//  Created by Terry Stillone on 14/03/2015.
//  Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import XCTest

import RxPatternsSDK
@testable import RxPatternsLib


class RxObservablePatternsTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testRelayPat()
    {
        let testObservableName = "relayPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias TxPatternTestTupleType = (Double, String)

            typealias ItemType = [Double]
            typealias RxEvalOp = RxTypes2<ItemType, ItemType>.RxEvalOp
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, evalOp:RxEvalOp, expectedResult:RxNotificationQueue<ItemType>)


            func createNormalEvalOp() -> RxEvalOp
            {
                return { (evalNode: RxEvalNode<ItemType, ItemType>) in

                    evalNode.itemDelegate = { (item: ItemType, notifier: ARxNotifier<ItemType>) in

                        notifier.notify(item: item)
                    }

                    evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemType>) in

                        notifier.notifyCompleted(error: error)

                    }
                }
            }

            func createNoDelegateEvalOp() -> RxEvalOp
            {
                return { (evalNode: RxEvalNode<ItemType, ItemType>) in }
            }

            func createNoPassEvalOp() -> RxEvalOp
            {
                return { (evalNode: RxEvalNode<ItemType, ItemType>) in

                    evalNode.itemDelegate = { (item: ItemType, notifier: ARxNotifier<ItemType>) in }
                }
            }

            func createMapCompletedToErrorEvalOp() -> RxEvalOp
            {
                let localError = error

                return { (evalNode: RxEvalNode<ItemType, ItemType>) in

                    evalNode.itemDelegate = { (item: ItemType, notifier: ARxNotifier<ItemType>) in }

                    evalNode.completedDelegate = { (errorArg: IRxError?, notifier: ARxNotifier<ItemType>) in

                        notifier.notifyCompleted(error: localError)

                    }
                }
            }

            func createMapErrorToCompletedEvalOp() -> RxEvalOp
            {
                return { (evalNode: RxEvalNode<ItemType, ItemType>) in

                    evalNode.completedDelegate = { (errorArg: IRxError?, notifier: ARxNotifier<ItemType>) in

                        notifier.notifyCompleted()

                    }
                }
            }

            func createMapItemsdEvalOp() -> RxEvalOp
            {
                return { (evalNode: RxEvalNode<ItemType, ItemType>) in

                    evalNode.itemDelegate = { (item: ItemType, notifier: ARxNotifier<ItemType>) in

                        notifier.notify(item: item + [22])
                    }
                }
            }

            let baseItemList: [ItemType] = [[1.0, 2.0], [4.0], [2.3, 3.3, 4.0], [], [223.1, 3.1]]
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray(baseItemList)
            let source1WithError = RxSource<ItemType>.fromArray(baseItemList, termination: eTerminationType.eError(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: eTerminationType.eError(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    evalOp: createNormalEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemList)
            )


            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    evalOp: createNormalEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    evalOp: createNormalEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(items: baseItemList, termination: .eError(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    evalOp: createNormalEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eError(error))
            )

            let run5: RunEntry = (

                    runName: "Test No Delegate Builder",
                    source: source1,
                    evalOp: createNoDelegateEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemList)
            )

            let run6: RunEntry = (

                    runName: "Test Items No Pass Builder",
                    source: source1,
                    evalOp: createNoPassEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run7: RunEntry = (

                    runName: "Test Map Completed to Error Builder",
                    source: source1,
                    evalOp: createMapCompletedToErrorEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eError(error))
            )

            let run8: RunEntry = (

                    runName: "Test Map Error to Completed Builder",
                    source: source1WithError,
                    evalOp: createMapErrorToCompletedEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemList)
            )

            let run9: RunEntry = (

                    runName: "Test map Items Builder",
                    source: source1,
                    evalOp: createMapItemsdEvalOp(),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [[1.0, 2.0, 22.0], [4.0, 22.0], [2.3, 3.3, 4.0, 22.0], [22.0], [223.1, 3.1, 22.0]])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8,
                    run9
            ]
            {
                let (runName, source, evalOp, expectedResult) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.relayPat(tag: runName, evalOp: evalOp)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testExtractPat()
    {
        let testObservableName = "extractPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([1.0, 4.5, 2.2, 0.0, 223.1])
            let source1WithError = RxSource<ItemType>.fromArray([1.0, 4.5, 2.2, 0.0, 223.1], termination: .eError(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eError(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [1.0, 4.5, 2.2, 0.0, 223.1])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [1.0, 4.5, 2.2, 0.0, 223.1], termination: .eError(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eError(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let resultQueue = RxNotificationQueue<ItemType>(tag: "result")

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.extractPat(tag: runName, extractConsumer: resultQueue)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expectedResult, resultQueue)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == resultQueue, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func NotImpl_testGateStreamsPat()
    {
        // To be implemented
    }

    func NotImpl_testGateStreamsInlinePat()
    {
        // To be implemented
    }
}

class RxSourcePatternsTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testSyncGenPat()
    {
        let testObservableName = "syncGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxTime
            typealias RunEntry = (runName:String, evalOp: RxTypes<ItemType>.RxEvalOp, expectedResult:RxNotificationQueue<ItemType>)

            let now = RxTime()

            let run1: RunEntry = (

                    runName: "Test empty, no completion",
                    evalOp: RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        return eRxSyncGenCommand.eTerminate
                    }),
                    expectedResult: RxNotificationQueue<ItemType>(items: [])
            )

            let run2: RunEntry = (

                    runName: "Test empty, with completion",
                    evalOp: RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        notifier.notifyCompleted(error: nil)
                        return eRxSyncGenCommand.eStopTicking
                    }),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test one item, no completion",
                    evalOp: RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        notifier.notify(item: now)
                        return eRxSyncGenCommand.eTerminate
                    }),
                    expectedResult: RxNotificationQueue<ItemType>(items: [now])
            )

            let run4: RunEntry = (

                    runName: "Test one item, with completion",
                    evalOp: RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: now)
                                return eRxSyncGenCommand.eNextTick

                            case 1:
                                notifier.notifyCompleted()
                                return eRxSyncGenCommand.eStopTicking

                            default:
                                XCTFail("Unexpected index")
                                return eRxSyncGenCommand.eTerminate
                        }
                    }),
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [now])
            )

            let run5: RunEntry = (

                    runName: "Test one item, with error",
                    evalOp: RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: now)
                                return eRxSyncGenCommand.eNextTick

                            case 1:
                                notifier.notifyCompleted(error: self.error)
                                return eRxSyncGenCommand.eStopTicking

                            default:
                                XCTFail("Unexpected index")
                                return eRxSyncGenCommand.eTerminate
                        }
                    }),
                    expectedResult: RxNotificationQueue<ItemType>(items: [now], termination: .eError(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ]
            {
                let (runName, evalOp, expectedResult) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource<ItemType>(tag: runName, subscriptionType : .eCold, evalOp: evalOp)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testAsyncGenPat()
    {
        let testObservableName = "asyncGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, evalOp: RxTypes<ItemType>.RxEvalOp, expectedResult:RxNotificationSequence<ItemType>)

            let t : RxTimeOffset = TxConfig.TestTickUnit


            let run1: RunEntry = (

                    runName: "Test empty, no completion",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        return eRxAsyncGenCommand.eTerminateSource
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItems: [])
            )

            let run2: RunEntry = (

                    runName: "Test empty, with completion",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        notifier.notifyCompleted()
                        return eRxAsyncGenCommand.eStopTicking
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test one item, no completion",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        if index != 0
                        {
                            XCTFail("Unexpected index")
                        }

                        notifier.notify(item: 11)
                        return eRxAsyncGenCommand.eTerminateSource
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItems: [(0, 11)])
            )

            let run4: RunEntry = (

                    runName: "Test one item, with completion",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: 22)
                                return eRxAsyncGenCommand.eNextTickAt(t)

                            case 1:
                                notifier.notifyCompleted()
                                return eRxAsyncGenCommand.eStopTicking

                            default:
                                XCTFail("Unexpected index")
                                return eRxAsyncGenCommand.eStopTicking
                        }
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 22)])
            )

            let run5: RunEntry = (

                    runName: "Test one item, with completion not returning Stop Tick",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: 22)
                                return eRxAsyncGenCommand.eNextTickAt(t)

                            case 1:
                                notifier.notifyCompleted()
                                return eRxAsyncGenCommand.eNextTickAt(t * 2)

                            default:
                                XCTFail("Unexpected index")
                                return eRxAsyncGenCommand.eTerminateSource
                        }
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 22)])
            )

            let run6: RunEntry = (

                    runName: "Test one item, with error",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: 44)
                                return eRxAsyncGenCommand.eNextTickAt(t)

                            case 1:
                                notifier.notifyCompleted(error: self.error)
                                return eRxAsyncGenCommand.eStopTicking

                            default:
                                XCTFail("Unexpected index")
                                return eRxAsyncGenCommand.eTerminateSource
                        }
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItems: [(t, 44)], termination: eTerminationType.eError(error))
            )

            let run7: RunEntry = (

                    runName: "Test multi items with completion",
                    evalOp: RxEvalOps.asyncGenPat(true, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        switch index
                        {
                            case 0:
                                notifier.notify(item: 22)
                                return eRxAsyncGenCommand.eNextTickAt(t)

                            case 1:
                                notifier.notify(item: 33)
                                return eRxAsyncGenCommand.eNextTickAt(t * 2)

                            case 2:
                                notifier.notifyCompleted()
                                return eRxAsyncGenCommand.eStopTicking

                            default:
                                XCTFail("Unexpected index")
                                return eRxAsyncGenCommand.eTerminateSource

                        }
                    }),
                    expectedResult: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 22), (t * 2, 33)])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7
            ]
            {
                let (runName, evalOp, expectedResult) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource<ItemType>(tag: runName, subscriptionType : .eCold, evalOp: evalOp)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func redirectPat_singleSubscriptionTest<ItemType>(_ subscriptionType : eRxSubscriptionType, timingBoard : TxTimingBoard<ItemType>)
    {
        let testObservableName = "redirectPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias RunEntry = (runName:String, matchTags: Set<Int>)

            let run1: RunEntry = (

                    runName:               "Test normal item operation",
                    matchTags:             [1]
            )

            let run2: RunEntry = (

                    runName:               "Test error operation",
                    matchTags:             [2]
            )

            let run3: RunEntry = (

                    runName:                "Test empty source",
                    matchTags:             [3]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3
            ] {
                let (runName, matchTags) = runSpec

                var tickNotifierInstanceID : RxInstanceID = -1
                var instanceMon : RxMonInstances? = nil
                var usingNotifierInstanceID : RxInstanceID = -1

                func createTickNotifier() -> TxTickNotifier<ItemType>
                {
                    instanceMon = RxMonInstances()

                    RxMon.addMonitor("instanceMon", monitor: instanceMon!)

                    let tickNotifier = TxTickNotifier<ItemType>(timingBoard: timingBoard)

                    tickNotifierInstanceID = tickNotifier.instanceID

                    XCTAssertTrue(instanceMon!.objectIsAlive(tickNotifierInstanceID), "Expected ticknotifier to be alive")

                    return tickNotifier
                }

                func runTest()
                {
                    let expectedResult = timingBoard.extractTimedNotifications(matchTags)
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    do
                    {
                        let _ = RxSDK.mon.startTraceSession(startTime: RxRelTime.now, traceTag: ">>", bufferTraceMessages: true)

                        let tickNotifier = createTickNotifier().traceSelf(">>")

                        tickNotifier.using({ (usingNotifier: RxNotifier_NotifyConsumers<ItemType>) in

                            let observable = RxSource.redirectPat(tag: runName, subscriptionType: subscriptionType, notifier: usingNotifier)
                            let subscription = observable.deferredSubscribe(observer)

                            usingNotifierInstanceID = usingNotifier.instanceID

                            tickNotifier.start(matchTags)

                            subscription.runAsync()

                            TxDisposalTestRigs.waitForDisposal(subscription)

                            XCTAssertTrue(0 == usingNotifier.count, "Expected using notifier to have been removed from the observable expression")
                        })

                        XCTAssertEqual(0, tickNotifier.count, "Expected tick notifier to have been removed from the observable expression")

                        tickNotifier.stop()
                    }

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())

                    #if RxMonEnabled

                        if let mon = RxSDK.mon.trace
                        {
                            if instanceMon!.objectIsAlive(tickNotifierInstanceID)
                            {
                                RxLog.log(">>> tickNotifier is still alive ----- \n")
                                trace(0, mon.traceMessageBuffer)
                            }

                            if expectedResult != observer.notifications
                            {
                                RxLog.log(reportFunc())
                                trace(0, mon.traceMessageBuffer)
                            }

                            mon.clearTraceBuffer()
                        }
                    #endif

                    XCTAssertTrue(!instanceMon!.objectIsAlive(tickNotifierInstanceID), "Expected tick notifier to be destroyed")
                    XCTAssertTrue(!instanceMon!.objectIsAlive(usingNotifierInstanceID), "Expected using notifier to have been destroyed")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")

                RxMon.shutdown()
            }
        }

        runAll()
    }

    func redirectPat_multiSubscriptionTest<ItemType>(_ subscriptionType : eRxSubscriptionType, timingBoard : TxTimingBoard<ItemType>)
    {
        let testObservableName = "redirectPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias RunEntry = (runName:String, matchTags : Set<Int>)

            let run1: RunEntry = (

                    runName:                   "Test normal item operation",
                    matchTags:                 [1]
            )

            let run2: RunEntry = (

                    runName:                   "Test error operation",
                    matchTags:                 [2]
            )

            let run3: RunEntry = (

                    runName:                   "Test empty sources",
                    matchTags:                 [3]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3
            ] {
                let (runName, matchTags) = runSpec

                var tickNotifierInstanceID : RxInstanceID = -1
                var usingNotifierInstanceID : RxInstanceID = -1
                var instanceMon : RxMonInstances? = nil

                func createTickNotifier() -> TxTickNotifier<ItemType>
                {
                    instanceMon = RxMonInstances()

                    RxMon.addMonitor("instanceMon", monitor: instanceMon!)

                    let tickNotifier = TxTickNotifier<ItemType>(timingBoard: timingBoard)

                    tickNotifierInstanceID = tickNotifier.instanceID

                    XCTAssertTrue(instanceMon!.objectIsAlive(tickNotifierInstanceID), "Expected ticknotifier to be alive")

                    return tickNotifier
                }

                func runTest()
                {
                    do
                    {
                        let observer1      = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
                        let observer2      = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")
                        let expectedResult = timingBoard.extractTimedNotifications(matchTags)

                        let reportFunc1 = reportGenerator(runName, expectedResult, observer1.notifications)
                        let reportFunc2 = reportGenerator(runName, expectedResult, observer2.notifications)

                        do
                        {
                            let _ = RxSDK.mon.startTraceSession(startTime: RxRelTime.now, traceTag: ">>", bufferTraceMessages: true)

                            let tickNotifier = createTickNotifier().traceSelf("<<")

                            tickNotifier.using({ (usingNotifier: RxNotifier_NotifyConsumers<ItemType>) in

                                let observable = RxSource.redirectPat(tag: runName, subscriptionType : subscriptionType, notifier : usingNotifier).traceAll(">>", bufferTraceMessages: true)

                                usingNotifierInstanceID = usingNotifier.instanceID

                                XCTAssertTrue(instanceMon!.objectIsAlive(tickNotifierInstanceID), "Expected ticknotifier to be alive")

                                let subscription1 = observable.deferredSubscribe(observer1)
                                let subscription2 = observable.deferredSubscribe(observer2)

                                tickNotifier.start(matchTags)

                                subscription1.runAsync()
                                subscription2.runAsync()

                                TxDisposalTestRigs.waitForDisposal(subscription1)
                                TxDisposalTestRigs.waitForDisposal(subscription2)

                                XCTAssertTrue(0 == usingNotifier.count, "Expected using notifier to have been removed from the observable expression")
                            })

                            XCTAssertTrue(0 == tickNotifier.count, "Expected tick notifier to have been removed from the observable expression")

#if RxMonEnabled
                            if let mon = RxSDK.mon.trace , instanceMon!.objectIsAlive(usingNotifierInstanceID)
                            {
                                RxLog.log(mon.traceMessageBuffer)
                            }
#endif

                            XCTAssertTrue(!instanceMon!.objectIsAlive(usingNotifierInstanceID), "Expected using notifier to have been destroyed")
                        }

                        XCTAssertTrue(expectedResult == observer1.notifications, reportFunc1())
                        XCTAssertTrue(expectedResult == observer2.notifications, reportFunc2())
                    }

                    //RxTrace.trace("----------- destruct 2 ----------------------------\n")

                    #if RxMonEnabled
                        if let mon = RxSDK.mon.trace
                        {
                            if !mon.isEmpty
                            {
                                RxLog.log(mon.traceMessageBuffer)
                            }

                            mon.clearTraceBuffer()
                        }
                    #endif

                    XCTAssertTrue(!instanceMon!.objectIsAlive(tickNotifierInstanceID), "Expected tick notifier to have been destroyed")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")

                RxMon.shutdown()
            }
        }

        runAll()
    }

    func testReDirectPat()
    {
        let testObservableName = "redirectPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TickType = ItemType

            let t : RxTimeOffset = TxConfig.TestShortTickUnit * 2
            let timingArray : TxTimingBoard<ItemType>.TimingArrayType = [

                    (t,         1,  .eItem("1-0")),
                    (t * 2,     2,  .eItem("2-0")),
                    (t * 3,     1,  .eItem("1-1")),
                    (t * 4,     3,  .eCompleted(.eCompleted)),
                    (t * 5,     2,  .eItem("2-1")),
                    (t * 6,     2,  .eItem("2-2")),
                    (t * 7,     1,  .eItem("1-2")),
                    (t * 8,     3,  .eTerm),
                    (t * 9,     1,  .eItem("1-3")),
                    (t * 10,    1,  .eCompleted(.eCompleted)),
                    (t * 11,    2,  .eCompleted(.eError(error))),
                    (t * 12,    1,  .eTerm),
                    (t * 13,    2,  .eTerm)

            ]

            let testMatrix = TxTestMatrix<ItemType>(tag : testName, timingArray : timingArray)

            for test in testMatrix.allTests
            {
                do
                {
                    redirectPat_singleSubscriptionTest(test.subscriptionType, timingBoard : test.timingBoard)
                    redirectPat_multiSubscriptionTest(test.subscriptionType, timingBoard : test.timingBoard)
                }
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func tickGenPat_singleSubscriptionTest<ItemType>(_ subscriptionType : eRxSubscriptionType, timingBoard : TxTimingBoard<ItemType>)
    {
        let testObservableName = "tickGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias TickType = (timeOffset : RxTimeOffset, index : RxIndexType, notification : RxNotification<ItemType>)
            typealias RunEntry = (runName:String, matchTags : Set<Int>)

            let t : RxTimeOffset = TxConfig.TestTickUnit
            var timerTagCounter : RxIndexType = 0

            func createTickGenerator(_ tags : Set<Int>, _ timerTag : RxIndexType, _ timingBoard : TxTimingBoard<ItemType>) -> RxNotifier_NotifyConsumers<TickType>
            {
                let notifier = RxNotifier_NotifyConsumers_Sync<TickType>(tag : "tick generator-\(timerTag)", queuer: RxSDK.evalQueue.Queuer)
                let timer = RxTimer(tag: "tick notification generator")
                let timings = timingBoard.extractTimedItems(tags)

                let _ = timer.addTickGenerator(RxTime(), generator: { (index: RxIndexType) -> RxTimeOffset? in

                    if index == 0
                    {
                        return timings.first!.timeOffset
                    }

                    let (timeOffset, _, timingNotifyType) = timings[index - 1]

                    switch timingNotifyType
                    {
                        case .eItem:
                            notifier.notify(item: (timeOffset, index, timingNotifyType.toNotification()))

                        case .eCompleted(let termination):
                            notifier.notify(termination: termination)
                            return nil

                        case .eTerm:
                            timer.cancelAll()
                            return nil
                    }

                    return timings[index].timeOffset
                })

                return notifier
            }

            func createNotificationGenerator() -> (_ tick : TickType, _ notifier : ARxNotifier<ItemType>) -> Bool
            {
                return { (tick : TickType, notifier : ARxNotifier<ItemType>) -> Bool in

                    switch tick.notification.notifyType
                    {
                        case .eNotification_Item(let item):
                            notifier.notify(item: item)
                            return true

                        case .eNotification_Termination(let termination):
                            notifier.notify(termination: termination)
                            return false
                    }
                }
            }

            let run1: RunEntry = (

                    runName:                "Test normal item operation",
                    matchTags:             [1]
            )

            let run2: RunEntry = (

                    runName:                "Test error operation",
                    matchTags:             [2]
            )

            let run3: RunEntry = (

                    runName:                "Test empty source",
                    matchTags:             [3]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3
            ] {
                let (runName, matchTags) = runSpec

                func runTest()
                {
                    let notificationGenerator = createNotificationGenerator()
                    let tickNotifier = createTickGenerator(matchTags, timerTagCounter, timingBoard)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource.tickGenPat(tag: runName, subscriptionType : subscriptionType, tickNotifier : tickNotifier, generator : notificationGenerator)
                    let subscription = observable.subscribe(observer)

                    let expectedResult = timingBoard.extractTimedNotifications(matchTags)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    timerTagCounter += 1

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                    XCTAssertEqual(0, tickNotifier.count, "Expected tick notifier to have been removed from the observable expression")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for _ in 0..<1
        {
            runAll()
        }
    }

    func tickGenPat_multiSubscriptionTest<ItemType>(_ subscriptionType : eRxSubscriptionType, timingBoard : TxTimingBoard<ItemType>)
    {
        let testObservableName = "tickGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias TickType = (timeOffset : RxTimeOffset, index : RxIndexType, notification : RxNotification<ItemType>)
            typealias RunEntry = (runName:String, matchTags : Set<Int>)

            let testObservableName = "tickGenPat"
            let testName = TxFactory.createTestName(#function)

            let t : RxTimeOffset = TxConfig.TestTickUnit
            var timerTagCounter : RxIndexType = 0

            func createTickGenerator(_ tags : Set<Int>, _ timerTag : RxIndexType, _ timingBoard : TxTimingBoard<ItemType>) -> RxNotifier_NotifyConsumers<TickType>
            {
                let notifier = RxNotifier_NotifyConsumers_Sync<TickType>(tag : "tick generator-\(timerTag)", queuer: RxSDK.evalQueue.Queuer)
                let timer = RxTimer(tag: "tick notification generator")
                let timings = timingBoard.extractTimedItems(tags)

                let _ = timer.addTickGenerator(RxTime(), generator: { (index: RxIndexType) -> RxTimeOffset? in

                    if index == 0
                    {
                        return timings.first!.timeOffset
                    }

                    let (timeOffset, _, timingNotifyType) = timings[index - 1]

                    switch timingNotifyType
                    {
                        case .eItem:
                            notifier.notify(item: (timeOffset, index, timingNotifyType.toNotification()))

                        case .eCompleted(let termination):
                            notifier.notify(termination: termination)
                            return nil

                        case .eTerm:
                            timer.cancelAll()
                            return nil
                    }

                    return timings[index].timeOffset
                })

                return notifier
            }

            func createNotificationGenerator() -> (_ tick : TickType, _ notifier : ARxNotifier<ItemType>) -> Bool
            {
                return { (tick : TickType, notifier : ARxNotifier<ItemType>) -> Bool in

                    switch tick.notification.notifyType
                    {
                        case .eNotification_Item(let item):
                            notifier.notify(item: item)
                            return true

                        case .eNotification_Termination(let termination):
                            notifier.notify(termination: termination)
                            return false
                    }
                }
            }

            let run1: RunEntry = (

                    runName:               "Test normal item operation",
                    matchTags:             [1]
            )

            let run2: RunEntry = (

                    runName:               "Test error operation",
                    matchTags:             [2]
            )

            let run3: RunEntry = (

                    runName:               "Test empty source",
                    matchTags:             [3]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3
            ] {
                let (runName, matchTags) = runSpec

                func runTest()
                {
                    let notificationGenerator = createNotificationGenerator()
                    let tickNotifier = createTickGenerator(matchTags, timerTagCounter, timingBoard)

                    let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
                    let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")
                    let observable = RxSource.tickGenPat(tag: runName, subscriptionType : subscriptionType, tickNotifier : tickNotifier, generator : notificationGenerator)

                    let expectedResult = timingBoard.extractTimedNotifications(matchTags)
                    let reportFunc1 = reportGenerator(runName, expectedResult, observer1.notifications)
                    let reportFunc2 = reportGenerator(runName, expectedResult, observer2.notifications)

                    let subscription1 = observable.deferredSubscribe(observer1)
                    let subscription2 = observable.deferredSubscribe(observer2)

                    subscription1.runAsync()
                    subscription2.runAsync()

                    timerTagCounter += 1

                    TxDisposalTestRigs.waitForDisposal(subscription1)
                    TxDisposalTestRigs.waitForDisposal(subscription2)

                    XCTAssertTrue(expectedResult == observer1.notifications, reportFunc1())
                    XCTAssertTrue(expectedResult == observer2.notifications, reportFunc2())

                    XCTAssertEqual(0, tickNotifier.count, "Expected tick notifier to have been removed from the observable expression")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for _ in 0..<1
        {
            runAll()
        }
    }

    func testTickGenPat()
    {
        let testObservableName = "tickGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TickType = ItemType

            let testName = TxFactory.createTestName(#function)

            let t : RxTimeOffset = TxConfig.TestTickUnit
            let timingArray : TxTimingBoard<ItemType>.TimingArrayType = [

                    (t,         1,  .eItem("1-0")),
                    (t * 2,     2,  .eItem("2-0")),
                    (t * 3,     1,  .eItem("1-1")),
                    (t * 4,     3,  .eCompleted(.eCompleted)),
                    (t * 5,     2,  .eItem("2-1")),
                    (t * 6,     2,  .eItem("2-2")),
                    (t * 7,     1,  .eItem("1-2")),
                    (t * 8,     3,  .eTerm),
                    (t * 9,     1,  .eItem("1-3")),
                    (t * 10,    1,  .eCompleted(.eCompleted)),
                    (t * 11,    2,  .eCompleted(.eError(error))),
                    (t * 12,    1,  .eTerm),
                    (t * 13,    2,  .eTerm)
            ]

            let testMatrix = TxTestMatrix<ItemType>(tag : testName, timingArray : timingArray)

            for test in testMatrix.allTests
            {
                tickGenPat_singleSubscriptionTest(test.subscriptionType, timingBoard : test.timingBoard)
                tickGenPat_multiSubscriptionTest(test.subscriptionType, timingBoard : test.timingBoard)
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(TxConfig.IterationCount, iteration)
            let isTenPercentile = isInTenPercentile(TxConfig.IterationCount, iteration)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}
