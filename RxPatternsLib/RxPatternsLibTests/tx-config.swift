// tx-config.swift
// RxPatternsLib Tests
//
//  Created by Terry Stillone on 9/09/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

///
/// TxConfig: The configuration of the RxPatterns Unit tests.
///

struct TxConfig
{
    /// The default timeout for waiting for a subscription to dispose.
    static let DisposalTimeoutSec: RxDuration = 30

    /// The tick testing time unit.
    static let TestTickUnit:       RxDuration = 0.005

    /// The short tick testing time unit for unit tests that can run faster.
    static let TestShortTickUnit:  RxDuration = 0.001

    /// The iteration count for unit tests.
    static let IterationCount:     Int        = 10

    /// The Level Of Detail threshold to display trace messages.
    static var TraceThreshold:     Int         { return IterationCount <= 5 ? 3 : 0 }
}


