//
//  RxPatternsSDK.h
//  RxPatternsSDK
//
//  Created by Terry Stillone on 14/08/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <stdatomic.h>

//! Project version number for RxPatternsSDK.
FOUNDATION_EXPORT double RxPatternsSDKVersionNumber;

//! Project version string for RxPatternsSDK.
FOUNDATION_EXPORT const unsigned char RxPatternsSDKVersionString[];
