// ipx-expr.swift
// RxPatternsSDK
//
// Created by Terry Stillone (http://www.originware.com) on 1/09/2015.
// Copyright © 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import Dispatch

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxProducer: A Producer of Notifications (See IRxConsumer).
///
public protocol IRxProducer
{
    associatedtype ItemType

    /// Subscribe to an observable expression with nothing required to observe the result (async operation).
    /// - Returns: The subscription.
    func subscribe() -> RxSubscription

    /// Subscribe a consumer to observable expression (async operation).
    /// - Parameter consumer: The consumer that is to observe the Notifications from the Observable.
    /// - Returns: The subscription.
    func subscribe(_ consumer: ARxConsumer<ItemType>) -> RxSubscription

    /// Subscribe actions to an observable expression (async operation).
    /// - Parameter itemAction: The action to perform on Item notification.
    /// - Parameter completedAction: The action to perform on completed notification.
    /// - Returns: The RxSubscription for the subscription.
    func subscribe(
            itemAction: @escaping RxTypes<ItemType>.RxItemActionDelegate,
            completedAction : RxTypes<ItemType>.RxCompletedActionDelegate?
    ) -> RxSubscription

    /// Subscribe actions with notifiers to an observable expression(async operation).
    /// - Parameter itemNotifier: The notifier be notified on Item notification.
    /// - Parameter completedNotifier: The notifier be notified on completed notification.
    /// - Returns: The subscription.
    func subscribe(
            itemNotifier : @escaping RxTypes<ItemType>.RxItemActionDelegate,
            completedNotifier : RxTypes<ItemType>.RxCompletedActionDelegate?
    ) -> RxSubscription
}

///
/// ARxProducer: The abstract base class for Notification Producers. All producer classes should inherit from this class.
///
open class ARxProducer<ItemType> : RxObject, IRxProducer
{
    /// The configuration of operational tracing for the producer.
    internal final var m_traceConfig: RxTraceConfig?

    /// Initialise with tag and description.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter description: The optional description for this instance.
    public override init(tag : String, description : String? = nil)
    {
        #if RxTraceEnabled
            self.m_traceConfig = nil
        #endif

        super.init(tag: tag, description: description)
    }

    /// Clone from the given ARxProducer with the given tag.
    /// - Parameter producer: The Producer to clone.
    /// - Parameter tag: The RxObject tag for this instance.
    public init?(clone producer: ARxProducer, tag: String)
    {
        #if RxMonEnabled
            self.m_traceConfig = producer.m_traceConfig
        #endif
        
        super.init(clone: producer, tag : tag)
    }

    /// Subscribe to an observable expression with nothing required to observe the result (async operation).
    /// - Returns: The subscription.
    open func subscribe() -> RxSubscription
    {
        // do nothing

        return RxSubscription(tag: "Failed Subscription", error: RxError(.eSubscriptionFailure("subscribe method not overridden")))
    }

    /// Subscribe a consumer to an observable expression (async operation).
    /// - Parameter consumer: The consumer that is to observe the Notifications from the Observable.
    /// - Returns: The subscription.
    open func subscribe(_ consumer : ARxConsumer<ItemType>) -> RxSubscription
    {
        // do nothing

        return RxSubscription(tag: "Failed Subscription", error: RxError(.eSubscriptionFailure("subscribe method not overridden")))
    }

    /// Subscribe an observer that manages its subscriptions to an observable expression (async operation).
    /// - Parameter observer: The observer that is to observe the Notifications from the Observable.
    /// - Returns: The subscription.
    open func subscribe(_ observer : ARxObserverManagingSubscription<ItemType>) -> RxSubscription
    {
        // do nothing

        return RxSubscription(tag: "Failed Subscription", error: RxError(.eSubscriptionFailure("subscribe method not overridden")))
    }

    /// Subscribe actions to an observable expression (async operation).
    /// - Parameter itemAction: The action to perform on Item notification.
    /// - Parameter completedAction: The action to perform on completed notification.
    /// - Returns: The RxSubscription for the subscription.
    open func subscribe(
            itemAction: @escaping RxTypes<ItemType>.RxItemActionDelegate,
            completedAction : RxTypes<ItemType>.RxCompletedActionDelegate?
    ) -> RxSubscription
    {
        // do nothing

        return RxSubscription(tag: "Failed Subscription", error: RxError(.eSubscriptionFailure("subscribe method not overridden")))
    }

    /// Subscribe actions to an observable expression(async operation).
    /// - Parameter itemNotifier: The notifier be notified on Item notification.
    /// - Parameter completedNotifier: The notifier be notified on completed notification.
    /// - Returns: The subscription.
    open func subscribe(
            itemNotifier : @escaping RxTypes<ItemType>.RxItemActionDelegate,
            completedNotifier : RxTypes<ItemType>.RxCompletedActionDelegate?
    ) -> RxSubscription
    {
        // do nothing

        return RxSubscription(tag: "Failed Subscription", error: RxError(.eSubscriptionFailure("subscribe method not overridden")))
    }

    /// Return an enumerable of the items from the producer.
    open func toEnumerable() -> TRxEnumerable<ItemType>
    {
        typealias ResultType = RxNotificationQueue<ItemType>

        let tag = "toEnumerable"
        let notificationQueue = RxNotificationQueue<ItemType>(tag : tag)
        let subscription = subscribe(notificationQueue)

        // collect all the notifications and then return the result.
        let _ = subscription.waitForDisposal()

        return TRxEnumerable(notificationQueue)
    }

    /// Trace all objects and expressions associated with this producer.
    /// - Parameter traceTag: The trace line title string when displaying trace messages.
    /// - Parameter bufferTraceMessages: Indicate to buffer trace messages so as to give a better timing performance view in the message timestamps.
    open func doTraceAll(_ traceTag: String, bufferTraceMessages: Bool = false)
    {
        #if RxMonEnabled
            if self.traceTag == nil
            {
                if RxSDK.mon.trace == nil
                {
                    let _ = RxSDK.mon.startTraceSession(startTime: nil, traceTag: traceTag, bufferTraceMessages: bufferTraceMessages)
                }

                m_traceConfig = RxTraceConfig(traceTag: traceTag, bufferTraceMessages: bufferTraceMessages)

                trace(traceTag)
            }
        #endif
    }

    /// Trace all objects and expressions associated with this producer.
    /// - Parameter traceTag: The trace line title string when displaying trace messages.
    /// - Parameter bufferTraceMessages: Indicate to buffer trace messages so as to give a better timing performance view in the message timestamps.
    /// - Returns: self.
    open func traceAll(_ traceTag: String, bufferTraceMessages: Bool = false) -> Self
    {
        doTraceAll(traceTag, bufferTraceMessages: bufferTraceMessages)

        return self
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxSource: An IRxProducer which is not pre-composable.
///
///    A standalone observable which is a source of notifications.
///

public protocol IRxSource : IRxProducer
{
    /// The type of subscription (hot, cold).
    var subscriptionType : eRxSubscriptionType { get }

    /// Initialise the Source.
    /// - Parameter: sourceTag The URI tag identifying the Source.
    /// - Parameter: evalOp The Evaluation Operation to be performed on Notification.
    //init(tag: String, subscriptionType: eRxSubscriptionType, evalOp: RxTypes2<ItemType, ItemType>.RxEvalOp?)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxConsumer:  A consumer of notifications issued by an IRxProducer.
///

public protocol IRxConsumer
{
    associatedtype ConsumerItemType

    /// Notify consumer of item.
    /// - Parameter item: The item associated with the Item Notification.
    func notify(item: ConsumerItemType)


    /// Notify consumer of completion, possibly with an error.
    /// - Parameter error: Nil for normal completion, otherwise an IRxError to indicate the associated error.
    func notifyCompleted(error : IRxError?)

    /// Notify consumer of state change.
    /// - Parameter stateChange: The new eval state.
    func notify(stateChange: eRxEvalStateChange)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxObserver:  An observer of notifications issued by an IRxProducer available after subscription.
///

public protocol IRxObserver : IRxConsumer
{

}

///
/// ARxConsumer: The abstract base class for Notification Consumers. All Consumer classes should inherit from this class.
///

open class ARxConsumer<ItemType> : RxObject, IRxConsumer
{
    public typealias ConsumerItemType = ItemType

    /// Initialise with tag and description.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter description: The optional description of the instance.
    public override init(tag : String, description : String? = nil)
    {
        super.init(tag: tag, description: description)
    }

    /// Clone from the given ARxConsumer with the given tag.
    /// - Parameter consumer: The Consumer to clone.
    /// - Parameter tag: The RxObject tag for this instance.
    public init?(clone consumer: ARxConsumer, tag: String)
    {
        super.init(clone: consumer, tag : tag)
    }

    /// Notify consumer of receipt of item notification.
    /// - Parameter item: The item associated with the Item Notification.
    open func notify(item: ConsumerItemType)
    {
        // do nothing.
    }

    /// Notify consumer of receipt of completion notification, possibly with an error.
    /// - Parameter error: The argument indicates the associated IRxError, otherwise nil for normal completion.
    open func notifyCompleted(error : IRxError? = nil)
    {
        // do nothing.
    }

    /// Notify consumer of receipt of state change.
    /// - Parameter stateChange: The new eval state.
    open func notify(stateChange: eRxEvalStateChange)
    {
        // do nothing.
    }

    /// Convenience function, notify a termination.
    /// - Parameter termination: The termination type to notify.
    open func notify(termination : eTerminationType)
    {
        switch termination
        {
            case .eCompleted:

                notifyCompleted(error: nil)

            case .eError(let error):

                notifyCompleted(error: error)
        }
    }

    /// Convenience function, notify from an enumerable of items notifications.
    /// - Parameter items: An enumerable of item notifications.
    public final func notify<EnumerationType : IRxEnumerable>(items: EnumerationType) where EnumerationType.ItemType == ConsumerItemType
    {
        let thunkedItems = TRxEnumerable(items)

        for item in thunkedItems
        {
            notify(item: item)
        }
    }

    /// Convenience function, notify from a queue of notifications.
    /// - Parameter notifications: A queue of notifications.
    public final func notifyNotifications<EnumerationType : IRxEnumerable>(_ notifications: EnumerationType) where EnumerationType.ItemType == RxNotification<ConsumerItemType>
    {
        let thunkedNotifications = TRxEnumerable(notifications)

        for notification in thunkedNotifications
        {
            switch notification.notifyType
            {
                case .eNotification_Item(let item):
                    notify(item: item)

                case .eNotification_Termination(let termination):
                    notify(termination: termination)
            }
        }
    }
}

///
/// The abstract base class for Observers. All Observer classes should inherit from this class.
///

open class ARxObserver<ItemType> : ARxConsumer<ItemType>, IRxObserver
{
    public typealias ConsumerItemType = ItemType

    /// The RxObject type for RxObservers.
    open override var objectType: eRxObjectType               {  return .eRxObject_RxObserver }

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for this instance.
    public override init(tag: String, description: String? = nil)
    {
        super.init(tag: tag, description: description)
    }

    /// Clone from the given ARxObserver with the given tag.
    /// - Parameter observer: The Observer to clone.
    /// - Parameter tag: The RxObject tag for this instance.
    public init?(clone observer: ARxObserver, tag: String)
    {
        super.init(clone: observer, tag : tag)
    }
}

///
/// IRxObserverManagingSubscription:  When an observer needs to directly manage unsubscription.
///

public protocol IRxObserverManagingSubscription
{
    associatedtype ObserverItemType

    /// Notify consumer of item.
    /// - Parameter item: The item associated with the Item Notification.
    /// - Parameter subscription: The subscription associated with the notification.
    func notify(item: ObserverItemType, subscription : RxSubscription)


    /// Notify consumer of completion, possibly with an error.
    /// - Parameter error: Nil for normal completion, otherwise an IRxError to indicate the associated error.
    /// - Parameter subscription: The subscription associated with the notification.
    func notifyCompleted(error : IRxError?, subscription : RxSubscription)

    /// Notify consumer of state change.
    /// - Parameter stateChange: The new eval state.
    func notify(stateChange: eRxEvalStateChange)
}

///
/// ARxObserverManagingSubscription: The abstract base class for Notification Observers that are responsible for managing their subscriptions.
///

open class ARxObserverManagingSubscription<ItemType> : RxObject, IRxObserverManagingSubscription
{
    public typealias ObserverItemType = ItemType

    /// The RxObject type for ARxObserverManagingSubscription.
    open override var objectType: eRxObjectType               {  return .eRxObject_RxObserver }

    /// Initialise with tag and description.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter description: The optional description of the instance.
    public override init(tag: String, description: String? = nil)
    {
        super.init(tag: tag, description: description)
    }

    /// Clone from the given ARxConsumer with the given tag.
    /// - Parameter observer: The Observer to clone.
    /// - Parameter tag: The RxObject tag for this instance.
    public init?(clone observer: ARxObserverManagingSubscription, tag: String)
    {
        super.init(clone: observer, tag: tag)
    }

    /// Notify consumer of item.
    /// - Parameter item: The item associated with the Item Notification.
    /// - Parameter subscription: The subscription associated with the notification.
    open func notify(item: ObserverItemType, subscription: RxSubscription)
    {
        // do nothing.
    }

    /// Notify consumer of completion, possibly with an error.
    /// - Parameter error: The argument indicates the associated IRxError, otherwise nil for normal completion.
    /// - Parameter subscription: The subscription associated with the notification.
    open func notifyCompleted(error: IRxError? = nil, subscription: RxSubscription)
    {
        // do nothing.
    }

    /// Notify consumer of state change.
    /// - Parameter stateChange: The new eval state.
    open func notify(stateChange: eRxEvalStateChange)
    {
        // do nothing.
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxSubject:  A composite object which is both an Observable and a Consumer.
///

public protocol IRxSubject : IRxObserver
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Notification type identifying item and completed (termination) notification types.
///
/// - eNotification_Item: Notification of item
/// - eNotification_Termination: Notification of termination as completed or error

public enum eNotifyType<ItemType>
{
    /// A item notification event.
    case eNotification_Item(ItemType)

    /// A termination notification event with optional error.
    case eNotification_Termination(eTerminationType)
    
    func isTerminatingType() -> Bool
    {
        switch self
        {
        case .eNotification_Termination:
            return true
            
        default:
            return false
        }
    }
    
    var description : String {
        
        switch self
        {
        case .eNotification_Item(let item):
            return String(describing: item)
            
        case .eNotification_Termination(let termination):
            return termination.description
        }
    }
}

///
/// Notification.
///
///     Encapsulates a notification for use as a first class citizen object.
///

public protocol IRxNotification
{
    associatedtype ItemType
    
    /// Notification type getter.
    var notifyType: eNotifyType<ItemType>       { get }
    
    /// Optional notification timestamp getter.
    var timeStamp:  eRxTime?                    { get }
}

/// Equate operator for the eNotifyType enum.
public func ==<ItemType: Equatable>(lhs: eNotifyType<ItemType>, rhs: eNotifyType<ItemType>) -> Bool
{
    switch (lhs, rhs)
    {
    case (.eNotification_Item(let lhsItem), .eNotification_Item(let rhsItem)):
        return lhsItem == rhsItem
        
    case (.eNotification_Termination(let lhsTermination), .eNotification_Termination(let rhsTermination)):
        return lhsTermination == rhsTermination
        
    default:
        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxNotifier: Notifies targets of the receipt of a notification.
///

public protocol IRxNotifier : IRxObserver
{
    associatedtype ItemType = ConsumerItemType

    /// Indicator of whether the Notifier is valid and configured.
    var isActive : Bool { get }

    /// Activate the notifier, enabling it for operation and flag it as active (isActive property).
    func activate()

    /// Deactivate the notifier, disabling operation and flag it as not active (isActive property).
    func deactivate()

    /// Clear the internal state of the notifier and release any dependants.
    func clear()
}

///
/// RxNotifier:  The abstract base class for notifiers. All notifier classes should inherit from this class.
///

open class ARxNotifier<ItemType> : ARxConsumer<ItemType>, IRxNotifier
{
    /// Indicator of whether the Notifier is valid and configured.
    open var isActive : Bool                              { return m_isActive }

    /// The count of targets.
    open var count : RxCountType                          { fatalError("Property should be overridden") }

    /// The RxObject type for this class.
    open override var objectType : eRxObjectType          { return .eRxObject_RxNotifier }

    /// Backing entity for isActive.
    private var m_isActive = true

    /// Initialise with tag, description and associated subscription ID.
    /// - Parameter tag: The tag identifier of the RxObject.
    /// - Parameter description: The description of the RxObject.
    public override init(tag : String, description : String? = nil)
    {
        super.init(tag : tag, description: description)
    }

    /// Clone from the given Notifier.
    /// - Parameter notifier: The Notifier to clone.
    public init?(clone notifier: ARxNotifier<ItemType>)
    {
        self.m_isActive = notifier.isActive

        super.init(clone : notifier, tag : notifier.tag + "/clone")
    }

    /// Activate the notifier and flag it as active.
    open func activate()
    {
        m_isActive = true
    }

    /// Deactivate the notifier and flag it as not active.
    open func deactivate()
    {
        m_isActive = false
    }

    /// Clear the configuration of the notifier and release any dependants.
    open func clear()
    {
        deactivate()
    }
}

///
/// IRxNotifierByDelegate: Notifies targets of a notification by means of delegates.
///

public protocol IRxNotifierToDelegate : IRxNotifier
{
    /// The delegate to handle an item notification.
    var onItem : RxTypes<ItemType>.RxItemActionDelegate                         { get set }

    /// The delegate to handle a completed notification.
    var onCompleted : RxTypes<ItemType>.RxCompletedActionDelegate               { get set }

    /// The delegate to handle a state change notification.
    var onStateChange : RxTypes<ItemType>.RxStateChangeActionDelegate           { get set }
}

public extension IRxNotifierToDelegate
{
    public typealias RxItemActionDelegate = RxTypes<ItemType>.RxItemActionDelegate
    public typealias RxCompletedActionDelegate = RxTypes<ItemType>.RxCompletedActionDelegate
    public typealias RxStateChangeActionDelegate = RxTypes<ItemType>.RxStateChangeActionDelegate

    /// The No Operation Notify Item Delegate.
    public static var NopOnItemDelegate: RxItemActionDelegate                 { return { (item : ItemType) -> Void in } }

    /// The No Operation Notify Completed Delegate.
    public static var NopOnCompletedDelegate: RxCompletedActionDelegate       { return { (error : IRxError?) -> Void in } }

    /// The No Operation Notify State Change Delegate.
    public static var NopOnStateChangeDelegate: RxStateChangeActionDelegate   { return { (stateChange : eRxEvalStateChange) -> Void in } }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxNotificationAcceptor: Monitor notifications. Delegates return an indicator of permission to process the notification.
///

public protocol IRxNotificationAcceptor
{
    associatedtype ItemType

    /// Indicate that the item notification is acceptable.
    /// - Parameter item: The item associated with the notification.
    /// - Returns: The indication of acceptance.
    func willAccept(item : ItemType) -> Bool

    /// Indicate that the completed/error notification is acceptable.
    /// - Parameter error: The error associated with the completed notification.
    /// - Returns: The indication of acceptance.
    func willAcceptCompleted(error : IRxError?) -> Bool

    /// Indicate that the eval state change notification is acceptable.
    /// - Parameter stateChange: The eval state change associated with the notification.
    /// - Returns: The indication of acceptance.
    func willAccept(stateChange: eRxEvalStateChange)-> Bool
}

///
/// ARxNotificationAcceptor: The abstract Notification Acceptor base class. All Notification Acceptor classes should inherit from this class.
///

open class ARxNotificationAcceptor<CheckerItemType> : IRxNotificationAcceptor
{
    public typealias ItemType = CheckerItemType
    public typealias NonAcceptanceDelegateType = (String) -> Void

    /// Optional delegate to handle non-acceptance events.
    open var onNonAcceptance : NonAcceptanceDelegateType? = nil

    /// Indicate that the eval state change notification is acceptable.
    /// - Parameter item: The item associated with the notification.
    /// - Returns: The indication of acceptance.
    open func willAccept(item : CheckerItemType) -> Bool
    {
        return true
    }

    /// Indicate that the completed/error notification is acceptable.
    /// - Parameter error: The error associated with the completed notification.
    /// - Returns: The indication of acceptance.
    open func willAcceptCompleted(error : IRxError?) -> Bool
    {
        return true
    }

    /// Indicate that the eval state change notification is acceptable.
    /// - Parameter stateChange: The eval state change associated with the notification.
    /// - Returns: The indication of acceptance.
    open func willAccept(stateChange: eRxEvalStateChange) -> Bool
    {
        return true
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

///
/// eRxSubscriptionType: The type of subscription.
///

public enum eRxSubscriptionType : String
{
    /// Hot subscription type: all subscriptions share the same expression eval engine.
    case eHot   = "Hot"

    /// Cold subscription type: all subscriptions have their own separate expression eval engine.
    case eCold  = "Cold"

    /// CustomStringConvertible conformance.
    public var description : String
    {
        return self.rawValue
    }
}

///
/// eRxSubscriptionState: The lifecycle state of the subscription.
///
/// - eContructingEvalEngine: The subscription is currently constructing the Evaluation Engine.
/// - eEvaluating:  The subscription is current is currently evaluating the expression.
/// - eUnsubscribing: The subscription is currently disposing.
/// - eShutdown: The subscription has been shutdown, if due to an error, then indicated.
///

public enum eRxSubscriptionState : Hashable
{
    /// No current subscription state.
    case eNoSubscription

    /// Currently constructing the eval engine.
    case eConstructingEvalEngine

    /// Currently evaluating.
    case eEvaluating

    /// Currently disposing/unsubscribing.
    case eUnsubscribing

    /// The previous subscription has been shutdown.
    case eShutdown(IRxError?)

    /// Indicate whether the subscription has shutdown.
    public var hasShutdown:  Bool
    {
        switch self
        {
            case .eShutdown:
                return true

            default:
                return false
        }
    }

    /// CustomStringConvertible conformance.
    public var description : String
    {
        switch self
        {
            case .eNoSubscription:           return "No Subscription"
            case .eConstructingEvalEngine:   return "Constructing Evaluation Engine"
            case .eEvaluating:               return "Evaluating"
            case .eUnsubscribing:            return "Unsubscribing"
            case .eShutdown(let error):      return error == nil ? "Shutdown" : "Shutdown with error(\(String(describing: error))"
        }
    }

    /// Hashable conformance.
    public var hashValue : Int
    {
        switch self
        {
            case .eNoSubscription:           return 0
            case .eConstructingEvalEngine:   return 1
            case .eEvaluating:               return 2
            case .eUnsubscribing:            return 3
            case .eShutdown(let error):      return error == nil ? 4 : String(describing: error).hashValue
        }
    }
}

/// Equate operator for the eRxSubscriptionState enum.
public func ==(lhs: eRxSubscriptionState, rhs : eRxSubscriptionState) -> Bool
{
    switch (lhs, rhs)
    {
        case (.eNoSubscription, .eNoSubscription),
             (.eConstructingEvalEngine, .eConstructingEvalEngine),
             (.eEvaluating, .eEvaluating),
             (.eUnsubscribing, .eUnsubscribing):
            return true

        case (.eShutdown(let lhsError), .eShutdown(let rhsError)):
            return lhsError == rhsError

        default:
            return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Subscription lifecycle events, used by the RxDisposalMonitor
///
/// - eRequestedToDisposeNotice:    Notice given of a request to dispose.
/// - eReadyForDisposalNotice:      Notice given that the subscription is now ready to dispose (if required).
/// - eEvalEndedNotice:             Notice given that the evaluation has ended.
/// - eEvalQueueStoppedNotice:      Notice given that the EvalQueue associated with the subscription has been stopped (due to disposal).
/// - SubscriptionShutdownNotice:   Notice given that the Subscription has been shutdown.
///

public enum eRxSubscriptionEvent_Option: Int, RawRepresentable, CustomStringConvertible
{
    /// Notice that the subscription has been requested to dispose.
    case eRequestedToDisposeNotice  = 0x1

    /// Notice that the subscription construction has finished and the subscription can be disposed at any time.
    case eReadyForDisposalNotice    = 0x2

    /// Notice that the evalutation has ened.
    case eEvalEndedNotice           = 0x4

    /// Notice that the Eval Queue for the expression has been stopped (after evaluation eneded).
    case eEvalQueueStoppedNotice    = 0x8

    /// Notice that the subscription has been fully shutdown.
    case eSubscriptionShutdownNotice = 0x10

    /// Initialise by raw value.
    public init?(rawValue: Int)
    {
        switch rawValue
        {
            case 0x1:   self = .eRequestedToDisposeNotice
            case 0x2:   self = .eReadyForDisposalNotice
            case 0x4:   self = .eEvalEndedNotice
            case 0x8:   self = .eEvalQueueStoppedNotice
            case 0x10:  self = .eSubscriptionShutdownNotice

            default:    return nil
        }
    }

    /// The index of the enum item.
    public var indexOf : Int
    {
        switch self
        {
            case .eRequestedToDisposeNotice:         return 0
            case .eReadyForDisposalNotice:           return 1
            case .eEvalEndedNotice:                  return 2
            case .eEvalQueueStoppedNotice:           return 3
            case .eSubscriptionShutdownNotice:       return 4
        }
    }

    /// CustomStringConvertible conformance.
    public var description: String
    {
        switch self
        {
            case .eRequestedToDisposeNotice:         return "RequestedToDisposeNotice"
            case .eReadyForDisposalNotice:           return "ReadyForDisposalNotice"
            case .eEvalEndedNotice:                  return "EvalEndedNotice"
            case .eEvalQueueStoppedNotice:           return "EvalQueueStoppedNotice"
            case .eSubscriptionShutdownNotice:        return "SubscriptionShutdownNotice"
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Subscription lifecycle events (eRxSubscriptionEvent_Option) as an option set.
///

public struct oRxSubscriptionEvent_Options: OptionSet, Sequence, Hashable, CustomStringConvertible
{
    private typealias IteratorFunc = AnyIterator<eRxSubscriptionEvent_Option>

    /// The raw value of the option set.
    public let rawValue: Int

    /// Subscription was requested to dispose.
    public static let oRxRequestedToDisposeNotice   = oRxSubscriptionEvent_Options(.eRequestedToDisposeNotice)

    /// Subscription is ready to dispose (when requested).
    public static let oRxReadyForDisposalNotice     = oRxSubscriptionEvent_Options(.eReadyForDisposalNotice)

    /// The Subscription evalqueue has ended performing operations.
    public static let oRxEvalEndedNotice            = oRxSubscriptionEvent_Options(.eEvalEndedNotice)

    /// The Subscription evalqueue has been fully stopped.
    public static let oRxEvalQueueStoppedNotice     = oRxSubscriptionEvent_Options(.eEvalQueueStoppedNotice)

    /// The Subscription has shutdown.
    public static let oRxSubscriptionShutdownNotice = oRxSubscriptionEvent_Options(.eSubscriptionShutdownNotice)

    /// Init by raw value.
    public init(rawValue: Int)
    {
        self.rawValue = rawValue
    }

    /// Init by corresponding enum.
    private init(_ enumValue: eRxSubscriptionEvent_Option)
    {
        rawValue = enumValue.rawValue
    }

    /// Hashable conformance.
    public var hashValue:   Int
    {
        return rawValue + 1
    }

    /// CustomStringConvertible conformance.
    public var description: String
    {
        var haveValues = false
        var result = "["

        for index in 0...4
        {
            let value = oRxSubscriptionEvent_Options(rawValue: 1 << index)

            if self.contains(value)
            {
                if haveValues
                {
                    result += ", "
                }

                result += oRxSubscriptionEvent_Options.m_names[index]

                haveValues = true
            }
        }

        return result + "]"
    }

    /// The Sequence iterator to enumerate through the oRxEvalRequest elements.
    public func makeIterator() -> AnyIterator<eRxSubscriptionEvent_Option>
    {
        var currentIndex: Int = 0

        return AnyIterator
        {
            while currentIndex < 5
            {
                let rawValue = 1 << currentIndex
                let value    = oRxSubscriptionEvent_Options(rawValue: rawValue)

                currentIndex += 1

                if self.contains(value)
                {
                    return eRxSubscriptionEvent_Option(rawValue: rawValue)
                }
            }

            return nil
        }
    }

    /// String names of each element.
    private static let m_names = [

            "RequestedToDisposeNotice",
            "ReadyForDisposalNotice",
            "EvalEndedNotice",
            "EvalQueueStoppedNotice",
            "SubscriptionShutdownNotice"
    ]
}

///
/// IRxSubscription: Encapsulates the actions to be performed during unsubscription.
///

public protocol IRxSubscription
{
    /// The subscription type.
    var subscriptionType : eRxSubscriptionType { get }

    /// The subscription state.
    var state : eRxSubscriptionState { get }

    /// Indicate current subscription status.
    var subscribed: Bool { get }
    
    /// Unsubscribe the associated subscription.
    func unsubscribe()
}

///
/// IRxDisposable:  Encapsulates the a set of actions to be performed when asked to dispose.
///

public protocol IRxDisposable
{
    /// Indicate current disposal status.
    var disposed: Bool { get }
    
    /// Dispose the IRxDisposable, execute all the of registered disposal actions.
    /// - Parameter sync: The eval queue to run the disposal actions in.
    /// - Returns: Indicator if the disposal was effected.
    func dispose(sync evalQueue : RxEvalQueue?) -> Bool

    /// Indicate that the IRxDisposable is allowed to dispose. The dispose func will block until this is called.
    func allowDisposal()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

///
/// eRxExprProperty: The operational properties of an expression. These are resolved during subscription construction.
///
///
/// - eEvalQueuePriority:       The expression RxEvalQueue priority
/// - eSubscriptionStats:       Enabler of subscription stats for the expression.
/// - eInfoTagEvalQueue:        Indicator of whether to info tag the EvalQueue dispatch queue.
/// - eQueueIDTagEvalQueue:     The Eval Queue ID of the Eval Queue for expression.
/// - eTraceConfig:             The resolved RxTraceConfig of the expression.
///

public enum eRxExprProperty
{
    /// The eval queue priority property of the observable or of the resolved expression.
    case eEvalQueuePriority(DispatchQoS)

    /// The subscription stats enabler property of the observable or of the resolved expression.
    case eSubscriptionStatsEnable(RxSubscriptionStats)

    /// Details of the Eval Queue for the expression.
    case eInfoTagEvalQueue(Bool)

    /// The Eval Queue ID of Eval Queue for the expression.
    case eQueueIDTagEvalQueue(Bool)

    /// The resolved RxTraceConfig property of the expression.
    case eTraceConfig(RxTraceConfig)
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxControllingObserver:  An observer that has a control interface, to terminate evaluation, or confirm to the source node the receipt of notifications.
///

public protocol IRxControllingObserver : IRxObserver
{
    /// The subscriptions that the observer is participating in.
    var subscriptions : [RxSubscription]                                { get }

    /// The source notifiers that the observer is participating in.
    var sourceNotifiers : RxNotifier_NotifyConsumers<ConsumerItemType>  { get }

    /// Add a subscription to the list of subscriptions that the observer is participating in.
    func addSubscription(_ subscription : RxSubscription)

    /// Add a source notifier to the list of source notifiers that the observer is participating in.
    func addSourceNotifier(_ sourceNotifier : ARxNotifier<ConsumerItemType>)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// IRxSchedulable: Schedulable item types.
//


/// Schedulability by RxTime.
public protocol IRxSchedulable
{
    /// The scheduled absolute time.
    var atTime: RxTime? { get }
}

/// Schedulability by RxRelTime.
public protocol IRxRelTimeSchedulable
{
    /// The scheduled relative time.
    var atTime: RxRelTime? { get }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// enum eRxTime:  RxNotificationQueue time offsets for use by offset to a specific time or by exact offset.
///
/// Used by RxNotification timestamp calculations for switching between relative and absolute timestamps.
///
/// - eByOffset:   The time as an offset.
/// - eByTime:     The time as an absolute.
///

public enum eRxTime
{
    /// Define a time by an offset (to a some timestamp).
    case eByOffset (RxTimeOffset)

    /// Define time by an absolute time.
    case eByTime (RxTime)

    /// Convert to absolute time.
    public func toFixedTime(_ baseTime : RxTime? = nil) -> RxTime
    {
        switch self
        {
            case .eByOffset(let offsetTime):

                assert(baseTime != nil, "Expected base time to be valid")
                return baseTime!.addingTimeInterval(offsetTime)

            case .eByTime(let time):
                return time
        }
    }

    /// Convert to relative time offset.
    public func toTimeOffset(_ baseTime : RxTime? = nil) -> RxTimeOffset
    {
        switch self
        {
            case .eByOffset(let offsetTime):
                return offsetTime

            case .eByTime(let time):

                assert(baseTime != nil, "Expected base time to be valid")
                return time.timeIntervalSince(baseTime!)
        }
    }

    /// Calculate the time difference to the given etRxTime.
    /// - Parameter comparisonTime: The etRxTime to compare to.
    /// - Returns: The time difference.
    public func diff(_ comparisonTime : eRxTime) -> RxDuration?
    {
        switch (self, comparisonTime)
        {
            case (.eByOffset(let selfOffsetTime), .eByOffset(let otherOffsetTime)):
                return otherOffsetTime - selfOffsetTime

            case (.eByTime(let selfTime), .eByTime(let otherTime)):
                return otherTime.timeIntervalSince(selfTime)

            default:
                return nil
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eGateType: The type of gating to be performed on a collection of streams.
///
/// - eGate_OnAnyQueueHasItem:                  Gate on any queue receiving a notifications.
/// - eGate_OnLatestItem:                       Keep the latest item for each stream, gate when any notification is received and all queues have at least one latest item.
/// - eGate_OnAllQueuesHaveAtLeastOneItem:      Gate when every queue has at least one item.
///

public enum eGateType : String
{
    /// Gate on any queue receiving a notifications.
    case eGate_OnAnyQueueHasItem =              "Gate any queue that has an item"

    /// Keep the latest item for each stream and emit a gate notification when any notification is received and all queues have at least one latest queued entry.
    case eGate_OnLatestItem =                   "Gate the latest item on any queue"

    /// Gate on every queue having at least one item.
    case eGate_OnAllQueuesHaveAtLeastOneItem =  "Gate all queues have at least one item"
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxTimerSubscription: Encapsulates the timer request state.
///

public typealias RxTimerAction = (RxIndexType) -> Void

///
/// RxTimerSubscription: The RxTimer context/state.
///

open class RxTimerSubscription: RxObject, Hashable, Equatable
{
    /// The type of leeway to be used in the internal timer dispatch_source_set_timer call.
    open var leewayType : eRxTimerLeeway                      { return m_leewayType }

    /// The starting reference time for the timer.
    open var startTime :  RxTime                              { return m_startTime }

    /// Indicator of whether the timer action is running.
    open var isRunning : Bool                                 { return m_isRunning }

    /// The action the timer request is to perform.
    open var timerAction: RxTimerAction = { _ in }

    /// The next index of the timer request.
    open var nextIndex : RxIndexType
    {
        let index = m_currentIndexCounter

        m_currentIndexCounter += 1

        return index
    }

    /// The RxObject type of this class.
    open override var objectType : eRxObjectType              { return .eRxObject_RxTimerSubscription }

    /// Backing variable for leewayType
    private let m_leewayType : eRxTimerLeeway
    /// Backing variable for startTime
    private let m_startTime : RxTime
    /// Backing variable for isRunning
    private var m_isRunning = true

    /// The current index counter.
    private var m_currentIndexCounter : RxIndexType = 0

    /// Initialise.
    /// - Parameter tag: The tag for this RxObject.
    /// - Parameter startTime: The starting time of the timer session.
    /// - Parameter startIndex: The starting index of the timer session.
    /// - Parameter leewayType: The timer leeway use type.
    /// - Parameter timerAction: The timer action to be executed.
    init(tag: String, startTime : RxTime, startIndex : RxIndexType = 0, leewayType : eRxTimerLeeway, timerAction: @escaping RxTimerAction = { _ in })
    {
        self.m_leewayType = leewayType
        self.m_startTime = startTime
        self.m_currentIndexCounter = startIndex
        self.timerAction = timerAction

        super.init(tag: tag + "/timerSubscription")
    }

    /// Terminate the timer action.
    open func terminate()
    {
        if m_isRunning
        {
            m_isRunning = false
        }

        self.timerAction = { _ in }
    }
}

public func ==(lhs  : RxTimerSubscription, rhs : RxTimerSubscription) -> Bool
{
    return lhs.hashValue == rhs.hashValue
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxRequestor : A RxService request forwarder.
///

public protocol IRxRequestor
{
    associatedtype RequestType
    
    /// Begin an RxService session.
    func sessionBegin()
    
    /// Request to the RxService.
    func request(_ request : RequestType)
    
    /// End an RxService session.
    func sessionEnd(_ error: IRxError?)
    
    /// The delegate to handle a session begin request.
    var onSessionBegin : RxTypes<RequestType>.RxBeginSessionDelegate               { get set }
    
    /// The delegate to handle a request.
    var onRequest : RxTypes<RequestType>.RxRequestDelegate                         { get set }
    
    /// The delegate to handle a session end request.
    var onSessionEnd : RxTypes<RequestType>.RxEndSessionDelegate                   { get set }
}

public extension IRxRequestor
{
    typealias RxBeginSessionDelegate = RxTypes<RequestType>.RxBeginSessionDelegate
    typealias RxRequestDelegate = RxTypes<RequestType>.RxRequestDelegate
    typealias RxEndSessionDelegate = RxTypes<RequestType>.RxEndSessionDelegate

    /// The no operation form of the session begin delegate.
    public static var NopOnSessionBeginDelegate: RxBeginSessionDelegate     { return {} }

    /// The no operation form of the request delegate.
    public static var NopOnRequestdDelegate: RxRequestDelegate              { return { (request : RequestType) -> Void in } }

    /// The no operation form of the session end delegate.
    public static var NopOnSessionEndDelegate: RxEndSessionDelegate         { return { (error : IRxError?) -> Void in } }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxEnumerable: Enumerable of type ItemType.
///
///  Iterates through a collection of item notifications.
///

public protocol IRxEnumerable : Sequence, CustomStringConvertible
{
    associatedtype ItemType
    associatedtype IteratorType = ItemType
    
    /// The Sequence iterator getter.
    func makeIterator() -> AnyIterator<ItemType>
}

extension IRxEnumerable
{
    /// CustomStringConvertible conformance.
    public var description : String
    {
        var desc = "["
        var isStart = true

        for entry in self
        {
            if !isStart
            {
                desc += ", "
            }
            else
            {
                isStart = false
            }

            desc += String(describing: entry)
        }

        return desc + "]"
    }
}

///
/// TRxEnumerable: Thunk Struct for IRxEnumerable.
///
///  Iterates through a collection of item notifications.
///

public struct TRxEnumerable<ItemType> : IRxEnumerable
{
    /// The generate func implementation.
    private let m_generateDelegate : () -> AnyIterator<ItemType>

    /// Initialise the thunk with the IRxEnumerable implementation.
    public init<EnumerableType : IRxEnumerable>(_ implementation : EnumerableType) where EnumerableType.ItemType == ItemType
    {
        m_generateDelegate = implementation.makeIterator
    }

    /// The Sequence generator getter.
    public func makeIterator() -> AnyIterator<ItemType>
    {
        return m_generateDelegate()
    }
}
