// ipx-support.swift
// RxPatternsSDK
//
//  Created by Terry Stillone on 5/09/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxLogger: The logging Interface.
///
/// - See The RxLogger class
///

public protocol IRxLogger
{
    /// Log the a message.
    /// - Parameter message: The message to be logged.
    func log(_ message : String)

    /// Shutdown the logger.
    func shutdown()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxTrace: The Tracing Interface, for debuuging.
///
/// - See The RxTrace class.
///

public protocol IRxLog: CustomStringConvertible, CustomDebugStringConvertible
{
    /// Log a trace message
    /// - Parameter indent: The indent level.
    /// - Parameter format: The format of the message (ellipsis)
    func log(_ indent : Int, format : String, _ args: CVarArg...)
    
    /// Log a static trace message.
    /// - Parameter format: The format of the message (ellipsis)
    static func log(_ format : String, _ args: CVarArg...)
    

    /// Log a trace error message.
    /// - Parameter format: The format of the message (ellipsis)
    static func logError(_ format : String, _ args: CVarArg...)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxTimerLeeway: Timer leeway handling types.
///
/// - eLeewayMin: Use the minimum leeway 10msec.
/// - eLeewayAsTime: Define the leeway as a fixed time, in sec (as RxDuration).
/// - eLeewayAsFactor: Define the leeway as a factor of the timer time offset (as Double).

public enum eRxTimerLeeway
{
    /// Use the minimum leeway 10msec.
    case eLeewayMin

    /// Define the leeway as a fixed time, in sec (as RxDuration).
    case eLeewayAsTime(leewayTime: RxDuration)

    /// Define the leeway as a factor of the timer time offset (as Double).
    case eLeewayAsFactor(leewayFactor: Double)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxDirectory_NameSpace: The URI namespaces used in RxDirectory. The namespaces are:
///
/// - eStore: The namespace used to store objects.
/// - eRun: The namespace used to reference objects during operation.
/// - eUnknown: Token for unknown namespaces.
///

public enum eRxDirectory_NameSpace: String
{
    /// The namespace is unknown.
    case eUnknown = "unknown"

    /// The store: namespace for storing objects.
    case eStore = "store"

    /// The run namespace for operational use.
    case eRun = "run"

    /// Get the components of the URI: (Namespace, Path)
    public static func components(_ uri : String) -> (namespace: eRxDirectory_NameSpace, path: String)
    {
        let tokens = uri.components(separatedBy: RxDirectory.Constant.NamespaceSeparator)
        let namespace = tokens.count == 2 ? tokens[0].trimmingCharacters(in: CharacterSet.whitespaces) : "unknown"

        switch namespace
        {
            case _ where namespace == "":
                return (.eRun, tokens[1].trimmingCharacters(in: CharacterSet.whitespaces))

            case _ where namespace == eRxDirectory_NameSpace.eRun.rawValue:
                return (.eRun, tokens[1].trimmingCharacters(in: CharacterSet.whitespaces))

            case _ where namespace == eRxDirectory_NameSpace.eStore.rawValue:
                return (.eStore, tokens[1].trimmingCharacters(in: CharacterSet.whitespaces))

            default:
                return (.eUnknown, uri)
        }
    }

    var description : String
    {
        return self.rawValue
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxDirectory_Command: Control commands for the RxDirectory. The commands include:
///
/// - eCommand_put: RxDirectory put RxObject command.
/// - eCommand_remove: RxDirectory remove RxObject command.
/// - eCommand_removeAllWithTagRegexpr: RxDirectory remove all RxObject matching regexp command.
/// - eCommand_removeAll: RxDirectory remove all RxObjects command.
///

public enum eRxDirectory_Command
{
    /// Put an object into the directory at the given URI.
    case eCommand_put(uri : String, object : RxObject)

    /// Remove the entry for the given URI in the directory.
    case eCommand_remove(uri : String)

    /// Remove all entries that match the given regex expression in the directory.
    case eCommand_removeAllWithTagRegexpr(tagRegexpr: String)

    /// Remove all entries in the directory.
    case eCommand_removeAll()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxDirectory_Event: RxDirectory monitoring events. The events include:
///
/// - eEvent_get: RxDirectory get RxObject event.
/// - eEvent_put: RxDirectory put RxObject event.
/// - eEvent_remove: RxDirectory remove RxObject event.
/// - eEvent_removeAllWithTagRegexpr: RxDirectory remove all RxObject matching regexp event.
/// - eEvent_removeAll: RxDirectory remove all RxObjects event.
/// - eEvent_Subscribe: RxDirectory subscribe event.
///

public enum eRxDirectory_Event
{
    /// An object was retrieved from the directory at the given URI.
    case eEvent_get(uri : String, object : RxObject?)

    /// An object was put into the directory at the given URI.
    case eEvent_put(uri : String, object : RxObject)

    /// An object was removed from the directory at the given URI.
    case eEvent_remove(uri : String, object : RxObject)

    /// All objects were removed from the directory at the given URI.
    case eEvent_removeAll

    /// Objects were removed that match the regex from the directory.
    case eEvent_removeAllWithTagRegexpr(tagRegexp : String)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxDevTraceable: Objects that are traceable.
///

public protocol IRxDevTraceable
{
    var traceEnabled : Bool                      { get set }
    var indent : Int                             { get set }

    mutating func pushScope(_ scopeName: String)
    mutating func popScope()

    func log(message : String)
}

extension IRxDevTraceable
{
    public func trace(_ name : String, _ message : String, indent : Int? = nil, marker : String? = nil)
    {
        guard traceEnabled else { return }

        let applyIndent = indent ?? self.indent
        let padding = String(repeating: " ", count : applyIndent * RxSDK.log.IndentLength)
        let message = padding + ">>" + name + ">> " + message

        log(message: message)
    }

    public func traceWait(_ name : String, _ message : String, indent : Int? = nil, marker : String? = nil)
    {
        guard traceEnabled else { return }

        let applyIndent = indent ?? self.indent
        let padding = String(repeating: " ", count : applyIndent * RxSDK.log.IndentLength)
        let effectiveName = "<< " + name

        log(message: "\(padding)\(effectiveName) \(message)")
    }

    public func traceWithSeparator(_ name : String, _ message : String, indent : Int? = nil, marker : String? = nil)
    {
        guard traceEnabled else { return }

        let applyIndent = indent ?? self.indent
        let padding = String(repeating: " ", count : applyIndent * RxSDK.log.IndentLength)
        let effectiveName = ">>" + name + ">> "

        log(message: "\n\(padding)----------------------------------------\n\(padding)\(effectiveName) \(message)")
    }

    public func traceError(_ name : String, _ message : String, indent : Int? = nil, force : Bool = false)
    {
        guard traceEnabled else { return }

        let applyIndent = indent ?? self.indent
        let padding = String(repeating: " ", count : applyIndent * RxSDK.log.IndentLength)

        log(message: "\(padding)>> Error >>>>>>> \(name) \(message)")
    }
}
