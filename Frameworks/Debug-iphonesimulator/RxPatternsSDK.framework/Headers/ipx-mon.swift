// ipx-mon.swift
// RxPatternsSDK
//
// Created by Terry Stillone (http://www.originware.com) on 8/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// RxMon.swift: Monitoring of SDK RxObjects.
//


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxTraceConfig: The RxMon Object trace-monitoring configuration.
///

public struct RxTraceConfig
{
    /// The trace line tag that prepends a trace message.
    public let traceTag:            String

    /// Indicator that monitor output should be buffered as to give more precise trace timestamps.
    public var bufferTraceMessages: Bool = true

    /// Initialise.
    /// - Parameter traceTag: The trace line tag that prepends a trace message.
    /// - Parameter bufferTraceMessages: Indicator that monitor output should be buffered as to give more precise trace timestamps.
    public init(traceTag: String, bufferTraceMessages: Bool = false)
    {
        self.traceTag = traceTag
        self.bufferTraceMessages = bufferTraceMessages
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxMonType_Observable<ItemInType, ItemOutType>: RxMon action for monitoring events related to Observables.
///
/// - eObservable_subscribeToObserver:                      A subscription to an observer occurred.
/// - eObservable_subscribeToObserverManagingSubscription:  A subscription to an observer managing subscription occurred.
/// - eObservable_unsubscribeToObserver:                    An un-subscription to an observer occurred.
/// - eObservable_deferredSubscribeToObserver:              A deferred subscription to an observer occurred.
/// - eObservable_subscribeToNullObserver:                  A non-observed subscription occurred.
/// - eObservable_unsubscribeToNullObserver:                A non-observed unsubscription occurred.
/// - eCustomAction_subscribeToCustomAction:                A subscription to a custom action occurred.
/// - eObservable_unsubscribeToCustomAction:                An un-subscription to a custom action occurred.
/// - eObservable_deferredSubscribeToCustomAction:          A deferred subscription to a custom action occurred.
/// - eObservable_notifyItem:                               Observable received an item notification.
/// - eObservable_notifyCompleted:                          Observable received a completed notification.
/// - eObservable_notifyStateChange:                        Observable received a state change.
/// - eEvalNode_notifyItemIn:                               EvalNode received an item notification.
/// - eEvalNode_notifyCompletedIn:                          EvalNode received a completed notification.
/// - eEvalNode_notifyStateChangeIn:                        EvalNode received a state change.
///

public enum eRxMonType_Observable<ItemInType, ItemOutType> : CustomStringConvertible
{
    // Observable subscription.

    case eObservable_subscribeToObserver(RxOperator<ItemInType, ItemOutType>, ARxConsumer<ItemOutType>, RxSubscription)
    case eObservable_subscribeToObserverManagingSubscription(RxOperator<ItemInType, ItemOutType>, ARxObserverManagingSubscription<ItemOutType>, RxSubscription)
    case eObservable_unsubscribeToObserver(RxOperator<ItemInType, ItemOutType>, ARxConsumer<ItemOutType>, RxSubscription)
    case eObservable_deferredSubscribeToObserver(RxOperator<ItemInType, ItemOutType>, ARxConsumer<ItemOutType>, RxSubscription)

    case eObservable_subscribeToNullObserver(RxOperator<ItemInType, ItemOutType>, RxSubscription)
    case eObservable_unsubscribeToNullObserver(RxOperator<ItemInType, ItemOutType>, RxSubscription)

    case eCustomAction_subscribeToCustomAction(RxOperator<ItemInType, ItemOutType>, RxSubscription)
    case eCustomAction_unsubscribeToCustomAction(RxOperator<ItemInType, ItemOutType>, RxSubscription)
    case eObservable_deferredSubscribeToCustomAction(RxOperator<ItemInType, ItemOutType>, RxSubscription)


    // Observable notification (subjects).

    case eObservable_notifyItem(RxOperator<ItemInType, ItemOutType>, ItemInType)
    case eObservable_notifyCompleted(RxOperator<ItemInType, ItemOutType>, IRxError?)
    case eObservable_notifyStateChange(RxOperator<ItemInType, ItemOutType>, eRxEvalStateChange)


    // EvalNode notification.

    case eEvalNode_notifyItemIn(RxEvalNode<ItemInType, ItemOutType>, ItemInType)
    case eEvalNode_notifyCompletedIn(RxEvalNode<ItemInType, ItemOutType>, IRxError?)
    case eEvalNode_notifyStateChangeIn(RxEvalNode<ItemInType, ItemOutType>, eRxEvalStateChange)


    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
                // Observable subscription.

            case .eObservable_subscribeToObserver(let (observable, observer, subscription)):
                return "subscribe(\(subscription))\tsubscribeObservable(\(observable.tag))\t=> observer(\(observer.tag))"

            case .eObservable_subscribeToObserverManagingSubscription(let (observable, observer, subscription)):
                return "subscribe(\(subscription))\tsubscribeObservable(\(observable.tag))\t=> observer managing subscription(\(observer.tag))"

            case .eObservable_unsubscribeToObserver(let (observable, observer, subscription)):
                return "\tunsubscribe(\(subscription))\tsubscribedObservable(\(observable.tag) => observer(\(observer.tag))"

            case .eObservable_deferredSubscribeToObserver(let (observable, observer, subscription)):
                return "deferred subscribe(\(subscription))\tsubscribeObservable(\(observable.tag))\t=> observer(\(observer.tag))"


            case .eObservable_subscribeToNullObserver(let (observable, subscription)):
                return "subscribe(\(subscription))\tno observation observable(\(observable.tag))"

            case .eObservable_unsubscribeToNullObserver(let (observable, subscription)):
                return "\tunsubscribe(\(subscription))\tsubscribedObservable(\(observable.tag)\tfor null-consumer"


                // Custom Action subscription.

            case .eCustomAction_subscribeToCustomAction(let (observable, subscription)):
                return "subscribe(\(subscription))\tsubscribeObservable(\(observable.tag))\tfor custom-action"

            case .eCustomAction_unsubscribeToCustomAction(let (observable, subscription)):
                return "\tunsubscribe(\(subscription))\tsubscribedObservable(\(observable.tag)\tfor custom-action"

            case .eObservable_deferredSubscribeToCustomAction(let (observable, subscription)):
                return "deferred subscribe(\(subscription))\ttsubscribedObservable(\(observable.tag))\tfor custom-action"


                // Observable notification.

            case .eObservable_notifyItem(let (observable, item)):
                return "\tnotifyItem(\(String(describing: item)))\t\t=> observable(\(observable.tag))"

            case .eObservable_notifyCompleted(let (observable, error)):
                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> evalNode(\(observable.tag))"

            case .eObservable_notifyStateChange(let (observable, stateChange)):
                return "\tnotifyStateChange(\(String(describing: stateChange.description)))\t=> evalNode(\(observable.tag))"


                // EvalNode notification.

            case .eEvalNode_notifyItemIn(let (evalNode, item)):

                if let description = evalNode.rawDescription
                {
                    return "\tnotifyItem(\(String(describing: item)))\t\t=> observable(\(evalNode.tag)) \(description)"
                }

                return "\tnotifyItem(\(String(describing: item)))\t\t=> observable(\(evalNode.tag))"

            case .eEvalNode_notifyCompletedIn(let (evalNode, error)):

                if let description = evalNode.rawDescription
                {
                    return "\tnotifyCompleted(\(String(describing: error)))\t\t=> observable(\(evalNode.tag)) \(description)"
                }

                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> observable(\(evalNode.tag))"

            case .eEvalNode_notifyStateChangeIn(let (evalNode, stateChange)):
                return "\tnotifyStateChange(\(stateChange.description))\t=> observable(\(evalNode.tag))"
        }
    }

    /// Indicate if the event's object instanceID is in the given set.
    public func hasInstanceID(_ instanceIDs : Set<RxInstanceID>) -> RxInstanceID?
    {
        switch self
        {
                // Observable subscription.

            case .eObservable_subscribeToObserver(let (observable, observer, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(observer.instanceID)       { return observer.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

            case .eObservable_subscribeToObserverManagingSubscription(let (observable, observer, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(observer.instanceID)       { return observer.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

            case .eObservable_unsubscribeToObserver(let (observable, observer, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(observer.instanceID)       { return observer.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil


            case .eObservable_deferredSubscribeToObserver(let (observable, observer, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(observer.instanceID)       { return observer.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil


            case .eObservable_subscribeToNullObserver(let (observable, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

            case .eObservable_unsubscribeToNullObserver(let (observable, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil


                // Custom Action subscription.

            case .eCustomAction_subscribeToCustomAction(let (observable, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

            case .eCustomAction_unsubscribeToCustomAction(let (observable, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

            case .eObservable_deferredSubscribeToCustomAction(let (observable, subscription)):

                if instanceIDs.contains(observable.instanceID)     { return observable.instanceID }
                if instanceIDs.contains(subscription.instanceID)   { return subscription.instanceID }

                return nil

                // Observable notification.

            case .eObservable_notifyItem(let (observable, _)):
                return instanceIDs.contains(observable.instanceID) ? observable.instanceID : nil

            case .eObservable_notifyCompleted(let (observable, _)):
                return instanceIDs.contains(observable.instanceID) ? observable.instanceID : nil

            case .eObservable_notifyStateChange(let (observable, _)):
                return instanceIDs.contains(observable.instanceID) ? observable.instanceID : nil


                // EvalNode notification.

            case .eEvalNode_notifyItemIn(let (evalNode, _)):
                return instanceIDs.contains(evalNode.instanceID) ? evalNode.instanceID : nil

            case .eEvalNode_notifyCompletedIn(let (evalNode, _)):
                return instanceIDs.contains(evalNode.instanceID) ? evalNode.instanceID : nil

            case .eEvalNode_notifyStateChangeIn(let (evalNode, _)):
                return instanceIDs.contains(evalNode.instanceID) ? evalNode.instanceID : nil


        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxMonType_Consumer<ItemType>: RxMon action for monitoring events related to Consumers.
///
/// - eObserver_notifyItem:                                 Observer received an item notification.
/// - eObserver_notifyCompleted:                            Observer received a completed notification.
/// - eObserver_notifyStateChange:                          Observer received a state change.
/// - eObserver_notifyItemManagingSubscription:             Managing Observer received an item notification.
/// - eObserver_notifyCompletedManagingSubscription:        Managing Observer received a completed notification.
/// - eObserver_notifyStateChangeManagingSubscription:      Managing Observer received a state change.
/// - eNullObserver_notifyItem:                             A Non-Observed subscription received an item notification.
/// - eNullObserver_notifyCompleted:                        A Non-Observed subscription received a completed notification.
/// - eNullObserver_notifyStateChange:                      A Non-Observed subscription received a state change.
/// - eNotifier_notifyItem:                                 A Notifier received an item notification.
/// - eNotifier_notifyCompleted:                            A Notifier received a completed notification.
/// - eNotifier_notifyStateChange:                          A Notifier received a state change.
/// - eCustomAction_notifyItem:                             A subscription to a custom action received an item notification.
/// - eQueue_notifyItem:                                    A Notification Queue received an item notification.
/// - eQueue_notifyCompleted:                               A Notification Queue received a completed notification.
/// - eQueue_notifyStateChange:                             A Notification Queue received a state change.
///

public enum eRxMonType_Consumer<ItemType> : CustomStringConvertible
{
    // Observer notification.
    case eObserver_notifyItem(ARxConsumer<ItemType>, RxSubscription, ItemType)
    case eObserver_notifyCompleted(ARxConsumer<ItemType>, IRxError?, RxSubscription)
    case eObserver_notifyStateChange(ARxConsumer<ItemType>, eRxEvalStateChange, RxSubscription)

    /// Observer managing subscription notification.
    case eObserver_notifyItemManagingSubscription(ARxObserverManagingSubscription<ItemType>, RxSubscription, ItemType)
    case eObserver_notifyCompletedManagingSubscription(ARxObserverManagingSubscription<ItemType>, IRxError?, RxSubscription)
    case eObserver_notifyStateChangeManagingSubscription(ARxObserverManagingSubscription<ItemType>, eRxEvalStateChange, RxSubscription)

    // Null Observer notification.
    case eNullObserver_notifyItem(ARxNotifier<ItemType>, RxSubscription, ItemType)
    case eNullObserver_notifyCompleted(ARxNotifier<ItemType>, IRxError?, RxSubscription)
    case eNullObserver_notifyStateChange(ARxNotifier<ItemType>, eRxEvalStateChange, RxSubscription)

    // Notifier notification.
    case eNotifier_notifyItem(ARxNotifier<ItemType>, ItemType)
    case eNotifier_notifyCompleted(ARxNotifier<ItemType>, IRxError?)
    case eNotifier_notifyStateChange(ARxNotifier<ItemType>, eRxEvalStateChange)

    // Custom Action notification.
    case eCustomAction_notifyItem(RxSubscription, ItemType)

    // RxQueue notification.
    case eQueue_notifyItem(ARxNotificationQueue<ItemType>, ItemType)
    case eQueue_notifyCompleted(ARxNotificationQueue<ItemType>, IRxError?)
    case eQueue_notifyStateChange(ARxNotificationQueue<ItemType>, eRxEvalStateChange)

    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
                // Observer notification.

            case .eObserver_notifyItem(let (observer, _, item)):
                return "\tnotifyItem(\(String(describing: item)))\t\t=> observer(\(observer.tag))"

            case .eObserver_notifyCompleted(let (observer, error, _)):
                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> observer(\(observer.tag))"

            case .eObserver_notifyStateChange(let (observer, stateChange, _)):
                return "\tnotifyStateChange(\(stateChange.description))\t=> observer(\(observer.tag))"

                // Observer managing subscription notification.

            case .eObserver_notifyItemManagingSubscription(let (observer, subscription, item)):
                return "\tnotifyItem(\(String(describing: item)), \(subscription.tag))\t\t=> observer(\(observer.tag))"

            case .eObserver_notifyCompletedManagingSubscription(let (observer, error, subscription)):
                return "\tnotifyCompleted(\(String(describing: error)), \(subscription.tag))\t\t=> observer(\(observer.tag))"

            case .eObserver_notifyStateChangeManagingSubscription(let (observer, stateChange, subscription)):
                return "\tnotifyStateChange(\(stateChange.description), \(subscription.tag))\t=> observer(\(observer.tag))"

                // Null Observer notification.

            case .eNullObserver_notifyItem(let (notifier, _, item)):

                if let description = notifier.rawDescription
                {
                    return "\tnotifyItem(\(String(describing: item)))\t\t=> null-observer(\(notifier.tag)) \(description)"
                }

                return "\tnotifyItem(\(String(describing: item)))\t\t=> nullNotifier(\(notifier.tag))"

            case .eNullObserver_notifyCompleted(let (notifier, error, _)):

                if let description = notifier.rawDescription
                {
                    return "\tnotifyCompleted(\(String(describing: error)))\t\t=> null-observer(\(notifier.tag)) \(description)"
                }

                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> null-observer(\(notifier.tag))"

            case .eNullObserver_notifyStateChange(let (notifier, stateChange, _)):

                return "\tnotifyStateChange(\(stateChange.description))\t=> null-observer(\(notifier.tag))"


                // Notifier notification.

            case .eNotifier_notifyItem(let (notifier, item)):

                if let description = notifier.rawDescription
                {
                    return "\tnotifyItem(\(String(describing: item)))\t\t=> notifier(\(notifier.tag)) \(description)"
                }

                return "\tnotifyItem(\(String(describing: item)))\t\t=> notifier(\(notifier.tag))"

            case .eNotifier_notifyCompleted(let (notifier, error)):

                if let description = notifier.rawDescription
                {
                    return "\tnotifyCompleted(\(String(describing: error)))\t\t=> notifier(\(notifier.tag)) \(description)"
                }

                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> notifier(\(notifier.tag))"

            case .eNotifier_notifyStateChange(let (notifier, stateChange)):
                return "\tnotifyStateChange(\(stateChange.description))\t=> notifier(\(notifier.tag))"


                // Custom Action notification.

            case .eCustomAction_notifyItem(let item):
                return "\tnotifyItem(\(String(describing: item)))\t=> customAction)"


                // RxQueue notification.

            case .eQueue_notifyItem(let (notificationQueue, item)):
                return "\tnotifyItem(\(String(describing: item)))\t\t=> notificationQueue(\(notificationQueue.tag))"

            case .eQueue_notifyCompleted(let (notificationQueue, error)):
                return "\tnotifyCompleted(\(String(describing: error)))\t\t=> notification queue(\(notificationQueue.tag))"

            case .eQueue_notifyStateChange(let (notificationQueue, stateChange)):
                return "\tnotifyStateChange(\(stateChange.description))\t=> notification queue(\(notificationQueue.tag))"

        }
    }

    /// Indicate if the event's object instanceID is in the given set.
    public func hasInstanceID(_ instanceIDs : Set<RxInstanceID>) -> RxInstanceID?
    {
        switch self
        {
                // Observer notification.

            case .eObserver_notifyCompleted(let (observer, _, _ )):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil

            case .eObserver_notifyStateChange(let (observer, _, _ )):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil

            case .eObserver_notifyItem(let (observer, _, _)):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil


                // Observer managing subscription notification.

            case .eObserver_notifyItemManagingSubscription(let (observer, _, _)):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil

            case .eObserver_notifyCompletedManagingSubscription(let (observer, _, _)):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil

            case .eObserver_notifyStateChangeManagingSubscription(let (observer, _, _)):

                if instanceIDs.contains(observer.instanceID)        { return observer.instanceID }

                return nil


                // Null Observer notification.

            case .eNullObserver_notifyItem(let (notifier, _, _)):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil

            case .eNullObserver_notifyCompleted(let (notifier, _, _ )):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil

            case .eNullObserver_notifyStateChange(let (notifier, _, _ )):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil


                // Notify notification.

            case .eNotifier_notifyItem(let (notifier, _)):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil

            case .eNotifier_notifyCompleted(let (notifier, _ )):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil

            case .eNotifier_notifyStateChange(let (notifier, _ )):
                return instanceIDs.contains(notifier.instanceID) ? notifier.instanceID : nil

                // Custom Action notification.

            case .eCustomAction_notifyItem:
                return nil


                // Queue notification.

            case .eQueue_notifyItem(let (notificationQueue, _)):
                return instanceIDs.contains(notificationQueue.instanceID) ? notificationQueue.instanceID : nil

            case .eQueue_notifyCompleted(let (notificationQueue, _ )):
                return instanceIDs.contains(notificationQueue.instanceID) ? notificationQueue.instanceID : nil

            case .eQueue_notifyStateChange(let (notificationQueue, _ )):
                return instanceIDs.contains(notificationQueue.instanceID) ? notificationQueue.instanceID : nil
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxMonType_Action: RxMon action for monitoring events not related to ItemType (non-generic actions).
///
/// - eSubscriptionDisposalBegin:                           Subscription disposal has begun.
/// - eSubscriptionDisposalEnd:                             Subscription disposal has ended.
/// - eSubscriptionWaitForDisposalBegin:                    A subscription waitForDisposal call has begun.
/// - eSubscriptionWaitForDisposalEnd:                      A subscription waitForDisposal call has ended.
/// - eSubscriptionDisposaMonitorReceivedNotice:            A subscription RxDisposalMonitor received an event notice.
/// - eSubscriptionDisposalMonitorGiveDisposalEndNotice:    A subscription RxDisposalMonitor sent a disposal end event notice.
/// - eSubscriptionDeferredAsyncRunBegin:                   A deferred subscription has begun evaluation.
/// - eSubscription_Failed:                                 A subscription has failed.
/// - eSDKError:                                            The SDK issued an SDK error.
/// - eCustomAction_notifyCompleted:                        A subscription to a custom action received a completed notification.
/// - eCustomAction_notifyStateChange:                      A subscription to a custom action received a state change.
/// - eComment:                                             A string comment for debugging purposes.
///

public enum eRxMonType_Action : CustomStringConvertible
{
    case eTagObject(RxObject)

    case eSubscriptionDisposalBegin(RxDisposable)
    case eSubscriptionDisposalEnd(RxInstanceID, String)
    case eSubscriptionWaitForDisposalBegin(RxSubscription)
    case eSubscriptionWaitForDisposalEnd(RxInstanceID, String, Bool)
    case eSubscriptionDisposaMonitorReceivedNotice(RxInstanceID, String, oRxSubscriptionEvent_Options)
    case eSubscriptionDisposalMonitorGiveDisposalEndNotice(RxInstanceID, String)
    case eSubscriptionDeferredAsyncRunBegin(RxSubscription)
    case eSubscription_Failed(RxSubscription)

    case eSDKError(IRxError)

    case eCustomAction_notifyCompleted(IRxError?, RxSubscription)
    case eCustomAction_notifyStateChange(eRxEvalStateChange, RxSubscription)


    case eComment(String)

    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
                // Object handling.

            case .eTagObject(let object):
                return "tag object(\(object.tag))"


                // Subscription monitoring

            case .eSubscriptionDisposalBegin(let subscription):
                return "subscription disposal begin(\(subscription.tag))"

            case .eSubscriptionDisposalEnd(let (_, subscriptionTag)):
                return "subscription disposal end(\(subscriptionTag))"

            case .eSubscriptionWaitForDisposalBegin(let subscription):
                return "subscription wait for disposal begin(\(subscription.tag))"

            case .eSubscriptionWaitForDisposalEnd(let (_, subscriptionTag, timedOut)):

                let message = "subscription wait for disposal end(\(subscriptionTag))"

                return !timedOut ? message : message + " timed-out"

            case .eSubscriptionDisposaMonitorReceivedNotice(let (_, subscriptionTag, event)):
                return "subscription DisposalMonitor \(subscriptionTag) received notice(\(event.description))"

            case .eSubscriptionDisposalMonitorGiveDisposalEndNotice(let (_, subscriptionTag)):
                return "subscription DisposalMonitor sent disposal notice to WaitForDisposal(\(subscriptionTag))"

            case .eSubscriptionDeferredAsyncRunBegin(let subscription):
                return "deferred subscription runAsync begin(\(subscription.tag))"

            case .eSubscription_Failed(let subscription):
                return "subscription failed(\(subscription.tag))"


                // Error

            case .eSDKError(let error):
                return "SDK error \(error)"

                // Custom Action

            case .eCustomAction_notifyCompleted(let (error, _)):
                return "\tnotifyCompleted(\(String(describing: error)))\t=> customAction)"

            case .eCustomAction_notifyStateChange(let (stateChange, _)):
                return "\tnotifyStateChange(\(stateChange.description))\t=> customAction)"


                // Comment monitor messages.

           case .eComment(let comment):
               return "\tcomment(\(comment))"
        }
    }

    /// Indicate if the event's object instanceID is in the given set.
    public func hasInstanceID(_ instanceIDs : Set<RxInstanceID>) -> RxInstanceID?
    {
        switch self
        {
                // Object handling.

            case .eTagObject(let object):
                return object.instanceID


                // Subscription monitoring

            case .eSubscriptionDisposalBegin(let subscription):
                return instanceIDs.contains(subscription.instanceID) ? subscription.instanceID : nil

            case .eSubscriptionDisposalEnd(let (subscriptionInstanceID, _)):
                return instanceIDs.contains(subscriptionInstanceID) ? subscriptionInstanceID : nil

            case .eSubscriptionWaitForDisposalBegin(let subscription):
                return instanceIDs.contains(subscription.instanceID) ? subscription.instanceID : nil

            case .eSubscriptionWaitForDisposalEnd(let (subscriptionInstanceID, _, _)):
                return instanceIDs.contains(subscriptionInstanceID) ? subscriptionInstanceID : nil

            case .eSubscriptionDisposaMonitorReceivedNotice(let (subscriptionInstanceID, _, _)):
                return instanceIDs.contains(subscriptionInstanceID) ? subscriptionInstanceID : nil

            case .eSubscriptionDisposalMonitorGiveDisposalEndNotice(let (subscriptionInstanceID, _)):
                return instanceIDs.contains(subscriptionInstanceID) ? subscriptionInstanceID : nil

            case .eSubscriptionDeferredAsyncRunBegin(let subscription):
                return instanceIDs.contains(subscription.instanceID) ? subscription.instanceID : nil

            case .eSubscription_Failed(let subscription):
                return instanceIDs.contains(subscription.instanceID) ? subscription.instanceID : nil

                // SDK Error

            case .eSDKError:
                 return instanceIDs.first

                // Custom Action.

            case .eCustomAction_notifyCompleted:
                return nil

            case .eCustomAction_notifyStateChange:
                return nil

                // Comment

           case .eComment:

                return -1
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxMonType_Instance: RxMon instance monitoring events (non-generic).
///
/// - eCreateObject:                            An RxObject based instance was created.
/// - eDestroyObject:                           An RxObject based instance was destroyed.
/// - eStopEvalQueue:                           An RxEvalQueue was stopped.
///

public enum eRxMonType_Instance : CustomStringConvertible
{
    case eCreateObject(RxObject)
    case eDestroyObject(RxObject)
    case eStopEvalQueue(RxEvalQueue)

    /// The timestamp for the event.
    public var timestamp : RxRelTime?
    {
        #if RxMonEnabled
            switch self
            {
                case .eCreateObject(let object):
                    return object.instanceTime

                case .eDestroyObject:
                    return nil

                case .eStopEvalQueue:
                    return nil
            }
        #else
            return nil
        #endif
    }

    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
            case .eCreateObject(let object):
                return "create \(object.objectType.description)(\(object.tag))"

            case .eDestroyObject(let object):
                return "destroy \(object.objectType.description)(\(object.tag))"

            case .eStopEvalQueue(let evalQueue):
                return "stop eval queue \(evalQueue.tag)"
        }
    }

    /// CustomStringConvertible compliance.
    public func fullDescription(_ isRetrospective : Bool) -> String
    {
        switch self
        {
            case .eCreateObject(let object):
                return isRetrospective  ? "create (retrospective) \(object.objectType.description)(\(object.tag))"
                                        : "create \(object.objectType.description)(\(object.tag))"

            case .eDestroyObject(let object):
                return "destroy \(object.objectType.description)(\(object.tag))"

            case .eStopEvalQueue(let evalQueue):
                return "stop eval queue \(evalQueue.tag)"
        }
    }

    /// Indicate if the event's object instanceID is in the given set.
    public func hasInstanceID(_ instanceIDs : Set<RxInstanceID>) -> RxInstanceID?
    {
        switch self
        {
            case .eCreateObject(let object):
                return instanceIDs.contains(object.instanceID) ? object.instanceID : nil

            case .eDestroyObject(let object):
                return instanceIDs.contains(object.instanceID) ? object.instanceID : nil

            case .eStopEvalQueue(let evalQueue):
                return instanceIDs.contains(evalQueue.instanceID) ? evalQueue.instanceID : nil
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxMon: Instrumented monitoring of SDK operations.
///
///     Use:         For monitoring SDK operations on RxObservables, RxObservers, RxNotifiers and evaluation.
///     Operation:   Author a class that conforms to IRxMon and add it to the monitoring system (See RxMon.addMonitor()).
///

public protocol IRxMon
{
    /// Monitor instance lifecycle delegate (non-ItemType).
    /// - Parameter monType: The monitored instance event.
    func onInstanceEvent(_ monType : eRxMonType_Instance)

    /// Monitor action on object associated with an ItemType.
    /// - Parameter monType: The monitored event.
    func onObservableEvent<ItemInType, ItemOutType>(_ monType : eRxMonType_Observable<ItemInType, ItemOutType>)

    /// Monitor action with associated Item.
    /// - Parameter monType: The monitored item action event.
    func onNotifiableEvent<ItemType>(_ monType : eRxMonType_Consumer<ItemType>)

    /// Monitor operational action (non-generic).
    /// - Parameter  monType: The monitored action event.
    func onOpAction(_ monType : eRxMonType_Action)
}

public extension IRxMon
{
    /// Monitor instance lifecycle delegate (non-ItemType).
    /// - Parameter monType: The monitored instance event.
    public func onInstanceEvent(_ monType : eRxMonType_Instance)
    {
        // Default behaviour: do nothing.
    }

    /// Monitor action on Observable.
    /// - Parameter monType: The monitored event.
    public func onObservableEvent<ItemInType, ItemOutType>(_ monType : eRxMonType_Observable<ItemInType, ItemOutType>)
    {
        // Default behaviour: do nothing.
    }

    /// Monitor action Notifiable.
    /// - Parameter monType: The monitored item action event.
    public func onNotifiableEvent<ItemType>(_ monType : eRxMonType_Consumer<ItemType>)
    {
        // Default behaviour: do nothing.
    }

    /// Monitor operational action (non-generic).
    /// - Parameter  monType: The monitored action event.
    public func onOpAction(_ monType : eRxMonType_Action)
    {
        // Default behaviour: do nothing.
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxTestMonitoredObject:  A Test Object that is monitored and can be used for checking for object deallocation in closures.
///

open class RxTestMonitoredObject : RxObject
{
    /// The RxObject type of RxTestMonitoredObject
    open override var objectType : eRxObjectType          { return .eRxObject_RxTestMonitoredObject }

    public init(tag: String = "RxTestMonitoredObject", traceTag: String? = nil)
    {
        super.init(tag:tag)

        if traceTag != nil
        {
            trace("> RxTestMonitoredObject >>")
        }
    }
}
