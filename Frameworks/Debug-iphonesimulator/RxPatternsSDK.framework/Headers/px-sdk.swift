//
// Created by Terry Stillone (http://www.originware.com) on 17/03/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// This file, all proprietary knowledge and algorithms it details are the sole property of
// Originware unless otherwise specified. The software, this file
// belong with is the confidential and proprietary information of Originware.
// ("Confidential Information"). You shall not disclose such
// Confidential Information and shall use it only in accordance with the terms
// of the license agreement you entered into with Originware.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSDK: `The RxPatterns SDK Management: versioning, configuration, startup and shutdown.
///

public struct RxSDK
{
    /// The RxSDK lifecycle events.
    public enum eSDKEvent
    {
        /// The SDK is starting.
        case eSDKStart

        /// The SDK has been shutdown.
        case eSDKShutdown

        /// The SDK logger has bee set.
        case eSetLogger(RxLogger?)

        /// A RxMon monitor has been added for operation.
        case eAddMonitor(IRxMon)

        /// A RxMon monitor has been removed from operation.
        case eRemoveMonitor(IRxMon)
    }

    /// The RxPatterns SDK Version.
    public struct version
    {
        /// The Major SDK Version
        public static let Major = 0

        /// The Minor SDK Version.
        public static let Minor = 91

        /// The version Variant.
        public static let Variant = "RxPatterns iOS Eval Kit"
    }

    /// The control of the SDK.
    public struct control
    {
        /// SDK Enabler.
        public static var enabled : Bool = false

        /// Start the SDK.
        public static func start()           { onSDKEvent(.eSDKStart) }

        /// Shutdown the SDK.
        public static func shutdown()        { onSDKEvent(.eSDKShutdown) }

        /// Start the SDK and check licensing.
        public static var onSDKEvent : (eSDKEvent) -> Void = { (event : eSDKEvent) in

            switch event
            {
                case .eSDKStart:
                    if !enabled
                    {
                        // Enable operation.
                        enabled = true

                        // Start Tracing.
                        #if RxTraceEnabled
                            if RxSDK.log.logger == nil
                            {
                                RxSDK.log.logger = RxLogger()
                            }

                        #endif

                        #if DEBUG
                            /// Global RxTrace message switch.
                            RxSDK.log.logEnable = true

                            /// Global RxTrace error switch.
                            RxSDK.log.logEnable = true
                        #else
                            /// Global RxTrace message switch.
                            RxSDK.log.logEnable = false

                            /// Global RxTrace error switch.
                            RxSDK.log.logEnable = false
                        #endif
                    }

                case .eSDKShutdown:

                    if enabled
                    {
                        // Disable operation.
                        enabled = false

                        // Shutdown Monitors.
                        RxMon.shutdown()

                        // Shutdown Tracing.
                        #if RxTraceEnabled
                            RxSDK.log.logger?.shutdown()
                            RxSDK.log.logger = nil
                        #endif

                        /// Shutdown All RxEvalQueue operations
                        RxSDK.evalQueue.stopAll()
                        RxEvalQueueManager.shutdown()
                    }

                case .eAddMonitor(let mon):

                    #if RxMonEnabled
                        switch mon
                        {
                            case (let traceMon as RxMonTraceRxObjects):
                                RxSDK.mon.trace = traceMon

                            default:
                                break
                        }
                    #endif

                case .eRemoveMonitor(let mon):

                    #if RxMonEnabled
                        switch mon
                        {
                            case _ where mon is RxMonTraceRxObjects:
                                RxSDK.mon.trace = nil

                            default:
                                break
                        }
                    #endif
                
                case .eSetLogger(let logger):

                    RxSDK.log.logger = logger
            }
        }
    }


    /// The current error handling settings.
    public struct error
    {
        /// Used in comparing RxNotifications timestamps.
        public static var TimestampComparator : RxMiscTypes.RxTimestampComparator? = nil

        /// Error handler for SDK.
        public static var handler: (IRxError) -> Void = { (error: IRxError) in

#if RxMonEnabled
            RxMon.monAction(eRxMonType_Action.eSDKError(error))
#endif

            assert(false, error.description)
        }
    }

    /// The current RxMon monitor settings.
    public struct mon
    {
        /// RxObject Constants.
        public struct Constant
        {
            /// The RxObject base URI assigned to internal SDK objects.
            public static let RxSystemTag = "RxSystem"

            /// The RxObject monitor key
            public static let RxObjectMonKey = "RxObjectMon"
        }

#if RxMonEnabled
        /// Global access to the RxMonTraceRxObjects instance.
        public static var trace : RxMonTraceRxObjects? = nil

        public static func startTraceSession(startTime: RxRelTime?, traceTag : String, bufferTraceMessages : Bool) -> RxMonTraceRxObjects
        {
            let key = RxSDK.mon.Constant.RxObjectMonKey

            if let mon = RxMon.getMonitor(key) as? RxMonTraceRxObjects
            {
                if (mon.startTime == nil) && (startTime != nil)
                {
                    mon.startTime = startTime
                }
                
                return mon
            }
            else
            {
                let mon = RxMonTraceRxObjects(traceTag: traceTag, bufferTraceMessages: bufferTraceMessages)

                RxMon.addMonitor(key, monitor: mon)

                RxSDK.mon.trace = mon
                RxSDK.mon.trace!.startTime = startTime

                return mon
            }
        }

        public static func stopTrace()
        {
            let key = RxSDK.mon.Constant.RxObjectMonKey

            if RxMon.hasMonitor(key)
            {
                RxMon.removeMonitor(RxSDK.mon.Constant.RxObjectMonKey)
            }
        }
#endif

    }

    /// Operational limits.
    public struct limits
    {
        /// When performing synchronous closure calls rather than dispatching, the max number of call levels.
        public static var MaxSyncCallStackLimit = 20
    }

    /// RxInstanceCounter:  A sequential cardinal number generator for RxObject instance IDs
    public struct instance
    {
        /// The current instance counter value.
        private static var counter : RxInstanceID = 1

        /// Get the next Instance ID
        static public var nextInstanceID: RxInstanceID
        {
            let currentCount = counter

            counter += 1

            return currentCount
        }
    }

    /// The current RxLog settings.
    public struct log
    {
        /// The message indent length.
        public static var IndentLength      = 2

        /// The message header length.
        public static var TraceHeaderLength = 30

        /// The message content length.
        public static var ContentLength     = 100

        /// Enable log messages (independant of error messages)
        public static var logEnable          = true

        /// Enable logError messages.
        public static var logErrorEnable     = true

        /// The trace logger, set by the start func.
        public static var logger: IRxLogger? = RxLogger()
    }

    /// Special purpose queues.
    public struct evalQueue
    {
        //<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

        /// The RxEvalQueue associated with the Main UI Dispatch Queue
        public static var UIThreadQueue: RxEvalQueue             { return getEvalQueue(eRxEvalQueueType.eUIThread) }

        /// The RxEvalQueue used for timer action calls.
        public static var Timer: RxEvalQueue                     { return getEvalQueue(eRxEvalQueueType.eTimer) }

        /// The RxEvalQueue used for disposal execution.
        public static var DisposalQueue: RxEvalQueue             { return getEvalQueue(eRxEvalQueueType.eDisposal) }

        /// The RxEvalQueue used for disposal request queueing.
        public static var DisposalQueuer: RxEvalQueue            { return getEvalQueue(eRxEvalQueueType.eDisposalQueuer) }

        /// The RxEvalQueue used for barrier blocking.
        public static var BarrierQueue: RxEvalQueue              { return getEvalQueue(eRxEvalQueueType.eBarrier) }

        /// The RxEvalQueue used for queueing notifications.
        public static var Queuer: RxEvalQueue                    { return getEvalQueue(eRxEvalQueueType.eQueuer)}

        /// The RxEvalQueue used for the RxEvalQueueManager.
        public static var EvalQueueManager: RxEvalQueue          { return getEvalQueue(eRxEvalQueueType.eEvalQueueManagerQueuer)}

        //<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

        // Store the referenced eval queues.
        static var m_queueByQueueType = [Int : RxEvalQueue]()

        fileprivate static func getEvalQueue(_ evalQueueType : eRxEvalQueueType) -> RxEvalQueue
        {
            if let evalQueue = m_queueByQueueType[evalQueueType.rawValue]
            {
                return evalQueue
            }

            func createEvalQueue() -> RxEvalQueue
            {
                /// The RxObject tag for the RxEvalQueue object.
                let RxSystemTag =  RxSDK.mon.Constant.RxSystemTag

                switch evalQueueType
                {
                    case eRxEvalQueueType.eDisposal:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/DisposalEvalQueue", evalQueueType: eRxEvalQueueType.eDisposal)

                    case eRxEvalQueueType.eDisposalQueuer:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/DisposalQueuerEvalQueue", evalQueueType: eRxEvalQueueType.eDisposalQueuer)

                    case eRxEvalQueueType.eQueuer:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/QueuerEvalQueue", evalQueueType: eRxEvalQueueType.eQueuer)

                    case eRxEvalQueueType.eBarrier:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/BarrierQueue", evalQueueType: eRxEvalQueueType.eBarrier)

                    case eRxEvalQueueType.eTimer:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/TimerEvalQueue", evalQueueType: eRxEvalQueueType.eTimer)

                    case eRxEvalQueueType.eUIThread:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/UIEvalQueue", evalQueueType: eRxEvalQueueType.eUIThread)

                    case eRxEvalQueueType.eEvalQueueManagerQueuer:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/EvalQueueManager", evalQueueType: eRxEvalQueueType.eEvalQueueManagerQueuer)

                    case eRxEvalQueueType.eMonitor:
                        return RxEvalQueue(sourceTag: RxSystemTag + "/Monitor", evalQueueType: eRxEvalQueueType.eMonitor)

                    default:
                        fatalError("Unexpected eval queue type")
                }
            }

            let evalQueueInstance = createEvalQueue()

            m_queueByQueueType[evalQueueType.rawValue] = evalQueueInstance

            return evalQueueInstance
        }

        /// Stop all special purpose eval queues.
        public static func stopAll()
        {
            for (_, queue) in m_queueByQueueType
            {
                queue.stopped()
            }

            m_queueByQueueType.removeAll()
        }
    }
}

