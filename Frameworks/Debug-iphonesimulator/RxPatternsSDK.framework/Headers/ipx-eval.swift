// ipx-eval.swift
// RxPatternsSDK
//
//  Created by Terry Stillone on 1/09/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import Dispatch

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eEvalStateChange: Identifies the current state of evaluation. All state changes are executed in the RxEvalQueue thread of the evaluation except for the eNewSubscriptionInSubscriptionThread state.
///
///     - eNewSubscriptionInSubscriptionThread: A new subscription is in progress. The state change is performed in the Subscription thread.
///     - eSubscriptionBegin:                   Subscription evaluation phase about to begin.
///     - eEvalBegin:                           Evaluation about to begin.
///     - eEvalEnd:                             Evaluation about to end (occurs after a Completed notification).
///     - eSubscriptionEnd                      Subscription is about to end.
///

public enum eRxEvalStateChange: CustomStringConvertible
{
    public typealias Index = Int
    public typealias _Element = eRxEvalStateChange

    /// A new subscription is in progress. The state change is performed in the Subscription thread.
    case eNewSubscriptionInSubscriptionThread(RxSubscription)

    /// Subscription evaluation phase about to begin.
    case eSubscriptionBegin(RxSubscription)

    /// Evaluation about to begin.
    case eEvalBegin(RxSubscription)

    /// Evaluation about to end (occurs afer a Completed notification).
    case eEvalEnd(RxSubscription)

    ///  Subscription is about to end.
    case eSubscriptionEnd(RxSubscription)

    /// The order of the enum within the enum collection.
    public var indexOf : RxIndexType
    {
        switch self
        {
            case .eNewSubscriptionInSubscriptionThread: return 0
            case .eSubscriptionBegin:                   return 1
            case .eEvalBegin:                           return 2
            case .eEvalEnd:                             return 4
            case .eSubscriptionEnd:                     return 5
        }
    }

    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
            case .eNewSubscriptionInSubscriptionThread: return "evalState: New Subscription"
            case .eSubscriptionBegin:                   return "evalState: Begin Subscription"
            case .eEvalBegin:                           return "evalState: Begin Eval"
            case .eEvalEnd:                             return "evalState: End Eval"
            case .eSubscriptionEnd:                     return "evalState: End Subscription"
        }
    }
}

/// Ordered compare operator for the eRxEvalStateChange enum.
public func <=(lhs: eRxEvalStateChange, rhs: eRxEvalStateChange) -> Bool
{
    return lhs.indexOf <= rhs.indexOf
}

/// Ordered compare operator for the optional eRxEvalStateChange? enum.
public func <=(lhs: eRxEvalStateChange?, rhs: eRxEvalStateChange?) -> Bool
{
    switch (lhs, rhs)
    {
    case (nil, _?):
        return true
        
    case (let lhsValue?, let rhsValue?):
        return lhsValue.indexOf <= rhsValue.indexOf
        
    default:
        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eTerminationType: Identifies the type of completion (termination) of a sequence.
///
///      - eCompleted:      Completion with no error.
///      - eError:          Completion with error.
///

public enum eTerminationType: Equatable, CustomStringConvertible
{
    /// Completion occurred (with no error).
    case eCompleted

    /// Termination occurred with the associated error.
    case eError (IRxError)

    /// Initialise, a valid error indicates an errored completion and nil indicates a successful completion.
    /// - Parameter error: The optional error of the completion.
    public init(error: IRxError? = nil)
    {
        if let error = error
        {
            self = .eError(error)
        }
        else
        {
            self = .eCompleted
        }
    }

    /// The Error for the termination, nil if no error.
    public var error : IRxError?
    {
        switch self
        {
            case .eError(let error):      return error
            case .eCompleted:             return nil
        }
    }

    /// CustomStringConvertible compliance.
    public var description : String
    {
        switch self
        {
            case .eError(let error):      return "Error: \(error.description)"
            case .eCompleted:             return "Completed"
        }
    }
}

/// Ordered compare operator for the etTerminationType enum.
public func ==(lhs: eTerminationType, rhs: eTerminationType) -> Bool
{
    switch (lhs, rhs)
    {
    case (.eError(let lhsError), .eError(let rhsError)):
        return lhsError.isEqual(rhsError)
        
    case (.eCompleted, .eCompleted):
        return true
        
    default:
        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxEvalNodeSupportOption: RxEvalNode support feature option.
///
///      - eSourceStartTime:       Option to request the source start time.
///      - eSourceTermination:     Option to request the source termination delegate.
///

public enum eRxEvalNodeSupport_Option: Int, RawRepresentable
{
    case eSourceStartTime = 0x1
    case eSourceTermination = 0x2

    /// Initialise by raw value.
    public init?(rawValue: Int)
    {
        switch rawValue
        {
            case 0x1:   self = .eSourceStartTime
            case 0x2:   self = .eSourceTermination

            default:    return nil
        }
    }

    /// The index of the enum item.
    public var indexOf : Int
    {
        switch self
        {
            case .eSourceStartTime:          return 0
            case .eSourceTermination:        return 1
        }
    }
}

///
/// oRxEvalNodeSupportOptions: RxEvalNode eval options.
///
///      - oSourceStartTime:       Request the source start time.
///      - oSourceTermination:     Request the source termination delegate.
///

public struct oRxEvalNodeSupport_Options: OptionSet, Sequence, Hashable
{
    private typealias GeneratorFunc = AnyIterator<eRxEvalNodeSupport_Option>

    /// The raw value of the option set.
    public let rawValue : Int

    /// The Request Option for a source start time.
    public static let oSourceStartTime       = oRxEvalNodeSupport_Options(.eSourceStartTime)

    /// The Request Option for a source termination delegate.
    public static let oSourceTermination     = oRxEvalNodeSupport_Options(.eSourceTermination)

    /// Init by raw value.
    public init(rawValue: Int)
    {
        self.rawValue = rawValue
    }

    /// Init by corresponding enum.
    private init(_ enumValue: eRxEvalNodeSupport_Option)
    {
        rawValue = enumValue.rawValue
    }

    /// Hashable conformance.
    public var hashValue : Int { return rawValue + 1 }

    /// The Sequence generator to enumerate through the oRxEvalRequest elements.
    public func makeIterator() -> AnyIterator<eRxEvalNodeSupport_Option>
    {
        var currentIndex : Int = 0

        return AnyIterator
        {
            while currentIndex < 2
            {
                let rawValue = 1 << currentIndex
                let value = oRxEvalNodeSupport_Options(rawValue: rawValue)

                currentIndex += 1

                if self.contains(value)
                {
                    return eRxEvalNodeSupport_Option(rawValue: rawValue)
                }
            }

            return nil
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eEvalQueueType: The types of RxEvalQueues.
///     Details the purpose of the queue and whether it is serial or concurrent.
///
/// - eEvalQueue_Serial: A General serial queue.
/// - eEvalQueue_Concurrent: A general concurrent queue.
/// - eEvalQueue_Eval: A queue assigned to an subscription for evaluation of the subscription.
/// - eEvalQueue_UIThread: The RxEvalQueue associated with the Main UI dispatch queue.
/// - eEvalQueue_Timer: The queue used for timer action calls.
/// - eEvalQueue_Disposal: The queue used for RxDisposal disposal.
/// - eEvalQueue_DisposalQueuer: The queue for queuing disposal requests (as opposed to actual disposal)
/// - eEvalQueue_Barrier: The queue for barrier blocking.
/// - eEvalQueue_Queuer: The queue used for ordering and queueing notifications (when queueing not supported by an eEvalQueue_Eval).
///

public enum eRxEvalQueueType: Int, CustomStringConvertible
{
   /// Serial queue.
   case eSerial

   /// Concurrent queue.
   case eConcurrent

   /// A eval queue to be assigned to an expression evaluation.
   case eEval

   /// The main UI thread queue.
   case eUIThread

   /// The eval queue used for timer triggering.
   case eTimer

   /// The eval queue used for subscription disposal.
   case eDisposal

   /// The eval queue used for disposal synchronisation.
   case eDisposalQueuer

   /// The eval queued used for blocking barriers.
   case eBarrier

   /// The eval queue used for general synchronisation.
   case eQueuer

   /// The eval queue used by the RxEvalQueueManager for synchronisation.
   case eEvalQueueManagerQueuer

   /// The eval queue used for trace-monitoring.
   case eMonitor

   /// The list of descriptions of the eval queue types.
    private static let descriptions = [

           "evalQueue Serial",
           "evalQueue Concurrent",
           "evalQueue Eval",
           "evalQueue UIThread",
           "evalQueue Timer",
           "evalQueue Disposal",
           "evalQueue DisposalQueuer",
           "evalQueue Barrier",
           "evalQueue Queuer",
           "evalQueue EvalQueue Manager",
           "evalQueue Monitoring"
   ]

   /// The list of dispatch queue attributes.
   private static let attributes : [DispatchQueue.Attributes] = [

           DispatchQueue.Attributes(),
           DispatchQueue.Attributes.concurrent,
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes.concurrent,
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes(),
           DispatchQueue.Attributes()
   ]

   /// The list of dispatch queue attributes.
   private static let qoses : [DispatchQoS] = [

           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default,
           DispatchQoS.default
   ]

   /// Getter for the dispatch queue attributes.
   public var attribute: DispatchQueue.Attributes
   {
       return eRxEvalQueueType.attributes[self.rawValue]
   }

   /// Getter for the dispatch queue qos.
   public var qos: DispatchQoS
   {
       return eRxEvalQueueType.qoses[self.rawValue]
   }

   /// The description of the type of dispatch queue.
   public var attributeDescription: String
   {
       switch self
       {
           case .eSerial, .eEval, .eDisposal, .eDisposalQueuer, .eQueuer, .eEvalQueueManagerQueuer, .eMonitor:
               return "Serial"

           case .eConcurrent, .eBarrier, .eTimer:
               return "Concurrent"

           case eRxEvalQueueType.eUIThread:
               return "UIThread"
       }
   }

   /// Indicates whether the queue is serial (or concurrent).
   public var isSerial : Bool
   {
       return eRxEvalQueueType.attributes[self.rawValue].contains(DispatchQueue.Attributes.concurrent)
   }

   /// CustomStringConvertible compliance
   public var description : String
   {
       return eRxEvalQueueType.descriptions[self.rawValue]
   }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxEvalQueueInfo: An entity class for identifying the characteristics of the currently running RxEvalQueue.
///
/// See the RxThreadTrackableObject<RxEvalQueueInfo>.getRunningInfo() method.
///

open class RxEvalQueueInfo: CustomStringConvertible, Equatable
{
   /// The dispatch queue queueID key.
   open static let DispatcKey = DispatchSpecificKey<RxEvalQueueInfo?>()

    let tag : String
    let instanceID  : RxInstanceID
    let evalQueueType : eRxEvalQueueType
    let evalQueuePriority : DispatchQoS

    /// Initialise all properties.
    public init(tag: String, instanceID : RxInstanceID, evalQueueType : eRxEvalQueueType, evalQueuePriority : DispatchQoS)
    {
        self.tag = tag
        self.instanceID = instanceID
        self.evalQueueType = evalQueueType
        self.evalQueuePriority = evalQueuePriority
    }

    /// CustomStringConvertible conformance.
    open var description : String
    {
        return "tag(\(tag)) instanceID(\(tag)) evalQueueType(\(evalQueueType)) evalQueuePriority(\(evalQueuePriority))"
    }

   /// Get the RxEvalQueueInfo for the running thread.
   /// Returns: The RxEvalQueueInfo set in the running thread, nil if not set.
   open static func getThreadInfo() -> RxEvalQueueInfo?
   {
       return DispatchQueue.getSpecific(key: RxEvalQueueInfo.DispatcKey) ?? nil
   }

   /// Set the RxEvalQueueInfo in the running thread.
   open func setThread(_ dispatchQueue: DispatchQueue)
   {
       dispatchQueue.setSpecific(key: RxEvalQueueInfo.DispatcKey, value: self)
   }

   /// Clear the RxEvalQueueInfo in the running thread.
   open func clearThread(_ dispatchQueue: DispatchQueue)
   {
       dispatchQueue.setSpecific(key: RxEvalQueueInfo.DispatcKey, value: nil)
   }
}

/// Equatable conformance.
public func ==(lhs: RxEvalQueueInfo, rhs: RxEvalQueueInfo) -> Bool
{
   return (lhs.instanceID == rhs.instanceID) && (lhs.tag == rhs.tag) && (lhs.evalQueueType == rhs.evalQueueType) && (lhs.evalQueuePriority == rhs.evalQueuePriority)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxAsyncGenCommand: AsyncGenPat generator commands.
///
///      - eNextTick:           Progress to the next tick synchronously.
///      - eNextTickAt:         Progress to the next tick at the associated time-offset.
///      - eStopTicking:        Stop the generation of ticks.
///      - eTerminateSource:    Terminate the associated subscriptions, For Hot subscription all subscriptions, for a cold only the current subscription.
///

public enum eRxAsyncGenCommand
{
   case eNextTick
   case eNextTickAt(RxTimeOffset)
   case eStopTicking
   case eTerminateSource
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxSyncGenCommand: SyncGenPat generator commands.
///
///      - eNextTick:           Progress to the next tick event-cycle immediately.
///      - eYieldTick:          Yield eval queue operation for a cycle to allow other activity to run on the queue.
///      - eStopTicking:        Stop the generation of ticks.
///      - eTerminate:          Terminate the associated subscriptions, For Hot subscription all subscriptions, for a cold only the current subscription.
///

public enum eRxSyncGenCommand
{
   /// Progress to the next tick event-cycle immediately.
   case eNextTick

   /// Stop emitting tick events.
   case eStopTicking

   /// Stop ticking and terminate the expression.
   case eTerminate
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSubscriptionStats: Statistics collected during Subscription.
///

open class RxSubscriptionStats
{
   /// The Subscription start time.
   open let subscriptionTime: RxTime

   /// The Start of Eval time
   open var evalStartTime : RxTime? = nil

   /// The Subscription dispose start time.
   open var disposeStartTime : RxTime? = nil

   /// The Subscription dispose end time.
   open var disposeEndTime: RxTime? = nil

   /// The duration of building.
   open var buildDuration : RxDuration { return evalStartTime != nil ? evalStartTime!.timeIntervalSince(subscriptionTime) : 0 }

   /// The duration of evaluation.
   open var evalDuration : RxDuration { return disposeStartTime != nil ? disposeStartTime!.timeIntervalSince(subscriptionTime) : 0 }

   /// The duration of the dispose cycle.
   open var disposeDuration : RxDuration { return (disposeStartTime != nil) && (disposeEndTime != nil) ? disposeEndTime!.timeIntervalSince(disposeStartTime!) : 0 }

   /// The total duration of evaluation and disposal.
   open var totalDuration : RxDuration { return disposeEndTime != nil ? disposeEndTime!.timeIntervalSince(subscriptionTime) : 0 }

   /// Initialise with the subscription start time.
   /// - Parameter subscriptionTime: The time of subscription.
   public init(subscriptionTime : RxTime = RxTime())
   {
       self.subscriptionTime = subscriptionTime
   }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
///  RxEvalOpsConvenience: Convenience RxPattern behaviours expressed as RxEvalOps.
///

open class RxEvalOpsConvenience<ItemInType, ItemOutType>
{
   /// The completedOpPat Pattern: Emit a completed/error
   open class func completedOpPat(_ error : IRxError? = nil) -> ((RxEvalNode<ItemInType, ItemOutType>) -> Void)
   {
       return { (evalNode : RxEvalNode<ItemInType, ItemOutType>) in

           evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

               switch stateChange
               {
                   case .eEvalBegin:

                       notifier.notifyCompleted(error: error)

                   default:
                       // do nothing:
                       break
               }
           }
       }
   }

   /// The completedOpPat Pattern: Emit a completed/error
   open class func terminationOpPat(_ termination: eTerminationType? = nil) -> ((RxEvalNode<ItemInType, ItemOutType>) -> Void)
   {
       if termination == nil
       {
           return noOpPat()
       }

       return { (evalNode : RxEvalNode<ItemInType, ItemOutType>) in

           evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

               switch stateChange
               {
                   case .eEvalBegin:

                       notifier.notify(termination: termination!)

                   default:
                       // do nothing:
                       break
               }
           }
       }
   }

   /// The noOpPat Pattern: Do nothing.
   open class func noOpPat<ItemInType, ItemOutType>() -> ((RxEvalNode<ItemInType, ItemOutType>) -> Void)
   {
       return { (evalNode: RxEvalNode<ItemInType, ItemOutType>) in }
   }
}
