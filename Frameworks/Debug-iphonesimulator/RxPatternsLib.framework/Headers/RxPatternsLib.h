//
//  RxPatternsLib.h
//  RxPatternsLib
//
//  Created by Terry Stillone on 23/01/2016.
//  Copyright © 2016 Originware. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RxPatternsLib.
FOUNDATION_EXPORT double RxPatternsLibVersionNumber;

//! Project version string for RxPatternsLib.
FOUNDATION_EXPORT const unsigned char RxPatternsLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RxPatternsLib/PublicHeader.h>


