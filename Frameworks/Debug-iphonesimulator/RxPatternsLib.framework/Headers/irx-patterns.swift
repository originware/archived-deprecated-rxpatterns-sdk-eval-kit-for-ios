// irx-patterns.swift
// RxPatternsLib
//
// Created by Terry Stillone (http://www.originware.com) on 24/03/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

public protocol RxObservablePatterns
{
    associatedtype ItemOutType

    //
    //  Single Stream Observable Patterns
    //

    /// Employ a given evaluation function to process in-coming notifications and emit the resulting notifications.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter evalOp: The RxEvalOp which defines the behaviour of the Observable.
    func relayPat<ItemTargetType>(tag: String, evalOp: @escaping RxTypes2<ItemOutType, ItemTargetType>.RxEvalOp) -> RxObservableMap<ItemOutType, ItemTargetType>

    /// Extract notifications to a given consumer for the purposes of external processing.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter extractConsumer: The given consumer to send notifications to.
    /// - Returns: The RxObservable that performs the extraction.
    func extractPat(tag: String, extractConsumer: ARxConsumer<ItemOutType>)                                      -> RxObservable<ItemOutType>

    //
    //  Multi Stream Gating Observable Patterns
    //

    /// Collect the items from separate streams together as a tuple and apply a mapping function on the tuples to generate a target notification.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter gateType: The type of gate operation to be performed.
    /// - Parameter streams: The streams to gate.
    /// - Parameter mapEvalOp: The map of gate output items to the TargetItemType.
    /// - Returns: The RxObservableMap that performs the gating.
    func gateStreamsMapPat<TargetItemType>(tag: String, gateType : eGateType, streams : [ARxProducer<ItemOutType>], mapEvalOp: @escaping (RxGateMapType<ItemOutType, TargetItemType>) -> Void) -> RxObservableMap<ItemOutType, TargetItemType>
}

public protocol RxSourcePatterns
{
    associatedtype ItemType

    //
    //  Source Generator Patterns
    //

    /// Create a Source that generates notifications from an async time based generator function.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter timesAreStrict: Indicator that the generated times should be strict or subject to timer coalescing.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter generator: Generator of item times and values to emit.
    /// - Returns: The RxSource that generates the async notifications.
    static func asyncGenPat(tag: String, timesAreStrict: Bool, subscriptionType: eRxSubscriptionType, generator: @escaping (_ index:RxIndexType, _ notifier:ARxNotifier<ItemType>) -> eRxAsyncGenCommand) -> RxSource<ItemType>

    ///  Create a Source that generates notifications from an sync non-time based generator function.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter generator: Generator of item values to emit.
    /// - Returns: The RxSource that generates the sync notifications.
    static func syncGenPat(tag: String, subscriptionType: eRxSubscriptionType, generator: @escaping (_ index : RxIndexType, _ notifier : ARxNotifier<ItemType>) -> eRxSyncGenCommand) -> RxSource<ItemType>

    /// Gate items from separate streams. Depending on the gate type, allow them through and apply a mapping function.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter gateType: The type of gate operation to be performed.
    /// - Parameter streams: The streams to gate.
    /// - Parameter mapEvalOp: The map of gate output items to the TargetItemType.
    /// - Returns: The RxSource that performs the gating.
    static func gateStreamsPat<TargetItemType>(tag: String, subscriptionType: eRxSubscriptionType, gateType : eGateType, streams : [ARxProducer<ItemType>], mapEvalOp: @escaping (RxGateMapType<ItemType, TargetItemType>) -> Void) -> RxSource<TargetItemType>
}

public protocol RxTickableSourcePatterns
{
    associatedtype ItemType

    ///  Create a Source that generates notifications which are directed by an external notifier.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter notifier: The notifier to send the notifications to.
    /// - Returns: The source that performs the redirection.
    static func redirectPat(tag: String, subscriptionType: eRxSubscriptionType, notifier : RxNotifier_NotifyConsumers<ItemType>) -> RxSource<ItemType>

    ///  Create a Source that generates notifications which are triggered by an external tick generator, which is then fed into a notification
    ///      generator to obtain the item to be emitted at that tick time.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter tickNotifier: The notifier that determines when to emit items.
    /// - Parameter generator: The generator that generates the item values to be emitted.
    /// - Returns: The RxSource that performs the tick emission.
    static func tickGenPat<TickType>(tag: String, subscriptionType: eRxSubscriptionType, tickNotifier : RxNotifier_NotifyConsumers<TickType>, generator: @escaping (_ tick : TickType, _ notifier : ARxNotifier<ItemType>) -> Bool) -> RxObservableMap<TickType, ItemType>
}
