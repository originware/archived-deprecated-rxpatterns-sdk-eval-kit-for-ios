//
// Created by Terry Stillone (http://www.originware.com) on 29/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIKeyword_Matcher: Match given text for POI keyword matches and sub-string matches. Notify matches.
///

class POIKeyword_Matcher : RxSource<AppEvent>
{
    fileprivate let m_minSubstringLength: Int
    fileprivate var m_POIKeywordMatcherByPOIKeyword = [String : SubstringMatcher]()
    fileprivate var m_notifier : ARxNotifier<AppEvent>? = nil

    init(tag: String, minSubstringLength : Int)
    {
        m_minSubstringLength = minSubstringLength

        super.init(tag : tag, subscriptionType: .eHot)

        // Load keywords to be matched.
        for poiKeyword in AppSettings.allPOIkeywords!
        {
            m_POIKeywordMatcherByPOIKeyword[poiKeyword] = SubstringMatcher(string: poiKeyword, minSubstringLength : minSubstringLength)
        }
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode]  (stateChange: eRxEvalStateChange, notifier: ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.m_notifier = evalNode.getSharedOutNotifier(forSync: false)

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_notifier = nil

                    default:
                        break
                }
            }
        }
    }

    /// Match the given keywords
    /// - Parameter stringToMatch: The text to match for POI keywords/
    func beginMatchingForPOIKeywords(_ stringToMatch: String)
    {
        let wordList = stringToMatch.characters.split(separator: " ").map{ String($0).lowercased() }
        let matches = POIKeywordEvent_Match()

        for keyWord in wordList
        {
            if (keyWord == "clear") || (keyWord == "remove")
            {
                let keyWordMatch = POIKeywordMatch(poiKeyword: keyWord)

                 matches.addMatchesForPOIKeyword(keyWordMatch)

                 continue
            }

            let substringMatcher : SubstringMatcher = SubstringMatcher(string: keyWord, minSubstringLength : m_minSubstringLength)
            let stringToMatchLength = keyWord.characters.distance(from: keyWord.startIndex, to: keyWord.endIndex)
            let matchThresholdLength = stringToMatchLength <= 2 ? 2 : stringToMatchLength - 1

            for (_, matcher) in m_POIKeywordMatcherByPOIKeyword
            {
                let poiKeywordMatches = matcher.getCommonSubstrings(substringMatcher)

                if (poiKeywordMatches.totalMatchLength >= matchThresholdLength) && (poiKeywordMatches.maxMatchLength >= 3)
                {
                    matches.addMatchesForPOIKeyword(poiKeywordMatches)
                }
            }
        }

        if matches.count > 0
        {
            // Matches found, notify.
            m_notifier?.notify(item: AppEvent(appEventType: .ePOIKeywordMatchSet(matches)))
        }
    }
}
