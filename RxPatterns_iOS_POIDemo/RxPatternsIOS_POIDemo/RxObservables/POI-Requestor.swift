//
// Created by Terry Stillone (http://www.originware.com) on 5/07/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

extension RxObservable
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// POIRequestor: Receives POI keywords requests and emits
    ///

    func POIRequestor(_ locationObservable: DeviceAdapter_Location, reachabilityObservable: DeviceAdapter_Reachability, timeout: RxDuration) -> RxObservable<ItemType>
    {
        let evalOp = {[unowned self, unowned reachabilityObservable, unowned locationObservable] (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            var locationSubscription : RxSubscription? = nil
            var reachabilitySubscription : RxSubscription? = nil

            var haveLocation = false
            var haveReachability = false
            var currentLocation: CLLocation? = nil

            var timer : RxTimer? = nil
            
            evalNode.itemDelegate = { (item: ItemType, notifier : ARxNotifier<ItemType>) in

                func notifyRequest()
                {
                    // Location and Network connectivity are available, so emit a poi request.
                    if let appEvent = item as? AppEvent
                    {
                        let poikeywordCollectionMatches = appEvent.appEventType.poikeywordCollectionMatches
                        let poiLocateRequest = MultiPOILocateEvent_Request(poiKeywords: Set<String>(poikeywordCollectionMatches.poiKeywords), location : currentLocation!)
                        
                        notifier.notify(item: AppEvent(appEventType: .ePOILocateRequest(poiLocateRequest)) as! ItemType)
                    }
                    else
                    {
                        assert(false, "Unexpected request to: POIRequestor: \(item)")
                    }
                }

                // If we have a location fix and network link.
                if haveLocation && haveReachability
                {
                    // Cancel the timeout timer.
                    if let locationTimer = timer
                    {
                        locationTimer.cancelAll()
                        timer = nil
                    }

                    // Location and Network connectivity are available, so emit a poi request.
                    notifyRequest()
                }
                else
                {
                    // Either the location or network is not currently available.
                    // Wait for timeout secs to allow location and reachability to be made available.

                    if timer == nil { timer = RxTimer(tag: self.tag + "/timer") }

                    let _ = timer!.addTimer(timeout, timerAction: { (index: RxIndexType) in

                        let confirmMessage : String? = {

                            switch (haveLocation, haveReachability)
                            {
                                case (false, false):
                                    return "Network and Location not available"

                                case (false, true):
                                    return "Location not available"

                                case (true, false):
                                    return "Network is not reachable"

                                case (true, true):
                                    // Late reply from location or reachability.
                                    notifyRequest()
                                    return nil
                            }
                        }()

                        if let confirmMessage = confirmMessage
                        {
                            notifier.notify(item: AppEvent(appEventType: .ePresentConfirmation(confirmMessage)) as! ItemType)
                        }
                    })
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                        if locationSubscription == nil
                        {
                            // Subscribe for location updates.
                            // Note: this is performed in the subscription thread as Core location wants to
                            //   attach to the RunLoop of the Main UI Thread.
                            locationSubscription = locationObservable.subscribe(itemAction: { (appEvent: AppEvent) in

                                switch appEvent.appEventType.locationChange
                                {
                                    case .eLocation_LastKnown(let lastLocation):

                                        currentLocation = lastLocation
                                        haveLocation = true

                                    default:

                                        haveLocation = false
                                }
                            })
                        }

                    case eRxEvalStateChange.eEvalBegin:

                        // Subscribe for reachability updates.
                        reachabilitySubscription = reachabilityObservable.subscribe(itemAction: { (appEvent : AppEvent) in

                            switch appEvent.appEventType.reachabilityChange
                            {
                                case .eReachability_IsReachable:

                                    haveReachability = true

                                case .eReachability_NotReachable:

                                    haveReachability = false

                                default:
                                    // do nothing.
                                    break
                            }
                        })

                    case eRxEvalStateChange.eEvalEnd:

                        // Destroy all resources and services being used.
                        timer?.cancelAll()
                        locationSubscription?.unsubscribe()
                        reachabilitySubscription?.unsubscribe()

                        locationSubscription = nil
                        reachabilitySubscription = nil

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemType>(tag: "POIRequestor", evalOp: evalOp).chainObservable(self)
    }
}
