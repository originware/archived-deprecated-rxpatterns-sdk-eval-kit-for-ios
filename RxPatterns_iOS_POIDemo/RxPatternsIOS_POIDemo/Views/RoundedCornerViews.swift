//
// Created by Terry Stillone (http://www.originware.com) on 26/07/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class RoundedCornersView : UIView
{
    static let CornerRadius = CGFloat(10.0)

    fileprivate let m_cornerMask : UIRectCorner

    required init?(coder: NSCoder)
    {
        m_cornerMask = UIRectCorner.allCorners

        super.init(coder: coder)
    }

    init?(coder: NSCoder, cornerMask : UIRectCorner)
    {
        m_cornerMask = cornerMask

        super.init(coder: coder)
    }

    override func layoutSubviews()
    {
        let maskLayer = CAShapeLayer()
        let cornerRadii = CGSize(width: RoundedCornersView.CornerRadius, height: RoundedCornersView.CornerRadius)
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: m_cornerMask, cornerRadii: cornerRadii)
        
        maskLayer.path = maskPath.cgPath
        
        layer.mask = maskLayer;
    }
}

class TopRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask : [UIRectCorner.topLeft, UIRectCorner.topRight])
    }

    override func draw(_ rect: CGRect)
    {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        context.saveGState()

        let colorspace = CGColorSpaceCreateDeviceRGB()
        let grStop0 = UIColor(red: 0.7, green: 0.7, blue: 0.3, alpha: 1)
        let grStop1 = UIColor(red: 0.59, green: 0.56, blue: 0.24, alpha: 1)

        let grStops: CFArray = [grStop0.cgColor, grStop1.cgColor] as CFArray
        let grLocations: [CGFloat] = [0, 0.7]

        let gradient = CGGradient(colorsSpace: colorspace, colors: grStops, locations: grLocations)

        context.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: self.bounds.height), options: [])

        context.restoreGState()
    }
}

class BottomRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask : [UIRectCorner.bottomLeft, UIRectCorner.bottomRight])
    }
}

class AllRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask: UIRectCorner.allCorners)
    }
}
