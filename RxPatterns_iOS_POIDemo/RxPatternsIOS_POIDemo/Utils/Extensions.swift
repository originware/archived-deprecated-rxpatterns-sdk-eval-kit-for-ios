//
// Created by Terry Stillone on 9/09/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation

extension String
{
    public func rangeToNSRange(_ range : Range<String.Index>) -> NSRange
    {
        let length = characters.distance(from: range.lowerBound, to: range.upperBound)
        let start = characters.distance(from: startIndex, to: range.lowerBound)
        let nsRange = NSMakeRange(start, length)

        return nsRange
    }

    public func map<T>(_ mapFunc: (String) -> T) -> T
    {
        return mapFunc(self)
    }
}

extension URL
{
    public func map<T>(_ mapFunc: (URL) -> T) -> T
    {
        return mapFunc(self)
    }
}
