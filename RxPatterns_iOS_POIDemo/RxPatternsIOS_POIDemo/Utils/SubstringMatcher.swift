//
// Created by Terry Stillone (http://www.originware.com) on 20/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// SubstringMatcher: A Utility class that performs substring matching by hashing sub-strings.
///

open class SubstringMatcher
{
    fileprivate var m_string : String
    fileprivate let m_minSubstringLength : Int
    fileprivate var m_substringHashToMatchRange = [Int : Range<String.Index>]()
    fileprivate var m_hashInDescendingSubstringLength = [Int]()

    public init(string : String, minSubstringLength : Int)
    {
        self.m_string = string
        self.m_minSubstringLength = minSubstringLength

        generateSubstringHashes()
    }

    /// Return the common sub-strings between self and other,
    /// This is done by comparing the hashes of the substrings of self and other,
    open func getCommonSubstrings(_ other : SubstringMatcher) -> POIKeywordMatch
    {
        let matches = POIKeywordMatch(poiKeyword: m_string)

        for hash in m_hashInDescendingSubstringLength where other.m_substringHashToMatchRange[hash] != nil
        {
            guard let selfSubstring = getSubstring(forHash: hash) else { continue }
            guard let otherSubstring = other.getSubstring(forHash: hash) else { continue }
                               
            if selfSubstring == otherSubstring
            {
                matches.addMatchForPOIKeyword(m_substringHashToMatchRange[hash]!)
            }
        }

        return matches
    }

    /// Generate the hashes for all the substrings of the base string: m_string
    fileprivate func generateSubstringHashes()
    {
        func addSubstring(_ range: Range<String.Index>)
        {
            let substring = m_string.substring(with: range)
            let hash = substring.hash

            if m_substringHashToMatchRange[hash] == nil
            {
                m_substringHashToMatchRange[hash] = range
                m_hashInDescendingSubstringLength.append(hash)
            }
            else
            {
                // No conflict occurs for the keyword set used by this app.
                print(">> hash conflict.")
            }
        }

        let stringLength = m_string.distance(from: m_string.startIndex, to: m_string.endIndex)
        let maxSubstringLength = stringLength
        let minSubstringLength = m_minSubstringLength

        // For each major substring range from large to small.
        for substringLength in stride(from: maxSubstringLength, through: minSubstringLength, by: -1)
        {
            // For each minor range in the major substring.
            for offset in 0...(stringLength - substringLength)
            {
                let startIndex = m_string.index(m_string.startIndex, offsetBy: offset)
                let range = Range<String.Index>(startIndex..<m_string.index(startIndex, offsetBy: substringLength))

                addSubstring(range)
            }
        }
    }

    /// Get the substring for a given hash.
    fileprivate func getSubstring(forHash hash: Int) -> String?
    {
        guard let range = m_substringHashToMatchRange[hash] else { return nil }

        return m_string.substring(with: range)
    }
}

