//
// Created by Terry Stillone on 9/09/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ValueClamper: A Utility to clamp CGFloat values.
///

struct ValueClamper
{
    fileprivate static let LowThreadhold : CGFloat = 0.3
    fileprivate static let HighThreashold : CGFloat = 0.8

    static func clampToZeroOne(_ value : CGFloat ) -> CGFloat
    {
        switch value
        {
            case _ where value < 0.0:
                return 0.0

            case _ where value > 1.0:
                return 1.0

            default:
                return value
        }
    }

    static func clampToNegPos1(_ value : CGFloat ) -> CGFloat
    {
        switch value
        {
            case _ where value < -1.0:
                return -1.0

            case _ where value > 1.0:
                return 1.0

            default:
                return value
        }
    }

    static func clampToThreshold(_ value : CGFloat) -> CGFloat
    {
        switch value
        {
            case _ where value < LowThreadhold:
                return 0.0

            case _ where value > HighThreashold:
                return 1.0

            default:
                return value
        }
    }
}