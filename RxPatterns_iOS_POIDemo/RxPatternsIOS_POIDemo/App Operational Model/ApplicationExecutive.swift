//
// Created by Terry Stillone on 14/10/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import MapKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ApplicationExecutive: Manages the Application operational scope behaviour.
///     Creates the application scope subscriptions.
///     Subscriptions are specifically tagged for later unsubscriptions.
///
struct ApplicationExecutive
{
    struct Constant
    {
        static let KeyboardDebounceInSeconds                     = 0.4
        static let LocationAndReachabilityFixTimeout: RxDuration = 9
    }

    /// Execute the application scope.
    /// - Parameter appRxDir: The directory of Rx components to be used in execution.
    /// - Parameter appScopeTag: The subscription tag to be applied. To be used for later unsubscription.
    func executeApplication(_ appRxDir : AppRxDir, appScopeTag : String)
    {
        // Clear the text fields.
        if let searchTextField = appRxDir.input.poiSearchText,
           let keywordMatchResult = appRxDir.view.keywordMatchResult
        {
            searchTextField.clear()
            keywordMatchResult.reset()
        }

        let runContext = AppRunContext()

        // Monitoring subscriptions.
        applyAppMonitoringSubscriptions(appRxDir, appScopeTag)

        // Service scope source subscriptions.
        applyServiceSubscriptions(appRxDir, appScopeTag)

        // Presentation scope source subscriptions.
        applyButtonTapSubscriptions(appRxDir, appScopeTag)
        applyTextInputSubscriptions(appRxDir, appScopeTag)

        // Application scope source subscriptions.
        applyPOIKeywordsMatchSubscriptions(runContext, appRxDir, appScopeTag)
        applyPOILocationSubscriptions(runContext, appRxDir, appScopeTag)
    }

    // Service scope source subscriptions.
    func applyServiceSubscriptions(_ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard   let confirm = appRxDir.view.confirm,
                let map = appRxDir.view.map,
                let logUITextView = appRxDir.view.logUITextView,
                let locationLabel = appRxDir.view.locationLabel,
                let reachabilityLabel = appRxDir.view.reachabilityLabel
        else { return }

        appRxDir.service.orientation.subscribe(logUITextView).addToDir(subscriptionTag)
        appRxDir.service.reachability.subscribe(logUITextView).addToDir(subscriptionTag)
        appRxDir.service.reachability.subscribe(reachabilityLabel).addToDir(subscriptionTag)
        appRxDir.service.location.subscribe(logUITextView).addToDir(subscriptionTag)
        appRxDir.service.location.subscribe(locationLabel).addToDir(subscriptionTag)

        appRxDir.service.location.subscribe(itemAction: { (appEvent: AppEvent) in

                    if case .eLocationChange(let locationChange) = appEvent.appEventType,
                       case .eLocation_LastKnown(let location) = locationChange, appEvent.isSimulatedEvent
                    {
                        confirm.notify(item: AppEvent(appEventType: .ePresentConfirmation("As LocationServices are not available,\nthe location will be simulated as\nCambridge, UK")))

                        RxSDK.evalQueue.UIThreadQueue.dispatch(async: {
                            map.setLocation(location: location)
                        })
                    }

                }).addToDir(subscriptionTag)
    }

    func applyButtonTapSubscriptions(_ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard let buttonTap = appRxDir.input.buttonTap,
              let searchTextField = appRxDir.input.poiSearchText,
              let poiPortalService = appRxDir.poiPortalComms.poiPortalService,
              let logUITextView = appRxDir.view.logUITextView,
              let keywordMatchResult = appRxDir.view.keywordMatchResult,
              let matchedKeywordsUICollection = appRxDir.view.matchedKeywordsUICollection,
              let map = appRxDir.view.map
        else { return }

        buttonTap.subscribe(itemAction: { (appEvent : AppEvent) in

            RxSDK.evalQueue.UIThreadQueue.dispatch(sync: {

                let (_, touchTarget) = appEvent.appEventType.touchControl

                switch touchTarget
                {
                    case .eTouch_clearSearchButton:
                        searchTextField.clear()

                    case .eTouch_clearPOIKeywordMatchesButton:
                        searchTextField.clear()
                        poiPortalService.clearLocatedPOIKeywords()
                        matchedKeywordsUICollection.clearPOIMatches()
                        keywordMatchResult.reset()
                        map.notify(item: appEvent)

                    case .eTouch_centerMapButton:
                        map.notify(item: appEvent)

                    case .eTouch_clearLogButton:
                        logUITextView.notify(item: appEvent)
                }
            })

        }).addToDir(subscriptionTag)
    }

    // Presentation scope source subscriptions
    func applyTextInputSubscriptions(_ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard   let searchTextField = appRxDir.input.poiSearchText,
                let keywordMatchResult = appRxDir.view.keywordMatchResult,
                let logUITextView = appRxDir.view.logUITextView,
                let keywordMatcher = appRxDir.keywordMatching.keywordMatcher
        else { return }

        searchTextField.subscribe(itemAction: { (appEvent : AppEvent) in

            RxSDK.evalQueue.UIThreadQueue.dispatch(async: {

                let textFieldChange = appEvent.appEventType.textFieldChange
                let attributedText = textFieldChange.attributedText

                if appEvent.isSimulatedEvent
                {
                    // Set the UITextField with the text to be replayed.
                    searchTextField.textField.attributedText = attributedText
                }
                else if attributedText.string.characters.count > 2
                {
                    // Commence keyword matching.
                    keywordMatcher.beginMatchingForPOIKeywords(attributedText.string)
                }
                else
                {
                    // Present the standard POI Keywords placeholder in the search results UITextView.
                    keywordMatchResult.reset()
                }

                appRxDir.monitoring.appOpEventMonitor.notify(item: .eFromKeyboard_To_POIKeywordMatcherPath)
            })

        }).addToDir(subscriptionTag)

        // Notify the log of text entered, in chunks of debounced values.
        searchTextField.debounce(Constant.KeyboardDebounceInSeconds).subscribe(logUITextView).addToDir(subscriptionTag)
    }

    // Application scope source subscriptions.
    func applyPOILocationSubscriptions(_ runContext : IRxDevContext, _ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard   let map = appRxDir.view.map,
                let logUITextView = appRxDir.view.logUITextView,
                let matchedKeywordsUICollection = appRxDir.view.matchedKeywordsUICollection,
                let poiPortalService = appRxDir.poiPortalComms.poiPortalService
        else { return }

        // POI Locator results to Map
        poiPortalService.input.issueRequest(runContext, POIPortalService.RequestScope.eRequest.eBeginScope({ (reply) in

            switch reply
            {
                case .eRequestSuccess(let appEvent):

                    // The request was successfull, so update the map, matched keywords and log.
                    // POI Locator results to Map
                    map.notify(item: appEvent)
                    logUITextView.notify(item: appEvent)
                    matchedKeywordsUICollection.notify(item: appEvent)

                case .eRequestFailure(let appEvent):

                    // Google Places replied, but failed to get an adequate response, so log it.
                    logUITextView.notify(item: appEvent)

                case .eError(let error):

                    // A System error occurred in comms with Google Places, on log the event.
                    let appEvent = AppEvent(appEventType: eAppEventType.eSystemFailure(error))

                    // POI Locator results to Log
                    logUITextView.notify(item: appEvent)

                case .eDidEndScope:
                    break
            }
        }))
    }

    // Application monitoring source subscriptions.
    func applyAppMonitoringSubscriptions(_ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard   let logUITextView = appRxDir.view.logUITextView,
                let map = appRxDir.view.map,
                let keywordMatchResult = appRxDir.view.keywordMatchResult,
                let poiPortalService = appRxDir.poiPortalComms.poiPortalService,
                let googlePlacesService = appRxDir.poiPortalComms.googlePlacesService,
                let httpService = appRxDir.poiPortalComms.httpService
        else { return }

        // The monitor observer.
        let appOpEventMonitor = appRxDir.monitoring.appOpEventMonitor
        
        // An intermediate monitor notifier.
        let notifier = RxNotifier_NotifyDelegates<eAppOpEventType>(tag: "/AppMonitor/notifier")

        // Append individual event sources.
        poiPortalService.monitorNotifier.appendConsumer(consumer: notifier)
        googlePlacesService.monitorNotifier.appendConsumer(consumer: notifier)
        httpService.monitorNotifier.appendConsumer(consumer: notifier)

        map.monitorNotifier.appendConsumer(consumer: notifier)
        logUITextView.monitorNotifier.appendConsumer(consumer: notifier)
        keywordMatchResult.monitorNotifier.appendConsumer(consumer: notifier)
        keywordMatchResult.monitorNotifier.appendConsumer(consumer: notifier)

        notifier.onItem = { (appEvent) in
            
            appOpEventMonitor.notify(item: appEvent)
        }
        
        // Subscribe reachbility source.
        appRxDir.service.reachability.subscribe(itemAction: { (_) in

            appOpEventMonitor.notify(item: .eFromLocation_To_POIRequestor)

        }).addToDir(subscriptionTag)

        // Subscribe location source.
        appRxDir.service.location.subscribe(itemAction: {  (_) in

            appOpEventMonitor.notify(item: .eFromLocation_To_POIRequestor)

        }).addToDir(subscriptionTag)
    }

    // perform POI keyword subscriptions at viewWillAppear time.
    func applyPOIKeywordsMatchSubscriptions(_ runContext : IRxDevContext, _ appRxDir : AppRxDir, _ subscriptionTag: String)
    {
        guard   let keywordMatcher = appRxDir.keywordMatching.keywordMatcher,
                let poiPortalService = appRxDir.poiPortalComms.poiPortalService,
                let googlePlacesService = appRxDir.poiPortalComms.googlePlacesService,
                let httpService = appRxDir.poiPortalComms.httpService,
                let keywordMatchResult = appRxDir.view.keywordMatchResult,
                let matchedKeywordsUICollection = appRxDir.view.matchedKeywordsUICollection,
                let logUITextView = appRxDir.view.logUITextView,
                let confirm = appRxDir.view.confirm,
                let searchTextField = appRxDir.input.poiSearchText
        else { return }

        var haveGivenConfirmation = false

        let _ = googlePlacesService.assignOutput(runContext, httpService.input)
        let _ = poiPortalService.assignOutput(runContext, googlePlacesService.input)

        keywordMatcher.debounce(Constant.KeyboardDebounceInSeconds

                ).filter({ (appEvent : AppEvent) -> Bool in

                    // Present the matches on the keyword match UITextView
                    keywordMatchResult.notify(item: appEvent)

                    // Log the keyword match results.
                    logUITextView.notify(item: appEvent)

                    // Place in the matched keywords list view
                    matchedKeywordsUICollection.notify(item: appEvent)

                    for keyWord in appEvent.appEventType.poikeywordCollectionMatches.poiKeywords
                    {
                        if (keyWord == "clear") || (keyWord == "remove")
                        {
                            RxSDK.evalQueue.UIThreadQueue.dispatch(sync: {

                                searchTextField.clear()
                                keywordMatchResult.reset()
                                poiPortalService.clearLocatedPOIKeywords()
                                matchedKeywordsUICollection.clearPOIMatches()
                            })

                            return false
                        }
                    }

                    // Monitoring for App Operation View.
                    let appOpEventMonitor = appRxDir.monitoring.appOpEventMonitor

                    appOpEventMonitor.notify(item: .eFromPOIKeywordMatcher_To_POIRequestor)

                    return true

                }).POIRequestor(appRxDir.service.location, reachabilityObservable : appRxDir.service.reachability, timeout: Constant.LocationAndReachabilityFixTimeout

                ).subscribe(itemAction: { (appEvent : AppEvent) in

                    switch appEvent.appEventType
                    {
                        case .ePOILocateRequest(let poiRequest):

                            haveGivenConfirmation = false
                            poiPortalService.input.issueRequest(runContext, POIPortalService.RequestScope.eRequest.ePOIPortalRequest(poiRequest))

                        case .ePresentConfirmation:

                            if !haveGivenConfirmation
                            {
                                haveGivenConfirmation = true
                                confirm.notify(item: appEvent)
                            }

                        default:    fatalError("Unexpected code point")
                    }

                }).addToDir(subscriptionTag)
    }
}
