//
// Created by Terry Stillone on 14/10/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation
import MapKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxDirStore: A RxDirectory store for persisting RxObjects with a specific role.
///

protocol IRxDirStore
{
}

extension IRxDirStore
{
    /// Get the object associated with the given relative path.
    func get(_ relativePath : String) -> AnyObject?
    {
        let namespace = eRxDirectory_NameSpace.eRun.rawValue

        return RxDirectory.get(namespace + ":/" + relativePath)
    }

    /// Store the given object by the given relative path.
    func store(_ relativePath : String, _ object : RxObject)
    {
        let namespace = eRxDirectory_NameSpace.eStore.rawValue

        let _ = RxDirectory.put(namespace + ":/" + AppConstant.ConfigScenario + "/" + relativePath, object: object)
    }

    /// Remove the given object by the given relative path.
    func remove(_ relativePath : String)
    {
        let namespace = eRxDirectory_NameSpace.eStore.rawValue

        let _ = RxDirectory.remove(namespace + ":/" + AppConstant.ConfigScenario + "/" + relativePath)
    }

    func tag(_ tag: String) -> String
    {
        return AppConstant.AppTag + "/" + tag
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppRxDir: The collection of App RxDirectories.
///
open class AppRxDir
{
    struct Service: IRxDirStore
    {
        /// The Location Change Device.
        var location: DeviceAdapter_Location                        { return get(Service.locationURI) as! DeviceAdapter_Location }
        static let locationURI = "inadapters/location"

        /// The Network Reacability Change Device.
        var reachability : DeviceAdapter_Reachability               { return get(Service.reachabilityURI) as! DeviceAdapter_Reachability }
        static let reachabilityURI = "inadapters/reachability"

        /// The Orientation Change Device.
        var orientation :  DeviceAdapter_Orientation                { return get(Service.orientationURI) as! DeviceAdapter_Orientation }
        static let orientationURI = "inadapters/orientation"

        /// The keyboard Show/Hide Change Device.
        var keyboard: EventAdapter_keyboard                         { return get(Service.keyboardURI) as! EventAdapter_keyboard }
        static let keyboardURI = "inadapters/keyboard"

        func createAllRxSources()
        {
            store(Service.locationURI, DeviceAdapter_Location(tag: tag("DeviceAdapter_LocationWithSimulateFailover")))
            store(Service.reachabilityURI, DeviceAdapter_Reachability(tag: tag("DeviceAdapter_Reachability"), hostname: AppConstant.ReachabilityHostname))
            store(Service.orientationURI, DeviceAdapter_Orientation(tag: tag("EventAdapter_Orientation")))
            store(Service.keyboardURI, EventAdapter_keyboard(tag: tag("EventAdapter_keyboard")))
        }

        func removeAllSources()
        {
            remove(Service.locationURI)
            remove(Service.reachabilityURI)
            remove(Service.orientationURI)
            remove(Service.keyboardURI)
        }
    }

    struct Monitoring: IRxDirStore
    {
        /// The App Event Monitor.
        var appOpEventMonitor : RxRelaySubject<eAppOpEventType>     { return get(Monitoring.appOpEventMonitorURI) as! RxRelaySubject<eAppOpEventType> }
        static let appOpEventMonitorURI = "monitors/appopevent"

        func createAllRxSources()
        {
            store(Monitoring.appOpEventMonitorURI, RxRelaySubject<eAppOpEventType>(tag: tag("AppOpEvent Monitor")))
        }

        func removeAllSources()
        {
            remove(Monitoring.appOpEventMonitorURI)
        }
    }

    struct PresentationInput : IRxDirStore
    {
        static let poiSearchTextURI = "inadapters/uitextfields/search"
        var poiSearchText : EventAdapter_UITextField?           { return get(PresentationInput.poiSearchTextURI) as? EventAdapter_UITextField }

        static let buttonTapURI = "inadapters/uicontrol/touch"
        var buttonTap : EventAdapter_UITouch?                   { return get(PresentationInput.buttonTapURI) as? EventAdapter_UITouch }

        func addTap(withTag: String, atURI: String)
        {
            store(atURI, EventAdapter_UITouch(tag: tag(withTag)))
        }

        func add(button: UIButton, touchTarget: eDeviceEvent_Touch)
        {
            buttonTap?.registerForUITouch(button, touchTarget : touchTarget)
        }

        func add(textField: UITextField, withTag: String, atURI: String)
        {
            store(atURI, EventAdapter_UITextField(tag: tag(withTag), textField: textField))
        }

        func removeAllSources()
        {
            remove(PresentationInput.poiSearchTextURI)
            remove(PresentationInput.buttonTapURI)
        }
    }

    struct View: IRxDirStore
    {
        static let locationLabelURI =  "outadapters/labels/location"
        var locationLabel : ViewAdapter_Label<AppEvent>?            { return get(View.locationLabelURI) as? ViewAdapter_Label<AppEvent> }

        static let reachabilityLabelURI =  "outadapters/labels/reachability"
        var reachabilityLabel : ViewAdapter_Label<AppEvent>?        { return get(View.reachabilityLabelURI) as? ViewAdapter_Label<AppEvent> }

        static let logUITextViewURI = "outadapters/uitextviews/log"
        var logUITextView: ViewAdapter_Log_UITextView<AppEvent>?    { return get(View.logUITextViewURI) as? ViewAdapter_Log_UITextView<AppEvent> }

        static let mapURI = "outadapters/maps/poiMap"
        var map: ViewAdapter_MapKit<AppEvent>?                      { return get(View.mapURI) as? ViewAdapter_MapKit<AppEvent> }

        static let matchedKeywordsUICollectionURI = "outadapters/uicollections/poiResults"
        var matchedKeywordsUICollection : ViewAdapter_UICollection<AppEvent>?   { return get(View.matchedKeywordsUICollectionURI) as? ViewAdapter_UICollection<AppEvent> }

        static let confirmURI = "outadapters/confirmation"
        var confirm: ViewAdapter_Confirmation<AppEvent>?     { return get(View.confirmURI) as? ViewAdapter_Confirmation<AppEvent> }

        static let keywordMatchResultURI = "outadapters/uitextviews/keywordMatchResults"
        var keywordMatchResult : ViewAdapter_KeywordResults_UITextView<AppEvent>?  { return get(View.keywordMatchResultURI) as? ViewAdapter_KeywordResults_UITextView<AppEvent> }

        func add(label: UILabel, withTag: String, atURI: String)
        {
            store(atURI, ViewAdapter_Label<AppEvent>(tag : tag(withTag), label: label))
        }

        func addKeyword(textView: UITextView, withTag: String, atURI: String, formatter: AppStyle_LogFormatter)
        {
            store(atURI, ViewAdapter_KeywordResults_UITextView<AppEvent>(tag: tag(withTag), textView: textView, logFormatter: formatter))
        }

        func addLog(textView: UITextView, withTag: String, atURI: String, formatter: AppStyle_LogFormatter)
        {
            store(atURI, ViewAdapter_Log_UITextView<AppEvent>(tag: tag(withTag), textView: textView, logFormatter: formatter))
        }

        func add(collectionView: UICollectionView, withTag: String, atURI: String)
        {
            store(atURI, ViewAdapter_UICollection<AppEvent>(tag: tag(withTag), collectionView: collectionView))
        }

        func add(mapView: MKMapView, withTag: String, atURI: String)
        {
            store(atURI, ViewAdapter_MapKit<AppEvent>(tag: tag(withTag), mapView: mapView))
        }

        func addConfirm(withTag: String, atURI: String)
        {
            store(atURI, ViewAdapter_Confirmation<AppEvent>(tag: tag(withTag)))
        }

        private func tag(_ strTag: String) -> String
        {
            return AppConstant.AppTag + "/" + strTag
        }

        func removeAllSources()
        {
            remove(View.locationLabelURI)
            remove(View.reachabilityLabelURI)
            remove(View.logUITextViewURI)
            remove(View.mapURI)
            remove(View.matchedKeywordsUICollectionURI)
            remove(View.confirmURI)
            remove(View.keywordMatchResultURI)
        }
    }

    struct KeywordMatching: IRxDirStore
    {
        struct Constant
        {
            static let MinMatchLength = 3
        }

        static let keywordMatcherURI = "poi/keywordMatcher"
        var keywordMatcher : POIKeyword_Matcher?                             { return get(KeywordMatching.keywordMatcherURI) as? POIKeyword_Matcher }

        func createAllRxSources()
        {
            let tag : (String) -> String = { return AppConstant.AppTag + "/" + $0 }

            store(KeywordMatching.keywordMatcherURI, POIKeyword_Matcher(tag: tag("POIKeyword_Matcher"), minSubstringLength: Constant.MinMatchLength))
        }

        func removeAllSources()
        {
            remove(KeywordMatching.keywordMatcherURI)
        }
    }

    struct POIPortalComms: IRxDirStore
    {
        var poiPortalService:       POIPortalService!
        var googlePlacesService:    GooglePlacesPortalService!
        var httpService:            HTTPService!

        mutating func createAllHTTPServices(_ runContext: IRxDevContext)
        {
            poiPortalService = POIPortalService(runContext)
            googlePlacesService = GooglePlacesPortalService(runContext)
            httpService = HTTPService(runContext)
        }

        mutating func removeAllHTTPServices()
        {
            poiPortalService = nil
            googlePlacesService = nil
            httpService = nil
        }
    }

    // Service scope RxObjects.
    let service = Service()
    let monitoring = Monitoring()

    // Presentation scope RxObjects.
    let input = PresentationInput()
    let view = View()

    // Application scope RxObjects.
    let keywordMatching = KeywordMatching()
    var poiPortalComms = POIPortalComms()
}

