//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import RxPatternsSDK
import RxPatternsLib


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The App Operational Scope request type
///

protocol IOperationalModelRequest
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The App Operational Model Request channel type
///

typealias OperationalModelRequestChannel = ARxDevChannelNSObject<IOperationalModelRequest>

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// OperationalModel: Manages the App operating states: Startup, Presentation and Application.
///
/// - Note: Startup scope handles SDK startup and shutdown.
/// - Note: Presentation scope handles view and input subscriptions.
/// - Note: Application scope handles application logic subscriptions.
///

class OperationalModel: ARxDevice<OperationalModelRequestChannel>
{
    /// The scope stack stores the active Operational scopes.
    class ScopeStack: ARxDevChannelNSObjectScopeStack<IOperationalModelRequest>
    {
        /// The input channel of the scope stack.
        var input: OperationalModelRequestChannel
        {
            return self
        }
    }

    /// The Startup Scope initialises the app.
    struct StartupScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IOperationalModelRequest
        {
            case eBeginScope(OperationalScopeContext, ReplyNotify)
            case eBeginPresentationScope(AppRxDir.PresentationInput, AppRxDir.View)
            case eEndScope(AppError?)
        }

        enum eReply
        {
            case eError(AppError)
            case eDidEndScope(AppError?)
        }

        /// The handler for the StartupScope requests and replies.
        final class Channel: OperationalModelRequestHandler<eRequest, eReply>
        {
            fileprivate var m_operationalContext : OperationalScopeContext!
            fileprivate var m_replyNotify :   ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/AppOperationalScope/Startup/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for StartupScope requests.
            /// - Parameter runContext: The context for the running the request.
            /// - Parameter channelRequest: The request from app management to control the app scope.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IOperationalModelRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginScope(let (operationalContext, replyNotify)):

                        m_replyNotify = replyNotify
                        m_operationalContext = operationalContext

                        // Start the RxPatterns SDK
                        RxSDKManagement.startRxSDK()

                    case .eBeginPresentationScope:

                        pushPresentationScope(runContext, { (reply: PresentationScope.eReply) in

                            switch reply
                            {
                                case .eError(let error):

                                    self.reply(.eError(error))

                                case .eDidEndScope:

                                    self.scopeStack.popScope(runContext)
                                    self.onRequest(runContext, eRequest.eEndScope(nil))
                            }
                        })

                    case .eEndScope:

                        // Stop the RxPatterns SDK
                        RxSDKManagement.shutdownRxSDK()

                        reply(.eDidEndScope(nil))
                }
            }

            /// Reply back to app management client.
            /// - Parameter withReply: The reply to notify back to app management.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IOperationalModelRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }

            /// Push the Presentation Scope onto the scope stack.
            fileprivate func pushPresentationScope(_ runContext: IRxDevContext, _ replyNotify : @escaping (PresentationScope.eReply) -> Void)
            {
                let presentationScopeChannel = PresentationScope.Channel(scopeStack: scopeStack)

                scopeStack.pushScope(runContext, presentationScopeChannel)

                presentationScopeChannel.onRequest(runContext, PresentationScope.eRequest.eBeginScope(m_operationalContext, replyNotify))
            }
        }
    }

    /// The PresentationScope operation handles the presentation of views.
    struct PresentationScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IOperationalModelRequest
        {
            case eBeginScope(OperationalScopeContext, ReplyNotify)
            case eBeginOperationalScope
            case eEndScope(AppError?)
        }

        enum eReply
        {
            case eError(AppError)
            case eDidEndScope(AppError?)
        }

        /// The handler for the PresentationScope requests and replies.
        final class Channel: OperationalModelRequestHandler<eRequest, eReply>
        {
            fileprivate var m_operationalContext: OperationalScopeContext!
            fileprivate var m_replyNotify :       ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/AppOperationalScope/Presentation/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for StartupScope requests.
            /// - Parameter runContext: The execution context for the request.
            /// - Parameter channelRequest: The request from app management to control the app scope.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IOperationalModelRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginScope(let (scopeContext, replyNotify)):

                        m_replyNotify = replyNotify
                        m_operationalContext = scopeContext

                        // Create Monitoring and Service RxSources.
                        m_operationalContext.appRxDir.monitoring.createAllRxSources()
                        m_operationalContext.appRxDir.service.createAllRxSources()

                    case .eBeginOperationalScope:

                        pushApplicationScope(runContext, { (reply: ApplicationScope.eReply) in

                            switch reply
                            {
                                case .eError(let error):

                                    self.reply(.eError(error))

                                case .eDidEndScope:

                                    self.scopeStack.popScope(runContext)
                                    self.onRequest(runContext, eRequest.eEndScope(nil))
                            }
                        })

                    case .eEndScope:

                        // Remove Monitoring and Service RxSources.
                        m_operationalContext.appRxDir.monitoring.removeAllSources()
                        m_operationalContext.appRxDir.service.removeAllSources()

                        // Remove Presentation RxSources and RxObservers.
                        m_operationalContext.appRxDir.input.removeAllSources()
                        m_operationalContext.appRxDir.view.removeAllSources()
                }
            }

            /// Reply back to app management.
            /// - Parameter withReply: The reply to notify back to app management.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IOperationalModelRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }

            /// Push the Application Scope onto the scope stack.
            fileprivate func pushApplicationScope(_ runContext: IRxDevContext, _ notifyReply : @escaping ApplicationScope.ReplyNotify)
            {
                let operationalScopeChannel = ApplicationScope.Channel(scopeStack: scopeStack)

                scopeStack.pushScope(runContext, operationalScopeChannel)

                operationalScopeChannel.onRequest(runContext, ApplicationScope.eRequest.eBeginScope(m_operationalContext, notifyReply))
            }
        }
    }

    /// The Application scope handles the behaviour-operation of the app with application RxSubscriptions active.
    struct ApplicationScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IOperationalModelRequest
        {
            case eBeginScope(OperationalScopeContext, ReplyNotify)
            case eLowMemoryCondition(AppRunContext)
            case eEndScope(AppError?)
        }

        enum eReply
        {
            case eError(AppError)
            case eDidEndScope(AppError?)
        }

        /// The handler for the Application Scope requests and replies.
        final class Channel: OperationalModelRequestHandler<eRequest, eReply>
        {
            struct Constant
            {
                static let AppScopeTag = "ApplicationScope"
            }

            fileprivate let executive = ApplicationExecutive()
            fileprivate var m_operationalContext: OperationalScopeContext!
            fileprivate var m_replyNotify:        ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/AppOperationalScope/Operational/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for ApplicationScope requests.
            /// - Parameter runContext: The executional context for the request.
            /// - Parameter channelRequest: The request from app management to control the app scope.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IOperationalModelRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else
                {
                    replyRequestFailure(channelRequest);
                    return
                }

                switch request
                {
                    case .eBeginScope(let (appContext, replyNotify)):

                        m_replyNotify = replyNotify
                        m_operationalContext = appContext

                        // Create the Application RxSources and RxServices
                        m_operationalContext.appRxDir.keywordMatching.createAllRxSources()
                        m_operationalContext.appRxDir.poiPortalComms.createAllHTTPServices(runContext)

                        executive.executeApplication(m_operationalContext.appRxDir, appScopeTag: Constant.AppScopeTag)

                    case .eLowMemoryCondition:

                        // clear caches.
                        break

                    case .eEndScope:

                        // Stop confirmations from being presented.
                        m_operationalContext.appRxDir.view.confirm?.enabled = false

                        // Unsubscribe all subscriptions related to this view.
                        RxDirectory.removeAll(withTagRegexp: Constant.AppScopeTag)

                        // Remove Application RxSources and RxServices
                        m_operationalContext.appRxDir.keywordMatching.removeAllSources()
                        m_operationalContext.appRxDir.poiPortalComms.removeAllHTTPServices()
                }
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IOperationalModelRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }

            /// Reply back to app management.
            /// - Parameter withReply: The reply to notify back to app management.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify?(withReply)
            }
        }
    }

    /// The context passed between the operational scopes.
    let context : OperationalScopeContext

    /// The scope stack stores the active Operational scopes.
    private let m_scopeStack = ScopeStack(name: "/AppOperationalScope/scopestack")

    /// Initialise with the run context.
    /// - Parameter runContext: The run context that controls the run-configuration of the AppOperationScopeController.
    init(_ runContext: IRxDevContext)
    {
        self.context = OperationalScopeContext(runContext)

        super.init(input: m_scopeStack.input)

        let startupScopeChannel = StartupScope.Channel(scopeStack: m_scopeStack)

        m_scopeStack.pushScope(runContext, startupScopeChannel)
    }

    func issueRequest(_ request : IOperationalModelRequest)
    {
        input.issueRequest(context.runContext, request)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppOperationScopeHandler: The channel that handles App Operation Scope requests.
///

class OperationalModelRequestHandler<RequestDataType, ReplyDataType> : OperationalModelRequestChannel
{
    let scopeStack: OperationalModel.ScopeStack

    /// Initialise with the name of the Scope and Scope Stack.
    init(name: String, scopeStack: OperationalModel.ScopeStack)
    {
        self.scopeStack = scopeStack
        super.init(name: name)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// OperationalScopeContext: The context used by the OperationalModel to store state between the various operational scopes.
///

class OperationalScopeContext
{
    let appRxDir = AppRxDir()
    let runContext : IRxDevContext

    init(_ runContext : IRxDevContext)
    {
        self.runContext = runContext
    }
}

extension RxSubscription
{
    /// Add the RxSubscription to a container for bulk unsubscription.
    /// - Parameter container: The container to put the subscription into.
    /// - Parameter tag: The tag to associate with the subscription.
    func addToDir(_ tag: String)
    {
        let _ = addToDirectory(tag)
    }
}

