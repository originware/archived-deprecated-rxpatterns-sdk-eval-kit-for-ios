//
// Created by Terry Stillone on 19/09/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppRxDevContext: The Context used for control of operational tracing and logging.
///

public class AppRunContext: IRxDevContext
{
    public var indent : Int         = 1
    public var traceEnabled : Bool  = false


    public func pushScope(_ scopeName: String)
    {
        // Scoping not required.
    }
    
    public func popScope()
    {
        // Scoping not required.
    }

    public func log(message : String)
    {
#if RxTraceEnabled
        RxSDK.log.logger?.log(message)
#endif
    }
}