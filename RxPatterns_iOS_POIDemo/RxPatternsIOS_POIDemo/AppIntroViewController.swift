// AppIntroViewController.swift
// Rxpatterns iOS POI Demo Project
//
//
//  Created by Terry Stillone on 25/07/2015.
//  Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//
// The Initial Intro Screen ViewController.
//

import Foundation
import UIKit

class UILabelWithMargin : UILabel
{
    override func drawText(in rect : CGRect)
    {
        let insets = UIEdgeInsets(top:0, left:0, bottom:20, right:0)

        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}

/// The Initial Intro Screen View Controller.
class AppIntroScreenViewController: UIViewController
{
    struct Constant
    {
        static let WebsiteURL = "http://www.originware.com/"
        static let PortraitBackgroundImage = "LaunchBackground-portrait"
        static let LandscapeBackgroundImage = "LaunchBackground-landscape"
    }

    class BackgroundImageOrienter
    {
        fileprivate var m_NSNotificationCenterObserver : AnyObject?
        fileprivate var m_currentOrientation = UIDeviceOrientation.unknown
        fileprivate var m_view: UIView
        fileprivate var m_backgroundImage: UIImageView

        fileprivate var notificationCenter : NotificationCenter           { return NotificationCenter.default }

        init(view: UIView, imageView : UIImageView)
        {
            m_view = view
            m_backgroundImage = imageView

            registerForOrientatingBackgroundImage()
        }

        func registerForOrientatingBackgroundImage()
        {
            if m_NSNotificationCenterObserver == nil
            {
                let device = UIDevice.current

                device.beginGeneratingDeviceOrientationNotifications()

                m_NSNotificationCenterObserver = notificationCenter.addObserver(forName: NSNotification.Name(rawValue: "UIDeviceOrientationDidChangeNotification"), object: device, queue: nil, using: { _ in

                    self.orientate()
                })
            }
            else
            {
                orientate()
            }
        }

        func deregisterForOrientatingBackgroundImage()
        {
            if m_NSNotificationCenterObserver != nil
            {
                notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
            }

            m_NSNotificationCenterObserver = nil
        }

        func orientate()
        {
            guard UIDevice.current.orientation != m_currentOrientation else { return }

            let orientation = UIDevice.current.orientation

            self.m_currentOrientation = orientation

            switch orientation
            {
                case .landscapeLeft, .landscapeRight:

                    self.m_backgroundImage.image = UIImage(named: Constant.LandscapeBackgroundImage)

                case .portrait, .portraitUpsideDown:

                    self.m_backgroundImage.image = UIImage(named: Constant.PortraitBackgroundImage)

                default:
                    break
            }

            self.m_view.setNeedsLayout()
        }
    }

    @IBOutlet weak var BackgroundImage: UIImageView!
    @IBOutlet weak var TitleText: UILabel!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBOutlet weak var HowToUse: UITextView!
    @IBOutlet weak var TapToContinueButton: UIButton!
    @IBOutlet weak var TopToTitleVerticalConstraint: NSLayoutConstraint!

    @IBAction func unwindFromApp(_ segue: UIStoryboardSegue)
    {
        let srcViewController = segue.source 
        let destViewController = segue.destination
        let customSegue = IntroToFromAppSegue(identifier: "idAppToIntro", source: srcViewController, destination: destViewController)

        customSegue.perform()
    }

    /// AppOperationScopeController reference for notifying presentation operational state changes.
    var appOperationScopeController : OperationalModel? = nil

    fileprivate var m_backgroundImageOrienter : BackgroundImageOrienter? = nil
    fileprivate var m_animationCenter = CGPoint(x: 0, y:0)

    var animationCenter: CGPoint                                    { return m_animationCenter }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        m_backgroundImageOrienter = BackgroundImageOrienter(view: self.view, imageView: self.BackgroundImage)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onWebLinkTap(_:)))

        tapGesture.numberOfTapsRequired = 1

        self.DescriptionText.addGestureRecognizer(tapGesture)

        // Style the "Tap To Continue" button.

        AppStyle.styleBorder(inButtonView: self.TapToContinueButton)
        AppStyle.styleBorder(inTextView: self.TitleText)
        AppStyle.styleBorder(inTextView: self.DescriptionText)
        AppStyle.styleBorder(inTextView: self.HowToUse)
    }

    override func viewWillAppear(_ animated : Bool)
    {
        super.viewWillAppear(animated)

        m_backgroundImageOrienter!.registerForOrientatingBackgroundImage()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.m_animationCenter = self.TapToContinueButton.center

        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated : Bool)
    {
        super.viewWillDisappear(animated)

        m_backgroundImageOrienter!.deregisterForOrientatingBackgroundImage()
    }

    func onWebLinkTap(_ gestureRecognizer : UIGestureRecognizer)
    {
        UIApplication.shared.open(URL(string: Constant.WebsiteURL)!)
    }
}
