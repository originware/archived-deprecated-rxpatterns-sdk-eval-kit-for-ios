//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POI Keyword Match
///

open class POIKeywordMatch
{
    open let poiKeyword : String
    open var keywordMatchRanges = [Range<String.Index>]()

    open var totalMatchLength : Int {

        var total = 0

        for range in keywordMatchRanges
        {
            total += poiKeyword.characters.distance(from: range.lowerBound, to: range.upperBound)
        }

        return total
    }

    open var maxMatchLength : Int {

        var maxMatchLength = 0

        for range in keywordMatchRanges
        {
            maxMatchLength = max(maxMatchLength, poiKeyword.characters.distance(from: range.lowerBound, to: range.upperBound))
        }

        return maxMatchLength
    }

    open var haveFullMatch : Bool {

        return maxMatchLength == poiKeyword.characters.count
    }

    public init(poiKeyword : String)
    {
        self.poiKeyword = poiKeyword
    }

    func addMatchForPOIKeyword(_ matchRange : Range<String.Index>)
    {
        func isMatchRangeUnique() -> Bool
        {
            for range in keywordMatchRanges
            {
                // If the matchRange intersects with another match, discard it.
                if doRangesIntersect(poiKeyword, matchRange, range)
                {
                    return false
                }
            }

            return true
        }

        if isMatchRangeUnique()
        {
            keywordMatchRanges.append(matchRange)
        }
    }

    func doRangesIntersect(_ string : String, _ range1 : Range<String.Index>, _ range2 : Range<String.Index>) -> Bool
    {
        let nsRange1 = string.rangeToNSRange(range1)
        let nsRange2 = string.rangeToNSRange(range2)

        return NSIntersectionRange(nsRange1, nsRange2).length > 0
    }
}
