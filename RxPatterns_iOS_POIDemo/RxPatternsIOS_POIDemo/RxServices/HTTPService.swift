//
// Created by Terry Stillone (http://www.originware.com) on 13/07/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import Foundation.NSURLSession
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The HTTP request type
///

protocol IHTTPRequest
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The HTTP Request channel type
///

typealias HTTPRequestChannel = ARxDevChannelNSObject<IHTTPRequest>

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// HTTPService: The RxDevice HTTP network handler.
///

class HTTPService: ARxDevice<HTTPRequestChannel>
{
    class ScopeStack: ARxDevChannelNSObjectScopeStack<IHTTPRequest>
    {
        /// The input channel of the scope stack.
        var input: HTTPRequestChannel { return self }

        /// Service Monitor
        var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: "/Service/HTTPService/monitorNotifier")
    }

    /// The Request Scope handles requests.
    struct RequestScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IHTTPRequest
        {
            case eBeginScope(ReplyNotify)
            case eHTTPRequest(URLRequestReply)
            case eEndScope(AppError?)
        }

        enum eReply
        {
            case eSuccess(URLRequestReply)
            case eFailure(AppError)
            case eDidEndScope
        }

        /// The handler for the RequestScope requests and replies.
        final fileprivate class Channel: HTTPRequestHandler<eRequest, eReply>
        {
            fileprivate var m_replyNotify : ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/Service/HTTPDevice/Request/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for RequestScope requests.
            /// - Parameter runContext: The context for the running the request.
            /// - Parameter channelRequest: The HTTP request channel.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IHTTPRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginScope(let (replyNotify)):

                        m_replyNotify = replyNotify

                    case .eHTTPRequest(let request):

                        // HTTP request AppEvent monitoring notification.
                        self.scopeStack.monitorNotifier.notify(item: .eFromGooglePlacesService_To_HTTPService)

                        // Handle the request from Google Services.
                        handleHTTPRequest(request, m_replyNotify)

                    case .eEndScope:

                        reply(.eDidEndScope)
                }
            }

            /// Reply back to HTTP client.
            /// - Parameter withReply: The reply to be issued back to the client.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IHTTPRequest)
            {
                m_replyNotify(eReply.eFailure(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }

            func handleHTTPRequest(_ request: URLRequestReply, _  notifyReply: @escaping (eReply) -> Void)
            {
                // Create a URL Session HTTP request.
                let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: request.url) { (data: Data?, response: URLResponse?, error: Error?) in

                    func processHTTPRequest() -> eReply
                    {
                        // HTTP request AppEvent monitoring notification.
                        self.scopeStack.monitorNotifier.notify(item: .eFromHTTPService_To_GooglePlacesService)

                        switch (error, response)
                        {
                            case (nil, let httpResponse as HTTPURLResponse!):

                                // NSURLSession issued a reply.
                                return handleHTTPReply(data: data, httpResponse: httpResponse, request: request)

                            case (let httpError?, _):

                                // NSURLSession issued a direct error.
                                return eReply.eFailure(AppError(nsError: httpError as NSError))

                            default:

                                // NSURLSession didn't get a reply within the expected time.
                                let errorDict = [NSLocalizedDescriptionKey: "URLSessionPeer: bad response"]
                                let nsError     = NSError(domain: "URLSessionPeer", code: 0, userInfo: errorDict)

                                return eReply.eFailure(AppError(nsError: nsError))
                        }
                    }

                    func handleHTTPReply(data: Data?, httpResponse: HTTPURLResponse, request: URLRequestReply) -> eReply
                    {
                        switch httpResponse.statusCode
                        {
                            case 201, 200, 401:

                                if let data = data
                                {
                                    let urlReply = URLRequestReply(request: request, replyData: data)

                                    return eReply.eSuccess(urlReply)
                                }

                            default: break
                        }

                        let errorDict = [NSLocalizedDescriptionKey : "URLSession: nil or invalid JSON response"]
                        let nsError = NSError(domain: "URLSession", code: 0, userInfo: errorDict)

                        return eReply.eFailure(AppError(nsError: nsError))
                    }

                    let reply = processHTTPRequest()

                    notifyReply(reply)
                }

                // Perform the HTTP request.
                dataTask.resume()
            }
        }
    }

    /// Notifier used for monitoring internal notifications.
    var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> { return m_scopeStack.monitorNotifier }

    /// The scope stack stores the active HTTP scope.
    private let m_scopeStack = ScopeStack(name: "/HTTPDevice/scopestack")

    /// Initialise with the run context.
    /// - Parameter runContext: The run context that controls the run-configuration of the AppOperationScopeController.
    init(_ runContext: IRxDevContext)
    {
        super.init(input: m_scopeStack.input)

        let requestChannel = RequestScope.Channel(scopeStack: m_scopeStack)

        m_scopeStack.pushScope(runContext, requestChannel)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// HTTPRequestHandler: The channel that handles HTTP requests.
///

fileprivate class HTTPRequestHandler<RequestDataType, ReplyDataType> : HTTPRequestChannel
{
    let scopeStack: HTTPService.ScopeStack

    /// Initialise with the name of the Scope and Scope Stack.
    init(name: String, scopeStack: HTTPService.ScopeStack)
    {
        self.scopeStack = scopeStack
        super.init(name: name)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// URLRequestReply: The HTTPService request/reply entity.
///

class URLRequestReply
{
    /// The URL of the request/reply.
    let url : URL

    /// The http request data.
    let requestData : Any?

    /// The http replay data.
    let replyData: Any?

    /// Indicator of error during the request/reply operation.
    let error : NSError?

    var hasError : Bool { return error != nil }

    /// Initialise with url, request data, reply data and optional error.
    /// - Parameter url: The url of the HTTP request.
    /// - Parameter requestData: The HTTP request data.
    /// - Parameter replyData: The HTTP reply data.
    /// - Parameter error: Optional error.
    init(url: URL, requestData: Any? = nil, replyData: Any? = nil, error: Error? = nil)
    {
        self.url = url
        self.requestData = requestData
        self.replyData = replyData
        self.error = error as NSError?
    }

    /// Initialise with the original request and new reply data.
    /// - Parameter request: The orignal request.
    /// - Parameter replyData: The HTTP reply data.
    init(request: URLRequestReply, replyData: Any)
    {
        self.url = request.url
        self.requestData = request.requestData
        self.replyData = replyData
        self.error = request.error
    }
}
