//
// Created by Terry Stillone (http://www.originware.com) on 30/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The POI Portal request type
///

protocol IPOIPortalRequest
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The POI Portal Request channel type
///

typealias POIPortalChannel = ARxDevChannel<IPOIPortalRequest>

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIPortalService: The ARxOperator POI Portal Service that queries the Google Places HTTP portal.
///

class POIPortalService: ARxDevOperator<POIPortalChannel, POIPortalChannel>
{
    class ScopeStack: ARxDevChannelScopeStack<IPOIPortalRequest>
    {
        /// The input channel of the scope stack.
        var input : POIPortalChannel { return self }

        /// The output channel of the scope stack.
        let output = POIPortalChannel(name: "/Service/POIPortal/ScopeStack/output")

        /// Service Monitor
        var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: "/Service/POIPortal/monitorNotifier")

        fileprivate var m_requestTracker = POIPortalActiveRequestTracker()
    }

    /// The Request Scope handles requests.
    struct RequestScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IPOIPortalRequest
        {
            case eBeginScope(ReplyNotify)
            case ePOIPortalRequest(MultiPOILocateEvent_Request)
            case eEndScope
        }

        enum eReply
        {
            case eRequestSuccess(AppEvent)
            case eRequestFailure(AppEvent)
            case eError(AppError)
            case eDidEndScope
        }

        /// The handler for the RequestScope requests and replies.
        final fileprivate class Channel: POIPortalRequestHandler<eRequest, eReply>
        {
            fileprivate var m_replyNotify : ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/POIPortalRequest/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for RequestScope requests.
            /// - Parameter runContext: The context for the running the request.
            /// - Parameter channelRequest: The POI Portal request channel.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IPOIPortalRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginScope(let (replyNotify)):

                        m_replyNotify = replyNotify

                        // Issue the request to the Google Places Service.
                        scopeStack.output.issueRequest(runContext, GooglePlacesPortalService.RequestScope.eRequest.eBeginScope({ (reply) in

                            // Handle the reply from the Google Places Service.

                            switch reply
                            {
                                case .eRequestSuccess(let urlReply):

                                    self.scopeStack.m_requestTracker.updateFromReply(urlReply)

                                    let locateResultAppEvent = AppEvent(appEventType: eAppEventType.ePOILocateResult(urlReply))

                                    replyNotify(eReply.eRequestSuccess(locateResultAppEvent))

                                    // POIPortal Reply AppEvent monitoring notification.
                                    self.scopeStack.monitorNotifier.notify(item: .eFromGooglePlaces_To_POIService)

                                case .eRequestFailure(let urlReply):

                                    let locateResultAppEvent = AppEvent(appEventType: eAppEventType.ePOILocateResult(urlReply))

                                    replyNotify(eReply.eRequestSuccess(locateResultAppEvent))

                                case .eError(let error):

                                    replyNotify(eReply.eError(error))

                                case .eDidEndScope:

                                    replyNotify(.eDidEndScope)
                            }
                        }))

                    case .ePOIPortalRequest(let multiRequest):

                        for poiKeyword in multiRequest.poiKeywords where !scopeStack.m_requestTracker.isActive(poiKeyword: poiKeyword, location : multiRequest.location)
                        {
                            let request = POILocateEvent_Request(poiKeyword: poiKeyword, requestedPOIKeywords: multiRequest.poiKeywords, location : multiRequest.location)

                            scopeStack.output.issueRequest(runContext, GooglePlacesPortalService.RequestScope.eRequest.ePOIPortalRequest(request))

                            // POIPortal Request AppEvent monitoring notification.
                            scopeStack.monitorNotifier.notify(item: .eFromPOIRequestor_To_POIPortal)
                        }

                    case .eEndScope:

                        reply(.eDidEndScope)
                }
            }

            /// Reply back to POI Portal client.
            /// - Parameter withReply: The reply to be issued back to the client.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IPOIPortalRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }
        }
    }

    /// Notifier used for monitoring internal notifications.
    var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> { return m_scopeStack.monitorNotifier }

    /// The scope stack stores the POI Portal Requests.
    private let m_scopeStack = ScopeStack(name: "/Service/POIPortal/scopestack")

    /// Initialise with the run context.
    /// - Parameter runContext: The run context that controls the run-configuration of the AppOperationScopeController.
    init(_ runContext: IRxDevContext)
    {
        super.init(input: m_scopeStack.input, output: m_scopeStack.output)

        let requestChannel = RequestScope.Channel(scopeStack: m_scopeStack)

        m_scopeStack.pushScope(runContext, requestChannel)
    }

    /// Clear state.
    func clearLocatedPOIKeywords()
    {
        m_scopeStack.m_requestTracker.clear()
    }
}

fileprivate class POIPortalRequestHandler<RequestDataType, ReplyDataType> : POIPortalChannel
{
    let scopeStack: POIPortalService.ScopeStack

    /// Initialise with the name of the Scope and Scope Stack.
    init(name: String, scopeStack: POIPortalService.ScopeStack)
    {
        self.scopeStack = scopeStack
        super.init(name: name)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIPortalActiveRequestTracker: The tracker of active POI requests.
///

fileprivate class POIPortalActiveRequestTracker
{
    fileprivate struct Constant
    {
        static let POINearByDistance = CLLocationDistance(100)
    }

    fileprivate lazy var activePOIKeywords = Set<String>()
    fileprivate var m_lastLocation: CLLocation? = nil

    /// Indicate if the given keyword is active.
    /// - Parameter poiKeyword: The keyword is check.
    func isActive(poiKeyword: String, location: CLLocation) -> Bool
    {
        let keywordIsActive = activePOIKeywords.contains(poiKeyword)

        return isNearLastLocation(location) && keywordIsActive
    }

    /// Update the active keywords state from the given locate request.
    /// - Parameter poiLocateRequest: The POI Locate request to update state from.
    func updateFromReply(_ poiLocateResult : POILocateEvent_Reply)
    {
        activePOIKeywords.formUnion(Set<String>(poiLocateResult.requestedKeywords))
        m_lastLocation = poiLocateResult.location
    }

    /// Clear all active keywords in state.
    func clear()
    {
        activePOIKeywords.removeAll()
    }

    /// Indicate if the given location is near the last known location.
    /// - Parameter location: The location to check.
    fileprivate func isNearLastLocation(_ location : CLLocation) -> Bool
    {
        return (m_lastLocation == nil) || (m_lastLocation!.distance(from: location) < Constant.POINearByDistance)
    }
}
