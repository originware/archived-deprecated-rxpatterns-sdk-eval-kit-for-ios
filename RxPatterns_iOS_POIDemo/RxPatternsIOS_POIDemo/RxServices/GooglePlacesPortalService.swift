//
// Created by Terry Stillone (http://www.originware.com) on 22/09/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// GooglePlacesPortalService: The ARxOperator POI Portal Service that queries the Google Places HTTP portal.
///

class GooglePlacesPortalService: ARxDevOperator<POIPortalChannel, HTTPRequestChannel>
{
    fileprivate struct Constant
    {
        static let GoogleAPIKey     = "AIzaSyBjBR4UyV_ypKrtJKUu0CyBQZdN1dk9GuQ"
        static let PortalName       = "Google Places"
        static let PortalBaseURL    = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
        static let POILocateRadius  = 1000
    }

    class ScopeStack: ARxDevChannelScopeStack<IPOIPortalRequest>
    {
        /// The input channel of the scope stack.
        var input : POIPortalChannel { return self }

        /// The output channel of the scope stack.
        let output = HTTPRequestChannel(name: "/Service/POIPortal/ScopeStack/output")

        /// Service Monitor
        var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: "/Service/GooglePlaces/monitorNotifier")
    }

    /// The Request Scope handles requests.
    struct RequestScope
    {
        typealias ReplyNotify = (eReply) -> Void

        enum eRequest : IPOIPortalRequest
        {
            case eBeginScope(ReplyNotify)
            case ePOIPortalRequest(POILocateEvent_Request)
            case eEndScope
        }

        enum eReply
        {
            case eRequestSuccess(POILocateEvent_Reply)
            case eRequestFailure(POILocateEvent_Reply)
            case eError(AppError)
            case eDidEndScope
        }

        /// The handler for the RequestScope requests and replies.
        final fileprivate class Channel: GooglePlacesRequestHandler<eRequest, eReply>
        {
            fileprivate var m_replyNotify : ReplyNotify!

            init(scopeStack: ScopeStack)
            {
                super.init(name: "/POIPortalRequest/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for RequestScope requests.
            /// - Parameter runContext: The context for the running the request.
            /// - Parameter channelRequest: The POI Portal request channel.
            func onRequest(_ runContext: IRxDevContext, _ channelRequest: IPOIPortalRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginScope(let (replyNotify)):

                        m_replyNotify = replyNotify

                        scopeStack.output.issueRequest(runContext, HTTPService.RequestScope.eRequest.eBeginScope({ (reply) in

                            switch reply
                            {
                                case .eSuccess(let urlReply):

                                    // POIPortal Reply AppEvent monitoring notification.
                                    self.scopeStack.monitorNotifier.notify(item: .eFromHTTPService_To_GooglePlacesService)

                                    replyNotify(self.handleHTTPReply(urlReply))

                                case .eFailure(let error):

                                    replyNotify(eReply.eError(error))

                                case .eDidEndScope:

                                    replyNotify(.eDidEndScope)
                            }
                        }))

                    case .ePOIPortalRequest(let request):

                        // POIPortal Request AppEvent monitoring notification.
                        self.scopeStack.monitorNotifier.notify(item: .eFromPOIPortalService_To_GooglePlacesService)

                        // Encode the Google Places JSON and emit to HTTPServices.
                        if let httpRequest = handlePOIPortalRequest(request)
                        {
                            scopeStack.output.issueRequest(runContext, HTTPService.RequestScope.eRequest.eHTTPRequest(httpRequest))
                        }
                        else
                        {
                            m_replyNotify(eReply.eError(AppError("POI Portal request could not be formed: invalid POI Request data.")))
                        }

                    case .eEndScope:

                        reply(.eDidEndScope)
                }
            }

            /// Reply back to POI Portal client.
            /// - Parameter withReply: The reply to be issued back to the client.
            fileprivate func reply(_ withReply: eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IPOIPortalRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for App Function Scope: \(channelRequest)")))
            }

            func handlePOIPortalRequest(_ request: POILocateEvent_Request) -> URLRequestReply?
            {
                func generateURLAsString(_ poiKeyword: String) -> String?
                {
                    let coordinate          = request.location.coordinate
                    let urlAsString         = "\(Constant.PortalBaseURL)?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(Constant.POILocateRadius)&type=\(poiKeyword)&key=\(Constant.GoogleAPIKey)"

                    return urlAsString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                }

                guard let urlAsString = generateURLAsString(request.poiKeyword), let url = URL(string: urlAsString)
                else { return nil }

                return URLRequestReply(url: url, requestData: request, replyData: nil)
            }

            func handleHTTPReply(_ urlRequestReply: URLRequestReply) -> eReply
            {
                guard let poiLocateRequest = urlRequestReply.requestData as? POILocateEvent_Request
                else { return .eError(AppError("Internal Error: cannot get original POI request")) }

                guard urlRequestReply.error == nil
                else { return .eRequestFailure(POILocateEvent_Reply(locateSourceName: Constant.PortalName, requestPOIKeywords: [poiLocateRequest.poiKeyword], location : poiLocateRequest.location, error: urlRequestReply.error!)) }

                guard let jsonAsData = urlRequestReply.replyData as? Data
                else { return .eError(AppError("Internal Error: cannot get HTTP reply data")) }

                let jsonDataSource = CrispJSON.JDataSource(jsonAsData)
                let jsonParser = CrispJSON.JParser(jsonDataSource)

                guard let pois = extractPOIsFromJSON(jsonParser, requestedPOIKeywords: poiLocateRequest.requestedPOIKeywords)
                else { return .eError(AppError("Cannot extract JSON from HTTP reply")) }

                let locateReply = POILocateEvent_Reply(locateSourceName: Constant.PortalName, requestPOIKeywords: [poiLocateRequest.poiKeyword], location : poiLocateRequest.location, pois: pois)

                return .eRequestSuccess(locateReply)
            }

            func extractPOIsFromJSON(_ jsonParser: CrispJSON.JParser, requestedPOIKeywords: Set<String>) -> [POI]?
            {
                func remark(_ message: String)
                {
                    // print(message)
                }

                return jsonParser.parse({ (json) in

                    var pois = [POI]()

                    json["results"]?.forArray({ (result) in

                        guard let name          = result ->> "name" ->> JValue<String>.value     else { remark("name field missing"); return }
                        guard let address       = result ->> "vicinity" ->> JValue<String>.value else { remark("address field missing"); return }
                        let keywordTypes        = result ->> "types" ->> JValue<[String]>.value

                        if let coordinate = result ->> "geometry" ->> "location"
                        {
                            guard let lat = coordinate ->> "lat" ->> JValue<Double>.value else { remark("lat field"); return }
                            guard let lng = coordinate ->> "lng" ->> JValue<Double>.value else { remark("lng field"); return }

                            let keywordTypeSet = keywordTypes != nil ? Set<String>(keywordTypes!) : requestedPOIKeywords
                            let commonKeywords = keywordTypeSet.intersection(requestedPOIKeywords)

                            if !commonKeywords.isEmpty
                            {
                                for poiKeyword in commonKeywords
                                {
                                    let location = CLLocation(latitude: lat, longitude:lng)
                                    let poi = POI(poiKeyword: poiKeyword, location: location, name: name, address: address)

                                    pois.append(poi)
                                }
                            }
                        }
                    })

                    return pois
                })
            }
        }
    }

    /// Notifier used for monitoring internal notifications.
    var monitorNotifier: RxNotifier_NotifyConsumers<eAppOpEventType> { return m_scopeStack.monitorNotifier }

    /// The scope stack stores the POI Portal Requests.
    private let m_scopeStack = ScopeStack(name: "/Service/GooglePlaces/scopestack")

    /// Initialise with the run context.
    /// - Parameter runContext: The run context that controls the run-configuration of the AppOperationScopeController.
    init(_ runContext: IRxDevContext)
    {
        super.init(input: m_scopeStack.input, output: m_scopeStack.output)

        let requestChannel = RequestScope.Channel(scopeStack: m_scopeStack)

        m_scopeStack.pushScope(runContext, requestChannel)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// GooglePlacesRequestHandler: The channel that handles POI Portal requests.
///

fileprivate class GooglePlacesRequestHandler<RequestDataType, ReplyDataType> : POIPortalChannel
{
    let scopeStack: GooglePlacesPortalService.ScopeStack

    /// Initialise with the name of the Scope and Scope Stack.
    init(name: String, scopeStack: GooglePlacesPortalService.ScopeStack)
    {
        self.scopeStack = scopeStack
        super.init(name: name)
    }
}