//
// Created by Terry Stillone (http://www.originware.com) on 7/07/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

struct Constant
{
    static let POITypePlaceHolderColumns = 3
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_KeywordResults_UITextView: An adapter to present POI keyword matches.
///

open class ViewAdapter_KeywordResults_UITextView<ItemType>: ARxObserver<ItemType>
{
    /// The UITextView to present to.
    fileprivate let m_textView: UITextView

    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// Notifier used for monitoring internal notifications.
    let monitorNotifier:      RxNotifier_NotifyConsumers<eAppOpEventType>

    /// Initialise with name tag, target UiTextView and text formatter.
    init(tag : String, textView: UITextView, logFormatter: AppStyle_LogFormatter)
    {
        self.m_textView = textView

        // Create monitor notifier.
        self.monitorNotifier = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: tag + "/monitorNotifier")

        // The observer runs in the UIThread.
        super.init(tag: tag)
    }

    /// Notify the UiTextView adapter of an inroute appEvent.
    public override final func notify(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatch(async: { [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    public func reset()
    {
        m_textView.attributedText = AppStyle.getFormattedPOIKeywordsPlaceHolder(Constant.POITypePlaceHolderColumns)
    }

    fileprivate func onAppEvent(_ appEvent: AppEvent)
    {
        monitorNotifier.notify(item: .eFromPOIKeywordMatcher_To_POIKeywordsResults)

        switch appEvent.appEventType
        {
            case .ePOIKeywordMatchSet:

                // Process Keyword Match AppEvent notifications.
                m_textView.attributedText = formatPOIKeywordMatchResultsText(appEvent)

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }

    /// Format a collection of keyword matches from an AppEvent.
    /// - Parameter appEvent: The appevent with the Keyword Match.
    /// - Returns: The formatted keywords as an NSAttributedString.
    fileprivate func formatPOIKeywordMatchResultsText(_ appEvent : AppEvent) -> NSAttributedString
    {
        func formatLogText(_ string : String) -> NSMutableAttributedString
        {
            let font = AppStyle.font(.eFont_POIResultsText)
            let textColor = AppStyle.color(.eColor_POISearchResultText)
            let paragraphStyle = NSMutableParagraphStyle()
            
            paragraphStyle.lineHeightMultiple = 1
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
            paragraphStyle.alignment = NSTextAlignment.center
            
            return NSMutableAttributedString(string: string + "\n", attributes: [
                        NSFontAttributeName : font,
                        NSForegroundColorAttributeName : textColor,
                        NSParagraphStyleAttributeName : paragraphStyle])
        }
        
        let poikeywordCollectionMatches = appEvent.appEventType.poikeywordCollectionMatches
        
        if poikeywordCollectionMatches.count == 0
        {
            return formatLogText("")
        }
        
        let matchText : NSMutableAttributedString = formatLogText("")
        
        let highlightColor : UIColor = AppStyle.color(.eColor_POIHighlightBackground)
        let matchedTextColor : UIColor = AppStyle.color(.eColor_POIText)
        
        for (poiKeyword, matches) in poikeywordCollectionMatches.poiKeywordMatchesByPOIKeyword
        {
            let text = formatLogText(poiKeyword)
            
            for range in matches.keywordMatchRanges
            {
                let nsRange : NSRange = poiKeyword.rangeToNSRange(range)

                text.addAttribute(NSBackgroundColorAttributeName, value:highlightColor, range:nsRange)
                text.addAttribute(NSForegroundColorAttributeName, value:matchedTextColor, range:nsRange)
            }

            matchText.append(text)
        }
        
        return matchText
    }
}
