//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

@available(iOS 8, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DeviceAdapter_Location: The Location Source.
///
///    Simulates location if the Location Service fails.
///

class DeviceAdapter_Location: RxSource<AppEvent>
{
    /// The last discovered position, posiibly the simulated position if a real one was not supplied intime.
    var currentLocation : CLLocation? { return m_currentLocation }

    /// Backing variable for currentLocation.
    fileprivate var m_currentLocation : CLLocation? = nil

    /// Initialise with the RxObject tag.
    /// - Parameter tag: The RxObject tag for this instance.
    init(tag: String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    /// Create the RxEvalOp for this RxSource.
    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            typealias LocationRequestType = RxLocationInDev.LocationScope.eRequest

            let context = AppRunContext()
            var locationDevice = RxLocationInDev(context)
            var outNotifier : ARxNotifier<AppEvent>! = nil

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                func notifyLocation(_ location: CLLocation, isSimulatedEvent: Bool)
                {
                    let appEvent = AppEvent(appEventType: eAppEventType.eLocationChange(eDeviceEvent_Location.eLocation_LastKnown(location)), isSimulatedEvent: isSimulatedEvent)

                    // Run the notification in the RxEvalNode's RxEvalQueue.
                    outNotifier.notify(item: appEvent)

                    self.m_currentLocation = location
                }

                func notifySimulatedLocation()
                {
                    notifyLocation(AppConstant.SimulatedPosition, isSimulatedEvent: true)
                }

                switch stateChange
                {
                    case .eNewSubscriptionInSubscriptionThread:

                        if evalNode.subscriptionCount == 0
                        {
                            outNotifier = evalNode.getSharedOutNotifier(forSync: false, withBuffering: true)

                            // Initiate a Location session with the Loaction Device (RxLocationInDev).
                            let timeout = AppViewController.Constant.LocationSimulationTimeout

                            locationDevice.input.issueRequest(context, LocationRequestType.eBeginLocationSession(timeout: timeout, { (reply) in

                                switch reply
                                {
                                    case .eHaveLocation(let location):      notifyLocation(location, isSimulatedEvent: false)
                                    case .eDidTimeoutOnFirstLocation:       notifySimulatedLocation()
                                    case .eError(let error):                outNotifier.notifyCompleted(error: error)
                                }
                            }))
                        }

                    case .eSubscriptionBegin:

                        // Notify subscriptions with an initial location notification.
                        locationDevice.input.issueRequest(context, LocationRequestType.eGetLastLocation)

                    case .eEvalBegin:

                        break

                    case .eEvalEnd:

                        /// Destroy CoreLocationController on the last subscription.
                        locationDevice.input.issueRequest(context, LocationRequestType.eEndLocationSession)

                    default:
                        break
                }
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The Location RxDevice location request type
//

public protocol IRxLocationRequest
{
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The Location RxDevice channel type
//

public typealias ARxDevLocationChannel = ARxDevChannelNSObject<IRxLocationRequest>

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxLocationInDev: The Location RxDevice, interfaces with Location Services.
///
///    The device also supports initial position timeout.
///

public class RxLocationInDev : ARxDevice<ARxDevLocationChannel>
{
    public class ScopeStack: ARxDevChannelNSObjectScopeStack<IRxLocationRequest>
    {
        public var input:  ARxDevLocationChannel { return self }
    }

    /// The location scope of the RxLocationInDev is responsbile for managing Location Services and replying location updates back to the client.
    public struct LocationScope
    {
        public typealias ReplyNotify = (eReply) -> Void

        public enum eRequest : IRxLocationRequest
        {
            case eBeginLocationSession(timeout: RxTimeOffset?, ReplyNotify)
            case eGetLastLocation
            case eEndLocationSession
        }

        public enum eReply
        {
            case eHaveLocation(location: CLLocation)
            case eDidTimeoutOnFirstLocation
            case eError(AppError)
        }

        /// The handler for the LocationScope requests and replies.
        final public class Channel: RxLocationChannelHandler<eRequest, eReply>, CLLocationManagerDelegate
        {
            fileprivate var m_locationManager: CLLocationManager? = nil
            fileprivate let m_useSignificantChanges = CLLocationManager.significantLocationChangeMonitoringAvailable()
            fileprivate var m_lastLocation: CLLocation? = nil

            // The timeout for the first location update.
            fileprivate var m_timeout : RxTimeOffset? = nil
            fileprivate var m_locationDidTimeout = false

            fileprivate var m_context : IRxDevContext!

            public init(scopeStack: ScopeStack)
            {
                super.init(name: "/RxLocationInDev/LocationScope/Channel", scopeStack: scopeStack)

                routeOutput(toNotifyFunc: onRequest)
            }

            deinit
            {
                stopLocationService()
            }

            /// The handler for LocationScope requests.
            /// - Parameter context: The context for the request.
            /// - Parameter channelRequest: The request from the client in the form of a general location request type.
            func onRequest(_ context: IRxDevContext, _ channelRequest: IRxLocationRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginLocationSession(let (timeout, replyNotify)):

                        m_timeout = timeout
                        m_replyNotify = replyNotify
                        m_context = context

                        if CLLocationManager.locationServicesEnabled()
                        {
                            m_locationManager = CLLocationManager()
                            m_locationManager!.delegate = self
                        }
                        else
                        {
                            reply(.eDidTimeoutOnFirstLocation)
                        }

                    case .eGetLastLocation:

                        if let location = m_lastLocation
                        {
                            reply(.eHaveLocation(location: location))
                        }
                        else if m_locationDidTimeout
                        {
                            reply(.eDidTimeoutOnFirstLocation)
                        }

                    case .eEndLocationSession:

                        stopLocationService()
                }
            }

            /// Reply back to client.
            /// - Parameter withReply: The reply to notify back to the client.
            fileprivate func reply(_ withReply : eReply)
            {
                m_replyNotify(withReply)
            }

            /// Notify a request failure back to the client.
            /// - Parameter channelRequest: The request that failed.
            fileprivate func replyRequestFailure(_ channelRequest: IRxLocationRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for RxLocationInDev.LocationTimeoutScope request was: \(channelRequest)")))
            }

            /// The location didUpdateLocations delegate handler.
            public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
            {
                guard let currentLocation: CLLocation = locations.last, m_lastLocation != currentLocation else { return }

                m_lastLocation = currentLocation

                // If the timeout scope is still effective, then terminate it.
                if scopeStack.count > 1
                {
                    scopeStack.input.issueRequest(m_context, LocationTimeoutScope.eRequest.eEndTimeout)
                }

                // Reply the location back to the client.
                reply(.eHaveLocation(location: currentLocation))
            }

            /// The location didFailWithError delegate handler.
            public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
            {
                let nsError = error as NSError

                guard nsError.code != CLError.locationUnknown.rawValue else { return }

                reply(.eError(AppError(nsError: nsError)))
            }

            /// The location didChangeAuthorizationStatus delegate handler.
            public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
            {
                switch status
                {
                    case .notDetermined:
                        manager.requestAlwaysAuthorization()

                    case .restricted, .denied:
                        reply(.eError(AppError("Location Services Not Enabled")))

                    default:

                        pushLocationTimeoutScope(m_context)
                        startLocationService()
                }
            }

            /// Push the Location Timeout scope onto the scope stack and begin the timeout.
            fileprivate func pushLocationTimeoutScope(_ context: IRxDevContext)
            {
                guard let timeout = m_timeout else { return }

                let locationTimeoutScopeChannel = LocationTimeoutScope.Channel(scopeStack: scopeStack)

                scopeStack.pushScope(context, locationTimeoutScopeChannel)

                locationTimeoutScopeChannel.onRequest(context, LocationTimeoutScope.eRequest.eBeginTimeout(timeout: timeout, { (reply: LocationTimeoutScope.eReply) in

                    switch reply
                    {
                        case .eDidTimeout:

                            self.m_locationDidTimeout = true

                            self.reply(.eDidTimeoutOnFirstLocation)

                            locationTimeoutScopeChannel.onRequest(context, LocationTimeoutScope.eRequest.eEndTimeout)

                        case .eError(let error):

                            self.reply(.eError(error))

                        case .eDidEndLocationTimeoutScope:

                             self.scopeStack.popScope(context)
                    }
                }))
            }
        }
    }

    /// The LocationTimeoutScope is responsible for checking if an initial location update is not supplied within a given timeout.
    public struct LocationTimeoutScope
    {
        public typealias ReplyNotify = (eReply) -> Void

        public enum eRequest : IRxLocationRequest
        {
            case eBeginTimeout(timeout: RxTimeOffset, ReplyNotify)
            case eEndTimeout
        }

        public enum eReply
        {
            case eDidTimeout
            case eError(AppError)
            case eDidEndLocationTimeoutScope
        }

        /// The handler for the LocationTimeoutScope requests and replies.
        final public class Channel: RxLocationChannelHandler<eRequest, eReply>
        {
            fileprivate let m_window = RxWindow(tag: "LocationTimeoutScope/timeout")

            public init(scopeStack: ScopeStack)
            {
                super.init(name: "/RxLocationInDev/LocationTimeoutScope/Channel", scopeStack: scopeStack)

                self.routeOutput(toNotifyFunc: onRequest)
            }

            /// The handler for LocationTimeoutScope requests.
            /// - Parameter context: The context for the request.
            /// - Parameter channelRequest: The request from the client in the form of a general location request type.
            func onRequest(_ context: IRxDevContext, _ channelRequest: IRxLocationRequest)
            {
                // Cast the request to this scope.
                guard let request = channelRequest as? eRequest
                else { replyRequestFailure(channelRequest); return }

                switch request
                {
                    case .eBeginTimeout(let (timeout, replyNotify)):

                        m_replyNotify = replyNotify

                        let _ = m_window <- eWindowCommand.eSetWindowEndAction({

                            replyNotify(.eDidTimeout)
                        })

                        m_window.createSingleWindow(startTime: RxTime(), duration: timeout)

                    case .eEndTimeout:

                        // Cancel pending windows.
                        m_window.cancelAll()

                        m_replyNotify(.eDidEndLocationTimeoutScope)
                }
            }

            fileprivate func replyRequestFailure(_ channelRequest: IRxLocationRequest)
            {
                m_replyNotify(eReply.eError(AppError("Invalid request for RxLocationInDev.LocationTimeoutScope request was: \(channelRequest)")))
            }
        }
    }

    private let m_scopeStack = ScopeStack(name: "RxLocationInDev/scopestack")

    public init(_ context: IRxDevContext)
    {
        super.init(input: m_scopeStack.input)

        let locationScopeChannel = LocationScope.Channel(scopeStack: m_scopeStack)

        m_scopeStack.pushScope(context, locationScopeChannel)
    }
}

extension RxLocationInDev.LocationScope.Channel
{
    /// Start Location Services.
    fileprivate func startLocationService()
    {
        guard let locationManager = m_locationManager else { return }

        if m_useSignificantChanges
        {
            locationManager.distanceFilter = AppConstant.LocationChangeUpdateDistance
            locationManager.desiredAccuracy = kCLLocationAccuracyBest

            locationManager.startMonitoringSignificantLocationChanges()
        }
        else
        {
            locationManager.distanceFilter = AppConstant.LocationChangeUpdateDistance
            locationManager.desiredAccuracy = kCLLocationAccuracyBest

            locationManager.startUpdatingLocation()
        }
    }

    /// Stop Location Services.
    fileprivate func stopLocationService()
    {
        guard let locationManager = m_locationManager else { return }

        if m_useSignificantChanges
        {
            locationManager.stopMonitoringSignificantLocationChanges()
        }
        else
        {
            locationManager.stopUpdatingLocation()
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxLocationChannelHandler: The channel that handles Location Service requests.
///

public class RxLocationChannelHandler<RequestDataType, ReplyDataType> : ARxDevLocationChannel
{
    public typealias ReplyNotify = (ReplyDataType) -> Void

    internal var m_replyNotify : ReplyNotify!

    public let scopeStack: RxLocationInDev.ScopeStack

    public init(name: String, scopeStack: RxLocationInDev.ScopeStack)
    {
        self.scopeStack = scopeStack
        super.init(name: name)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Operators
//

precedencegroup ManipulatorPrecedence {
    associativity: left
    higherThan: TernaryPrecedence
}

infix operator <- : ManipulatorPrecedence

