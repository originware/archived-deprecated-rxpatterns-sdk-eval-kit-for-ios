//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Confirmation: An adapter to present confirmations triggered from AppEvent notifications.
///

open class ViewAdapter_Confirmation<ItemType> : ARxObserver<ItemType>
{
    // Enabler.
    open var enabled = true

    /// The current alerts begin presented.
    fileprivate lazy var m_pendingAlertQueue = [String]()

    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// Get the top most controller.
    fileprivate var topMostController : UIViewController?
    {
        func isVisible(_ viewController : UIViewController?) -> Bool
        {
            return (viewController != nil) && viewController!.isViewLoaded && (viewController!.view.window != nil)
        }

        var topController : UIViewController? = UIApplication.shared.keyWindow?.rootViewController

        while topController != nil
        {
            let nextPresentedController = topController!.presentedViewController

            /// Check its view is loaded and in the view hierarchy.
            if !isVisible(nextPresentedController)
            {
                break
            }

            topController = nextPresentedController
        }

        return isVisible(topController) ? topController : nil
    }

    /// Initialise with name tag.
    init(tag : String)
    {
        super.init(tag : tag)
    }


    /// Notify the confirmation adapter of an inroute appEvent.
    open override func notify(item: ItemType)
    {
        if !enabled { return }

        if let appEvent = item as? AppEvent
        {
            let message = appEvent.appEventType.confirmation

            // If the alert has not already been presented, then either queue or present.
            if !m_pendingAlertQueue.contains(message)
            {
                m_pendingAlertQueue.append(message)

                if m_pendingAlertQueue.count == 1
                {
                    UIQueue.dispatch(async: {
                        self.presentConfirmation(message)
                    })
                }
            }
        }
        else
        {
            fatalError("Expected a String based item")
        }
    }

    /// Present Confirmation dialog.
    fileprivate func presentConfirmation(_ message : String)
    {
        if !enabled { return }

        // Get present the confirmation on the top most view controller.
        if let viewController = topMostController
        {
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)

            alert.modalPresentationStyle = UIModalPresentationStyle.popover
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { [unowned self] action in

                // Dismiss the presented confirmation.
                alert.dismiss(animated: true, completion: nil)

                // Remove the last alert request.
                self.m_pendingAlertQueue.remove(at: 0)

                // If there are more messages queued, present the next.
                if let nextMessage = self.m_pendingAlertQueue.first
                {
                    self.presentConfirmation(nextMessage)
                }
            }))

            if let popoverController = alert.popoverPresentationController
            {
                popoverController.sourceView = viewController.view
                popoverController.sourceRect = viewController.view.bounds
            }

            // Present the confirmation.
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}
