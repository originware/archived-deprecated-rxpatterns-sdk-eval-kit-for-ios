//
// Created by Terry Stillone (http://www.originware.com) on 19/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

open class ViewAdapter_UIControl<ItemType> : ARxObserver<ItemType>
{
    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue = RxSDK.evalQueue.UIThreadQueue

    init()
    {
        // The observer runs in the UIThread.
        super.init(tag : "ViewAdapter_UIControl")
    }

    /// Notify the UI Control adapter of an inroute appEvent.
    public override final func notify(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatch(async: { [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    fileprivate func onAppEvent(_ appEvent: AppEvent)
    {
        switch appEvent.appEventType
        {
            case .eUIControlTouch(let (uiControl, _)):

                if appEvent.isSimulatedEvent
                {
                    if let button = uiControl as? UIButton
                    {
                        button.sendActions(for: UIControlEvents.touchDown)
                    }
                }

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}
