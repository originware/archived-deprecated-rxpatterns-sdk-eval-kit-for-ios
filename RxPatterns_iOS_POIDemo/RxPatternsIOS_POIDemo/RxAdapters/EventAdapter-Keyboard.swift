//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// EventAdapter_keyboard: Adapt from Keyboard NSNotifications to AppEvent RxNotifications.
///

class EventAdapter_keyboard: RxSource<AppEvent>
{
    /// The show keyboard NSNotification observers.
    fileprivate var m_NSNotificationCenterShowObserver: AnyObject? = nil

    /// The hide keyboard NSNotification observers.
    fileprivate var m_NSNotificationCenterHideObserver: AnyObject? = nil

    /// Initialise with The RxObject tag for this instance.
    /// - Parameter tag: The RxObject tag.
    init(tag : String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.registerForKeyboardNSNotifications(evalNode.getSharedOutNotifier(forSync: false))

                    case eRxEvalStateChange.eEvalEnd:

                        self.deregisterForKeyboardNSNotifications()

                    default:
                        break
                }
            }
        }
    }

    /// Register for NSNotification keyboard events.
    /// - Parameter notifier: The notifier to notify keyboard changes to.
    fileprivate func registerForKeyboardNSNotifications(_ notifier : ARxNotifier<AppEvent>)
    {
        m_NSNotificationCenterShowObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UIKeyboardDidShowNotification"), object: nil, queue: nil, using: { [weak weakNotifier = notifier] (notification : Notification!) in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notify(item: AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didShow)))
            }
        })

        m_NSNotificationCenterHideObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UIKeyboardDidHideNotification"), object: nil, queue: nil, using: { [weak weakNotifier = notifier] (notification : Notification!) in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notify(item: AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didHide)))
            }
        })
    }

    /// De-register for NSNotification keyboard events.
    fileprivate func deregisterForKeyboardNSNotifications()
    {
        if m_NSNotificationCenterShowObserver != nil
        {
            NotificationCenter.default.removeObserver(m_NSNotificationCenterShowObserver!)
        }

        if m_NSNotificationCenterHideObserver != nil
        {
            NotificationCenter.default.removeObserver(m_NSNotificationCenterHideObserver!)
        }
    }
}
