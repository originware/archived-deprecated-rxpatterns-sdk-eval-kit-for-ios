//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// UITouchController: Adapt from UITouch events to AppEvent RxNotifications.
///

class EventAdapter_UITouch: RxSource<AppEvent>
{
    lazy fileprivate var m_touchController : UITouchHandler = UITouchHandler()

    /// Initialise with the RxObject tag.
    /// - Parameter tag: The RxObject tag.
    init(tag: String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.m_touchController.notifier = evalNode.getSharedOutNotifier(forSync: false)

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_touchController.notifier = nil
                        self.deregisterAll()

                    default:
                        break
                }
            }
        }
    }

    /// Register for touch events.
    /// - Parameter control: The UIControl to register for.
    /// - Parameter touchTarget: The UIControl target.
    func registerForUITouch(_ control : UIControl, touchTarget: eDeviceEvent_Touch)
    {
        m_touchController.registerForUITouch(control, touchTarget : touchTarget)
    }

    /// De-register for touch events
    /// - Parameter control: The UIControl to deregister for.
    func deregisterForUITouch(_ control : UIControl)
    {
        m_touchController.deregisterForUITouch(control)
    }

    /// De-register all registered touchs.
    func deregisterAll()
    {
        m_touchController.deregisterAll()
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// UITouchHandler: Handler Touch Registrations and De-registrations.
///
///     Note: Touch event handlers are constained to run through classes inheriting from NSObject.
///

class UITouchHandler: NSObject
{
    /// The current notifier to notify AppEvent touches to.
    var notifier: ARxNotifier<AppEvent>? = nil

    /// Map of UIControl Target to UIControl.
    fileprivate var m_touchTargetByUIControl = [UIControl : eDeviceEvent_Touch]()

    /// Register UIControl for touches to target.
    /// - Parameter control: The UIControl to register for touches.
    /// - Parameter touchTarget: The target of the touch.
    func registerForUITouch(_ control : UIControl, touchTarget: eDeviceEvent_Touch)
    {
        m_touchTargetByUIControl[control] = touchTarget

        control.addTarget(self, action:#selector(onUIControlEvent(_:event:)), for:UIControlEvents.allTouchEvents)
    }

    /// De-register UIControl for touches to target.
    /// - Parameter control: The UIControl to de-register touches.
    func deregisterForUITouch(_ control : UIControl)
    {
        m_touchTargetByUIControl.removeValue(forKey: control)

        control.removeTarget(self, action:#selector(onUIControlEvent(_:event:)), for:UIControlEvents.allTouchEvents)
    }

    /// De-register all registered touches.
    func deregisterAll()
    {
        // Make a copy of the keys as the deregister will modify the underlying m_touchTargetByUIControl
        let controls = Array<UIControl>(m_touchTargetByUIControl.keys)

        for control in controls
        {
            deregisterForUITouch(control)
        }
    }

    /// Touch event handler.
    func onUIControlEvent(_ sender: UIControl, event : UIEvent)
    {
        guard let emitNotifier = notifier, let touches = event.allTouches else { return }

        for touch : UITouch in touches where touch.phase == UITouchPhase.ended
        {
            let touchTarget = m_touchTargetByUIControl[sender]

            emitNotifier.notify(item: AppEvent(appEventType: .eUIControlTouch((sender, touchTarget!))))
        }
    }
}
