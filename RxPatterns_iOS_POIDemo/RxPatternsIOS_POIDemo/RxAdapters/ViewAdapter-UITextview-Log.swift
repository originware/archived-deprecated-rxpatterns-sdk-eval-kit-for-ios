//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Log_UITextView: An adapter to present textual logs from AppEvents.
///

open class ViewAdapter_Log_UITextView<ItemType>: ARxObserver<ItemType>
{
    /// Notifier used for monitoring internal notifications.
    let monitorNotifier:      RxNotifier_NotifyConsumers<eAppOpEventType>

    /// The UITextView to present to.
    fileprivate let m_textView: UITextView

    /// The text formatter.
    fileprivate var m_logFormatter: AppStyle_LogFormatter

    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// Initialise with name tag, target UiTextView and text formatter.
    init(tag: String, textView : UITextView, logFormatter : AppStyle_LogFormatter)
    {
        self.m_textView = textView
        self.m_logFormatter = logFormatter

        // Create monitor notifier.
        self.monitorNotifier = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: tag + "/monitorNotifier")

        // The observer runs in the UIThread.
        super.init(tag : tag)
    }

    /// Notify the UITextView adapter of an inroute appEvent.
    public override final func notify(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatch(async: { [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    fileprivate func onAppEvent(_ appEvent: AppEvent)
    {
        /// Set the log text in the UITextView associated with this Adapter.
        /// - Parameter attributedText: The log text.
        func setText(_ attributedText : NSAttributedString)
        {
            m_textView.attributedText = attributedText
            m_textView.scrollRangeToVisible(NSMakeRange(attributedText.length, 0))
        }

        switch appEvent.appEventType
        {
            case .eLocationChange(let locationChange):
                let simulatedNote = appEvent.isSimulatedEvent ? " [Simulated]" : ""

                setText(m_logFormatter.formatLogLineEntry(appEvent, locationChange.description + simulatedNote, AppStyle.color(.eColor_LogEventText)))

            case .eNetworkReachabilityChange(let reachabilityChange):
                setText(m_logFormatter.formatLogLineEntry(appEvent, reachabilityChange.description, AppStyle.color(.eColor_LogEventText)))

            case .ePOILocateResult(let poilocateResult):
                monitorNotifier.notify(item: eAppOpEventType.eFromPOILocator_To_LogAdaptor)

                setText(m_logFormatter.formatPOILocateResultForLog(appEvent, poilocateResult : poilocateResult))

            case .eOrientation:
                setText(m_logFormatter.formatLogLineEntry(appEvent, appEvent.description, AppStyle.color(.eColor_LogEventText)))

            case .eUITextFieldChange(let uiTextField):
                setText(m_logFormatter.formatLogLineEntry(appEvent, "Search Input Text: \(uiTextField.attributedText.string)", AppStyle.color(.eColor_LogEventText)))

            case .ePOIKeywordMatchSet:
                monitorNotifier.notify(item: eAppOpEventType.eFromPOIKeywordMatcher_To_LogAdapter)

                setText(m_logFormatter.formatPOIKeywordMatchLogText(appEvent))

            case .eUIControlTouch(let (_, touchTarget)):

                setText(m_logFormatter.formatLogLineEntry(appEvent, "Tap: \(touchTarget.rawValue)", AppStyle.color(.eColor_LogEventText)))

                if touchTarget == .eTouch_clearLogButton
                {
                    setText(NSAttributedString(string: ""))
                    m_logFormatter.clearLog()
                }

            case .eSystemFailure:

                setText(m_logFormatter.formatPOIKeywordMatchLogText(appEvent))

            default:
                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}
