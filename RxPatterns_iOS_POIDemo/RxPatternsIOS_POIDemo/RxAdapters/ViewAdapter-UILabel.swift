//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Label: An adapter to present labels triggered from AppEvent notifications.
///

open class ViewAdapter_Label<ItemType> : ARxObserver<ItemType>
{
    /// The label to present to.
    fileprivate let m_label: UILabel

    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// Initialise with name tage and target UILabel.
    public init(tag: String, label: UILabel)
    {
        self.m_label = label

        super.init(tag: tag)
    }

    /// Notify the UILabel adapter of an inroute appEvent.
    public override final func notify(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatch(async: { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.onAppEvent(appEvent)
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    fileprivate func onAppEvent(_ appEvent: AppEvent)
    {
        func updateLabelTitle(_ text: NSAttributedString)
        {
            guard text != m_label.attributedText else { return }

            m_label.attributedText = text
        }

        switch appEvent.appEventType
        {
            case .eLocationChange(let locationChange):
                updateLabelTitle(AppStyle.formatUILabel(forLocationChange: locationChange, isSimulated: appEvent.isSimulatedEvent))

            case .eNetworkReachabilityChange(let reachability):
                updateLabelTitle(AppStyle.formatUILabel(forReachabilityChange: reachability))

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}
