//
// Created by Terry Stillone (http://www.originware.com) on 19/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

@available(iOS 7, *)

fileprivate let MapRegionSpan : CLLocationDistance = 1000
fileprivate let MapPitch : CGFloat = 30

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_MapKit: An adapter to MapKit Annotations. Triggers off AppEvents for POI locations and the map center button touch event.
///

open class ViewAdapter_MapKit<ItemType> : ARxObserver<ItemType>
{
    /// Notifier used for monitoring internal notifications.
    let monitorNotifier:      RxNotifier_NotifyConsumers<eAppOpEventType>

    /// The MapKit view to present to.
    fileprivate var m_mapView : MKMapView

    /// The last set location.
    fileprivate var lastKnownLocation : CLLocation? = nil

    /// The UI EvalQueue to perform UI.
    fileprivate let UIQueue             = RxSDK.evalQueue.UIThreadQueue

    /// Indicates whether the MapKit region has been set.
    fileprivate var m_hasSetMapRegion   = false

    /// Get the map center.
    fileprivate var mapCenter : CLLocationCoordinate2D? {

        let userCoord = m_mapView.userLocation.coordinate

        func validUserCoord(_ coordinate : CLLocationCoordinate2D) -> CLLocationCoordinate2D?
        {
            guard (coordinate.latitude != 0) && (coordinate.longitude != 0) else { return nil }

            return coordinate
        }

        return validUserCoord(userCoord) ?? lastKnownLocation?.coordinate ?? AppConstant.SimulatedPosition.coordinate
    }

    /// Initialise with the Map View.
    init(tag: String, mapView : MKMapView)
    {
        self.m_mapView = mapView

        // Create monitor notifier.
        self.monitorNotifier = RxNotifier_NotifyConsumers<eAppOpEventType>(tag: tag + "/monitorNotifier")

        super.init(tag : tag)
    }

    /// Notify the Map View adapter of an inroute appEvent.
    public override final func notify(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatch(async: { [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    public func setLocation(location: CLLocation)
    {
        guard !m_hasSetMapRegion else { return }

        // Set the map region span.
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, MapRegionSpan, MapRegionSpan)
        let fitRegion = m_mapView.regionThatFits(region)

        m_mapView.setCenter(location.coordinate, animated:false)
        m_mapView.setRegion(fitRegion, animated:true)

        // Set the camera pitch.
        let mapCamera = MKMapCamera()

        mapCamera.centerCoordinate = m_mapView.camera.centerCoordinate
        mapCamera.pitch = MapPitch
        mapCamera.altitude = m_mapView.camera.altitude
        mapCamera.heading = m_mapView.camera.heading

        m_mapView.camera = mapCamera

        // Core Location Bug fix: make sure the region is set.
        m_mapView.setRegion(fitRegion, animated:true)

        lastKnownLocation = location

        m_hasSetMapRegion = true
    }

    fileprivate func onAppEvent(_ appEvent: AppEvent)
    {
        switch appEvent.appEventType
        {
            case .ePOILocateResult(let poiLocateResult):        // Handle POI Locate events.

                monitorNotifier.notify(item: .eFromPOILocator_To_MapAdaptor)

                for pois in poiLocateResult.poiByPOIKeyword.values
                {
                    for poi in pois
                    {
                        addPOIToMap(poi)
                    }
                }

            case .eUIControlTouch(let (_, touchTarget)):        // Handle touch events.

                switch touchTarget
                {
                    case .eTouch_centerMapButton:               // Trigger on Map Center button touch.

                        if let currentMapCenter = mapCenter
                        {
                            m_mapView.setCenter(currentMapCenter, animated: true)
                        }

                    case .eTouch_clearPOIKeywordMatchesButton:  // Trigger on clear Located POIs button touch.

                        m_mapView.removeAnnotations(m_mapView.annotations)

                    case .eTouch_clearSearchButton:             // Trigger on search clear button, not handled here.
                        break

                    case .eTouch_clearLogButton:                // Trigger on clear log button, not handled here.
                        break
                }

            case .ePOIKeywordMatchSet:

                // ignore
                break

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }

    /// Add a POI annotation to the map.
    /// - Parameter poi: The POI details to be used to create a POI annotation.
    fileprivate func addPOIToMap(_ poi : POI)
    {
        for annotation in m_mapView.annotations
        {
            if let flagAnnotation = annotation as? FlagAnnotation
            {
                if poi == flagAnnotation.poi
                {
                    return
                }
            }
        }

        let flagAnnotation = FlagAnnotation(poi:poi)

        m_mapView.addAnnotation(flagAnnotation)
    }
}
