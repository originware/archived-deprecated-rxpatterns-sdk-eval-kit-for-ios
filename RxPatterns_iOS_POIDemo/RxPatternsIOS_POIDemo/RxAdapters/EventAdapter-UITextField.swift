//
// Created by Terry Stillone (http://www.originware.com) on 25/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// EventAdapter_keyboard: Adapt from UITextField NSNotifications to AppEvent RxNotifications.
///

class EventAdapter_UITextField: RxSource<AppEvent>
{
    /// The UITextField being observed for NSNotifications
    let textField: UITextField

    /// The NSNotifications observer.
    fileprivate var m_NSNotificationCenterObserver : AnyObject? = nil

    fileprivate var notificationCenter : NotificationCenter
    {
        return NotificationCenter.default
    }

    /// Initialise with RxObject tag and UITextField.
    /// - Parameter tag: The RxObject tag.
    /// - Parameter textField: The UItextField to observe.
    init(tag: String, textField : UITextField)
    {
        self.textField = textField

        super.init(tag : tag + "/EventAdapter_UITextField", subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.registerForNSNotifications(evalNode.getSharedOutNotifier(forSync: false))

                    case eRxEvalStateChange.eEvalEnd:

                        self.deregisterForNSNotifications()

                    default:
                        break
                }
            }
        }
    }

    /// Clear the UITextField
    public func clear()
    {
        textField.text = ""
    }

    /// Register for NSNotifications.
    /// - Parameter notifier: The notifier to notify UITextField changes to.
    fileprivate func registerForNSNotifications(_ notifier : ARxNotifier<AppEvent>)
    {
        let textField = self.textField

        m_NSNotificationCenterObserver = notificationCenter.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: nil, using: { [weak weakNotifier = notifier] _ in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notify(item: AppEvent(appEventType: eAppEventType.eUITextFieldChange(eDeviceEvent_TextField.eText_Change(textField, textField.attributedText!))))
            }
        })
    }

    // De-register for NSNotifications.
    fileprivate func deregisterForNSNotifications()
    {
        guard m_NSNotificationCenterObserver != nil else { return }

        notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
        m_NSNotificationCenterObserver = nil
    }
}
