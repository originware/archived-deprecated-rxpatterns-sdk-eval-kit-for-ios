// AppViewController.swift
// Rxpatterns iOS POI Demo Project
//
//
// Created by Terry Stillone (http://www.originware.com) on 25/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//
// The Main App View Controller.
//

import UIKit
import CoreLocation
import MapKit
import RxPatternsSDK
import RxPatternsLib

/// Main App View Controller.
class AppViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIViewControllerTransitioningDelegate
{
    struct Constant
    {
        static let MapRegionSpan : CLLocationDistance = 1000
        static let MapPitch : CGFloat = 30
        static let POITypePlaceHolderColumns = 3
        static let LocationSimulationTimeout : RxDuration = 5
        static let TransitionDuration = 0.5
    }

    /// State of the views.
    struct State
    {
        /// Indicates whether the MapKit region has been set.
        var hasSetMapRegion = false

        /// Formatter and Writer and to the App log.
        fileprivate var logWriter = AppStyle_LogFormatter()
    }

    // Outlets

    @IBOutlet weak var locationStatus : UILabel!
    @IBOutlet weak var reachabilityStatus: UILabel!

    @IBOutlet weak var clearSearchTextButton : UIButton!
    @IBOutlet weak var appEventClearButton : UIButton!

    @IBOutlet weak var clearPOIMatchesButton : UIButton!
    @IBOutlet weak var centreMapButton : UIButton!

    @IBOutlet weak var searchTextField : UITextField!
    @IBOutlet weak var searchResultsTextView : UITextView!
    @IBOutlet weak var appEventLogTextView : UITextView!

    @IBOutlet weak var selectorSliderContainer: UIView!
    @IBOutlet weak var appEventLogContainer : UIView!
    @IBOutlet weak var poiSearchContainer : UIView!
    @IBOutlet weak var poiMatchContainer : UIView!
    @IBOutlet weak var appTitle : UIView!
    @IBOutlet weak var searchTextFieldContainer : UIView!

    @IBOutlet weak var appOperationViewContainer: UIView!
    @IBOutlet weak var appOperationView: AppOperationView!
    @IBOutlet weak var appOperationScrollView: TappableUIScrollView!
    
    @IBOutlet weak var mapViewContainer : UIView!
    @IBOutlet weak var networkStatusPanel : UIView!
    @IBOutlet weak var locationStatusPanel : UIView!

    @IBOutlet weak var mapView : MKMapView!

    @IBOutlet weak var selectorSlider : UISlider!

    @IBOutlet weak var foundPOITypesCollectionView : UICollectionView!

    @IBOutlet weak var BackButton: UIButton!

    @IBOutlet weak var InternalOperationLabel: UILabel!
    @IBOutlet weak var ShowMapLabel: UILabel!

    /// Transitions
    var transition                          = ExpandAnimatedTransition()

    /// State
    var m_state                             = State()

    /// AppOperationScopeController reference for notifying presentation operational state changes.
    var operationalModel: OperationalModel? = nil

    override func viewDidLoad()
    {
        super.viewDidLoad()

        configureControls()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(onSliderTapped(_:)))

        self.selectorSlider.addGestureRecognizer(gestureRecognizer)

        // Hide views not to be shown on first load.
        appOperationViewContainer.isHidden = true
        mapViewContainer.isHidden = true

        let inputDir = AppRxDir.PresentationInput()
        let viewDir = AppRxDir.View()

        // Add buttons to the Rx Directory.
        inputDir.addTap(withTag: "EventAdapter_UITouch", atURI: AppRxDir.PresentationInput.buttonTapURI)
        inputDir.add(button: clearSearchTextButton, touchTarget: eDeviceEvent_Touch.eTouch_clearSearchButton)
        inputDir.add(button: appEventClearButton, touchTarget: eDeviceEvent_Touch.eTouch_clearLogButton)
        inputDir.add(button: clearPOIMatchesButton, touchTarget: eDeviceEvent_Touch.eTouch_clearPOIKeywordMatchesButton)
        inputDir.add(button: centreMapButton, touchTarget: eDeviceEvent_Touch.eTouch_centerMapButton)

        // Add UITexFields to the Rx Directory.
        inputDir.add(textField: searchTextField, withTag: "search", atURI: AppRxDir.PresentationInput.poiSearchTextURI)

        // Add UILabels to the Rx Directory.
        viewDir.add(label: locationStatus, withTag: "ViewAdapter_Label/location", atURI: AppRxDir.View.locationLabelURI)
        viewDir.add(label: reachabilityStatus, withTag: "ViewAdapter_Label/reachability", atURI: AppRxDir.View.reachabilityLabelURI)

        // Add UITextViews to the Rx Directory.
        viewDir.addKeyword(textView: searchResultsTextView, withTag: "ViewAdapter_KeywordResults_UITextView", atURI: AppRxDir.View.keywordMatchResultURI, formatter: m_state.logWriter)
        viewDir.addLog(textView: appEventLogTextView, withTag: "ViewAdapter_Log_UITextView", atURI: AppRxDir.View.logUITextViewURI, formatter: m_state.logWriter)

        //print(viewDir.keywordMatchResult)

        // Add map to the Rx Directory.
        viewDir.add(mapView: mapView, withTag: "MapKitAnnotationAdapter", atURI: AppRxDir.View.mapURI)

        // Add UICollectionViews to the Rx Directory.
        viewDir.add(collectionView: foundPOITypesCollectionView, withTag: "ViewAdapter_UICollection", atURI: AppRxDir.View.matchedKeywordsUICollectionURI)

        // Add Dialogs to the Rx Directory.
        viewDir.addConfirm(withTag: "ViewAdapter_Confirmation", atURI: AppRxDir.View.confirmURI)

        appOperationView.operationalModel = operationalModel

        operationalModel?.issueRequest(OperationalModel.StartupScope.eRequest.eBeginPresentationScope(inputDir, viewDir))
    }

    override func viewWillAppear(_ animated : Bool)
    {
        super.viewWillAppear(animated)

        #if DEBUG
            RxMonReports.save()
        #endif

        styleControls()
        styleSlider()

        operationalModel?.issueRequest(OperationalModel.PresentationScope.eRequest.eBeginOperationalScope)
    }

    override func viewDidAppear(_ animated : Bool)
    {
        super.viewDidAppear(animated)

        appOperationViewContainer.isHidden = false
        mapViewContainer.isHidden = false
    }

    override func viewWillDisappear(_ animated : Bool)
    {
        // Shutdown processing in views
        appOperationView.shutdown()

        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)

        #if DEBUG
            RxMonReports.reportInstanceStats()
            RxMonReports.restore()
        #endif

        operationalModel?.issueRequest(OperationalModel.ApplicationScope.eRequest.eEndScope(nil))
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    func onSelectorSliderEvent(_ slider : UISlider)
    {
        let sectorSize : CGFloat = 1.0 / 3.0
        let value : CGFloat = CGFloat(slider.value)

        let archPosValue: CGFloat = ValueClamper.clampToNegPos1((value - (0.5 - sectorSize)) / sectorSize)
        let poiMatchesPosValue: CGFloat = ValueClamper.clampToNegPos1((value - 0.5) / sectorSize)
        let mapViewPosValue: CGFloat = ValueClamper.clampToNegPos1((value - (0.5 + sectorSize)) / sectorSize)

        func calcAlpha(_ value : CGFloat) -> CGFloat
        {
            let pi : CGFloat = 3.14159
            let paraValue = 1.0 - (value * value)
            let sinValue = sin(paraValue * pi / 8.0) / 0.3

            return ValueClamper.clampToZeroOne(sinValue)
        }

        let mapViewAlpha : CGFloat = calcAlpha(mapViewPosValue) * fabs(poiMatchesPosValue)
        let poiMatchesAlpha : CGFloat = calcAlpha(poiMatchesPosValue) * fabs(archPosValue)
        let archAlpha : CGFloat = calcAlpha(archPosValue)

        self.appOperationViewContainer.alpha = ValueClamper.clampToThreshold(archAlpha)
        self.poiMatchContainer.alpha = ValueClamper.clampToThreshold(poiMatchesAlpha)
        self.mapViewContainer.alpha = ValueClamper.clampToThreshold(mapViewAlpha)
    }

    func onSliderTapped(_ gestureRecognizer : UIGestureRecognizer)
    {
        func sliderNotch(_ index : Int) -> Float
        {
            let sectorSize : Float  = 1.0 / 3.0
            let index1 : Float = Float(index) - 1.0

            return 0.5 + index1 * sectorSize
        }

        func isInSliderNotch(_ index : Int, value : Float, delta : Float) -> Bool
        {
            let notchCentre : Float = sliderNotch(index)
            let notchStart : Float  = notchCentre - delta / 2
            let notchEnd : Float  = notchCentre + delta / 2

            return (notchStart <= value) && (value <= notchEnd)
        }

        func clampToSliderNotch(_ value : Float, delta : Float) -> Float
        {
            for i in 0..<3
            {
                if isInSliderNotch(i, value: value, delta: delta)
                {
                    return sliderNotch(i)
                }
            }

            return -1.0
        }

        if let slider = gestureRecognizer.view as? UISlider
        {
            if slider.isHighlighted { return }

            let point = gestureRecognizer.location(in: slider)
            let factor = Float(point.x / slider.bounds.size.width)
            let value = slider.minimumValue + factor * (slider.maximumValue - slider.minimumValue)
            let notchValue = clampToSliderNotch(value, delta: 0.2)

            if (notchValue > 0.0)
            {
                slider.setValue(notchValue, animated:true)
                onSelectorSliderEvent(slider)
            }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension AppViewController // Configuration and Styling.
{
    fileprivate func configureControls()
    {
        // Configure map MKMapView.
        self.mapView.delegate = self

        // Configure the App Operation View UIScrollView.
        self.appOperationScrollView.containerView = self.appOperationView
        self.appOperationScrollView.minimumZoomScale = 0.95
        self.appOperationScrollView.maximumZoomScale = 4.0
        self.appOperationScrollView.zoomScale = 0.95

        // Configure the Located-POIs UICollectionView.
        let collectionViewLayout = self.foundPOITypesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout

        collectionViewLayout.sectionInset = UIEdgeInsetsMake(7, 0, 0, 0)
        collectionViewLayout.minimumLineSpacing = 2
        collectionViewLayout.minimumInteritemSpacing = 2
        collectionViewLayout.scrollDirection = UICollectionViewScrollDirection.vertical

        self.searchResultsTextView.contentInset = UIEdgeInsetsMake(2, 0, 2, 0)
    }

    fileprivate func styleControls()
    {
        AppStyle.styleBorder(inTitleViewContainer: self.appTitle)
        AppStyle.styleBorder(inTitleViewContainer: self.selectorSliderContainer)
        AppStyle.styleBorder(inTitleViewContainer: self.appEventLogContainer)
        AppStyle.styleBorder(inView: self.appEventLogContainer)

        AppStyle.styleBorder(inView: self.networkStatusPanel)
        AppStyle.styleBorder(inView: self.locationStatusPanel)
        AppStyle.styleBorder(inView: self.searchTextFieldContainer)

        AppStyle.styleBorder(inView: self.searchTextField)
        AppStyle.styleBorder(inView: self.poiSearchContainer)
        AppStyle.styleBorder(inView: self.foundPOITypesCollectionView)

        AppStyle.styleSearchResults(self.searchResultsTextView)
    }

    fileprivate func styleSlider()
    {
        let maxWidth = max(self.view.bounds.width, self.view.bounds.height)
        let trackImage = AppStyle.createSliderThumbImage(CGSize(width: maxWidth, height: self.selectorSlider.bounds.size.height), trackHeight:6)
        let stretchableTrackImage =  trackImage.stretchableImage(withLeftCapWidth: 9, topCapHeight:0)

        selectorSlider.addTarget(self, action:#selector(onSelectorSliderEvent(_:)), for:UIControlEvents.valueChanged)
        selectorSlider.setMinimumTrackImage(stretchableTrackImage, for:UIControlState())
        selectorSlider.setMaximumTrackImage(stretchableTrackImage, for:UIControlState())

        onSelectorSliderEvent(self.selectorSlider)
    }
}

extension AppViewController // MKMapViewDelegate
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    @objc(mapView:didUpdateUserLocation:) func mapView(_ mapView: MKMapView, didUpdate: MKUserLocation)
    {
        let userCoordinate = didUpdate.location!.coordinate

        // Set the map region on the first location determination.

        guard (userCoordinate.latitude != 0.0) && (userCoordinate.longitude != 0.0) && !m_state.hasSetMapRegion else { return }

        // First set a dummy location so that we can animate to the users location.
        let dummyCoordinate = CLLocationCoordinate2DMake(0, 0)

        mapView.setCenter(dummyCoordinate, animated:false)

        // Set the map region span.
        let region = MKCoordinateRegionMakeWithDistance(userCoordinate, Constant.MapRegionSpan, Constant.MapRegionSpan)
        let fitRegion = mapView.regionThatFits(region)

        mapView.setRegion(fitRegion, animated:true)

        // Set the camera pitch.
        let mapCamera = MKMapCamera()

        mapCamera.centerCoordinate = mapView.camera.centerCoordinate
        mapCamera.pitch = Constant.MapPitch
        mapCamera.altitude = mapView.camera.altitude
        mapCamera.heading = mapView.camera.heading

        mapView.camera = mapCamera

        m_state.hasSetMapRegion = true
    }

    @objc(mapView:viewForAnnotation:) public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        // Add a map annotation to mark the POI on the map.
        guard let flagAnnotation = annotation as? FlagAnnotation else { return nil }

        if let flagAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: flagAnnotation.poi.poiKeyword) as? FlagAnnotationView
        {
            return flagAnnotationView
        }

        let flagAnnotationView = FlagAnnotationView(annotation:annotation, reuseIdentifier:flagAnnotation.poi.poiKeyword)

        flagAnnotationView.poi = flagAnnotation.poi

        return flagAnnotationView
    }

    public func setMapViewLocation(_ mapView: MKMapView, location: CLLocation)
    {
        guard !m_state.hasSetMapRegion else { return }

        // Set the map region span.
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, Constant.MapRegionSpan, Constant.MapRegionSpan)
        let fitRegion = mapView.regionThatFits(region)

        mapView.setCenter(location.coordinate, animated:false)
        mapView.setRegion(fitRegion, animated:true)

        // Set the camera pitch.
        let mapCamera = MKMapCamera()

        mapCamera.centerCoordinate = mapView.camera.centerCoordinate
        mapCamera.pitch = Constant.MapPitch
        mapCamera.altitude = mapView.camera.altitude
        mapCamera.heading = mapView.camera.heading

        mapView.camera = mapCamera

        // Core Location Bug fix: make sure the region is set.
        mapView.setRegion(fitRegion, animated:true)

        m_state.hasSetMapRegion = true
    }
}

extension AppViewController // UIViewControllerTransitioningDelegate
{
    func animationController(
            presented: UIViewController,
            presentingController: UIViewController,
            sourceController: UIViewController) ->
            UIViewControllerAnimatedTransitioning?
    {
        transition.isPresenting = true
        transition.duration = Constant.TransitionDuration

        return transition
    }

    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        transition.isPresenting = false
        transition.duration = Constant.TransitionDuration * 3

        return transition
    }
}
