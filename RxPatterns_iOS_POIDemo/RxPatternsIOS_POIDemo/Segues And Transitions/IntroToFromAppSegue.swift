//
//  Created by Terry Stillone on 25/07/2015.
//  Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class IntroToFromAppSegue: UIStoryboardSegue
{
    var srcViewController : UIViewController { return self.source }
    var destViewController : UIViewController { return self.destination }

    override func perform()
    {
        guard let identifier = self.identifier else { fatalError("Cannot transition to view controller") }

        switch identifier
        {
            case "idIntroToApp":

                let appViewController = self.destViewController as! AppViewController
                let introViewController = self.srcViewController as! AppIntroScreenViewController
                let transition = appViewController.transition

                transition.originFrame = introViewController.TapToContinueButton.frame

                appViewController.transitioningDelegate = appViewController
                appViewController.operationalModel = introViewController.appOperationScopeController

                self.srcViewController.present(self.destViewController, animated: true, completion: nil)

            case "idAppToIntro":

                self.srcViewController.dismiss(animated: true, completion: nil)

            default:
                fatalError("Cannot transition to view controller")
        }
    }
}
