//
//  Created by Terry Stillone on 16/06/15.
//  Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import UIKit

@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Create the OperationalModel, which initiates the StartUp scope and pass it onto the root View Controller.
        if let navigationController = window?.rootViewController as? UINavigationController
        {
            // Create the run context that controls operational tracing (for debugging).
            let runContext = AppRunContext()
            let operationalModel = OperationalModel(runContext)

            for viewController in navigationController.viewControllers
            {
                switch viewController
                {
                    case let appIntroViewController as AppIntroScreenViewController:
                        appIntroViewController.appOperationScopeController = operationalModel

                    case let appViewController as AppViewController:
                        appViewController.operationalModel = operationalModel

                    default: break
                }
            }

            operationalModel.issueRequest(OperationalModel.StartupScope.eRequest.eBeginScope(operationalModel.context, { (reply) in

            }))
        }
        else
        {
            fatalError("Cannot get Root View Controller")
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {

    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {

    }

    func applicationWillTerminate(_ application: UIApplication)
    {
    }
}
