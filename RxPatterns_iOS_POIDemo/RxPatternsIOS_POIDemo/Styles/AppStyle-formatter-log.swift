//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

open class AppStyle_Log
{
    open class func getAppEventLogParagraphStyle() -> NSMutableParagraphStyle
    {
        let terminator = NSTextTab.columnTerminators(for: NSLocale.current)
        let tabs = [
                NSTextTab(textAlignment: NSTextAlignment.left, location:30, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.left, location:100, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.left, location:250, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.left, location:450, options:[NSTabColumnTerminatorsAttributeName : terminator])
        ]

        let paraStyle = NSMutableParagraphStyle()

        paraStyle.tabStops = tabs
        paraStyle.headIndent = 0
        paraStyle.alignment = NSTextAlignment.left

        return paraStyle
    }
}

class AppStyle_LogFormatter
{
    fileprivate let TimeFormat = "HH:mm:ss"
    fileprivate let Separator = "\t"
    fileprivate static let BackgroundColor1 = UIColor.clear
    fileprivate static let BackgroundColor2 = UIColor.blue.withAlphaComponent(0.1)

    fileprivate let m_datestampFormatter = DateFormatter()
    fileprivate var m_logLineNumber : Int  = 1
    fileprivate let m_paragraphStyle = AppStyle_Log.getAppEventLogParagraphStyle()
    fileprivate var m_currentLogText = NSMutableAttributedString()
    fileprivate var m_currentLineBackgroundColor = AppStyle_LogFormatter.BackgroundColor2

    fileprivate var m_currentLogLine = NSMutableAttributedString()


    var currentLogText : NSMutableAttributedString
    {
        return m_currentLogText
    }

    init()
    {
        self.m_datestampFormatter.dateFormat = TimeFormat
    }

    func nextLine()
    {
        m_currentLineBackgroundColor = (m_logLineNumber % 2 == 1) ? AppStyle_LogFormatter.BackgroundColor1 : AppStyle_LogFormatter.BackgroundColor2

        m_currentLogText.append(m_currentLogLine)

        m_currentLogLine = NSMutableAttributedString()

        m_logLineNumber += 1
    }

    func clearLog()
    {
        m_currentLogText = NSMutableAttributedString()
        m_logLineNumber = 1
    }

    func appendLogText(_ text: String, textColor : UIColor = UIColor.black)
    {
        let atributedText = NSMutableAttributedString(string: text, attributes:[NSForegroundColorAttributeName : textColor, NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.append(atributedText)
    }

    func appendTimestampText(_ timestamp : Date)
    {
        let formattedimestamp = m_datestampFormatter.string(from: timestamp) + Separator
        let textColor = AppStyle.color(.eColor_LogTimeStamp)
        let atributedText = NSMutableAttributedString(string:formattedimestamp, attributes:[NSForegroundColorAttributeName : textColor, NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.append(atributedText)
    }

    func appendMarkerText(_ showMarker : Bool, isPlayedEvent: Bool)
    {
        let markerText = showMarker ? "  \u{25C6}\(Separator)" : Separator
        let markerColor = isPlayedEvent ? AppStyle.color(.eColor_statusTextColor) : AppStyle.color(.eColor_errorTextColor)
        let attributes = showMarker ? [NSForegroundColorAttributeName : markerColor] : [NSBackgroundColorAttributeName : m_currentLineBackgroundColor]
        let atributedText = NSMutableAttributedString(string:markerText, attributes:attributes)

        m_currentLogLine.append(atributedText)
    }

    func appendLineEnd()
    {
        let atributedText = NSMutableAttributedString(string:"\n", attributes:[NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.append(atributedText)
    }

    func formatLogLineEntry(_ appEvent : AppEvent, _ text : String, _ textColor : UIColor) -> NSAttributedString
    {
        let showMarker = appEvent.isSimulatedEvent
        let timestamp = appEvent.timestamp

        appendMarkerText(showMarker, isPlayedEvent : appEvent.isSimulatedEvent)
        appendTimestampText(timestamp as Date)
        appendLogText(text, textColor : textColor)
        appendLineEnd()

        nextLine()

        return currentLogText
    }
}

extension AppStyle_LogFormatter
{
    func formatPOILocateResultForLog(_ appEvent : AppEvent, poilocateResult : POILocateEvent_Reply) -> NSAttributedString
    {
        return formatLogLineEntry(appEvent, poilocateResult.description, AppStyle.color(.eColor_LogHighLightedEventText))
    }

    func formatPOIKeywordMatchLogText(_ appEvent : AppEvent) -> NSAttributedString
    {
        return formatLogLineEntry(appEvent, appEvent.description, AppStyle.color(.eColor_LogEventText))
    }
}
