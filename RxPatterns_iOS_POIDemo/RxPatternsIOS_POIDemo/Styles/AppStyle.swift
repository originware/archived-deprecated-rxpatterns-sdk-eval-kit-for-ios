//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import QuartzCore

public enum eColorElement : Int
{
    case eColor_borderColor
    case eColor_buttonColor
    case eColor_recordColor
    case eColor_playColor

    case eColor_sliderTrackColor
    case eColor_sliderNotchColor

    case eColor_Error
    case eColor_errorTextColor
    case eColor_statusTextColor

    case eColor_LogTimeStamp
    case eColor_LogEventText
    case eColor_LogHighLightedEventText

    case eColor_POISearchResultText
    case eColor_POIHighlightBackground
    case eColor_POIText

    case eColor_disabled
    case eColor_enabled

    case eColor_Available
    case eColor_NotAvailable
}

public enum eFontElement : Int
{
    case eFont_POIResultsText
}

open class AppStyle
{
    fileprivate static let m_color: [UIColor] = [

        UIColor(hue:0.0, saturation: 0.0, brightness: 0.5, alpha: 0.5),
        UIColor(hue:1.0, saturation: 0.0, brightness: 1.0, alpha: 1.0),
        UIColor(hue:1.0, saturation: 1.0, brightness: 0.9, alpha: 1.0),
        UIColor(hue:0.66, saturation: 1.0, brightness: 0.5, alpha: 1.0),

        UIColor(white: 0.7, alpha: 0.6),
        UIColor(white:0.6, alpha: 0.7),

        UIColor.red,
        UIColor.red,
        UIColor.blue,

        UIColor.gray,
        UIColor.orange,
        UIColor.purple,

        UIColor(red:0.5, green: 0.5, blue: 0.45, alpha: 1.0),
        UIColor(red:0.8, green: 0.8, blue: 1.0, alpha: 0.1),
        UIColor(red:0.2, green: 0.9, blue: 0.2, alpha: 1.0),

        UIColor.lightGray,
        UIColor(red:0.4, green: 0.8, blue: 0.5, alpha: 1.0),

        UIColor(red:0.2, green: 0.2, blue: 0.8, alpha: 1.0),
        UIColor(red:0.8, green: 0.2, blue: 0.2, alpha: 1.0),
    ]

    open static func color(_ item : eColorElement) -> UIColor
    {
        return m_color[item.rawValue]
    }

    open static func font(_ item : eFontElement) -> UIFont
    {
        switch item
        {
            case .eFont_POIResultsText:

                return UIFont(name:"Helvetica", size:18.0)!

        }
    }

    class func styleSearchResults(_ inUITextView: UITextView)
    {
        // Round all corners.
        inUITextView.layer.cornerRadius = 10.0
        inUITextView.layer.borderWidth = 1.0
        inUITextView.layer.borderColor = color(.eColor_borderColor).cgColor

        inUITextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10)
        inUITextView.scrollRangeToVisible(NSRange(location: 0, length: 0))
    }

    class func styleBorder(inView: UIView)
    {
        var borderColor : UIColor = color(.eColor_borderColor)

        // Round all corners.
        inView.layer.cornerRadius = 10.0
        inView.layer.borderWidth = 1.0
        inView.layer.borderColor = borderColor.cgColor

        if let button = inView as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inTextView: UIView)
    {
        var borderColor : UIColor = UIColor.white

        // Round all corners.
        inTextView.layer.cornerRadius = 10.0
        inTextView.layer.borderWidth = 1.0
        inTextView.layer.borderColor = borderColor.cgColor

        if let button = inTextView as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inButtonView: UIView)
    {
        var borderColor : UIColor = UIColor.white

        // Round all corners.
        inButtonView.layer.cornerRadius = 10.0
        inButtonView.layer.borderWidth = 1.0
        inButtonView.layer.borderColor = borderColor.cgColor

        if let button = inButtonView as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inTitleViewContainer: UIView)
    {
        let borderColor : UIColor = color(.eColor_borderColor)

        inTitleViewContainer.layer.borderWidth = 1.0
        inTitleViewContainer.layer.borderColor = borderColor.cgColor
    }

    class func createSliderThumbImage(_ sliderSize : CGSize, trackHeight : CGFloat) -> UIImage
    {
        let sectorSize : CGFloat = 1.0 / 3.0
        let sliderWidth : CGFloat = sliderSize.width
        let sectorNotchWidth : CGFloat = sliderSize.width * 0.1
        let thumbWidth : CGFloat = sliderSize.height
        let trackCentreYPos : CGFloat = (sliderSize.height - trackHeight) / 2.0
        let notchColour : UIColor = AppStyle.color(eColorElement.eColor_sliderNotchColor)
        let trackColour : UIColor = AppStyle.color(eColorElement.eColor_sliderTrackColor)

        UIGraphicsBeginImageContext(sliderSize)

        let context : CGContext = UIGraphicsGetCurrentContext()!

        // Draw the track background.
        context.setFillColor(trackColour.cgColor)
        context.fill(CGRect(x: thumbWidth / 2, y: trackCentreYPos, width: sliderSize.width - thumbWidth, height: trackHeight))

        // Draw the notches in the track.
        context.setFillColor(notchColour.cgColor)

        context.fill(CGRect(x: thumbWidth / 2 + sliderWidth * (0.5 - sectorSize) - sectorNotchWidth / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))
        context.fill(CGRect(x: (sliderWidth - sectorNotchWidth) / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))
        context.fill(CGRect(x: -thumbWidth / 2 + sliderWidth * (0.5 + sectorSize) - sectorNotchWidth / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image!
    }

    class func getPOIKeywordsPlaceHolderParagraphStyle(_ columnCount : Int) -> NSMutableParagraphStyle
    {
        let entryLength = 150
        let paraStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle

        let terms = NSTextTab.columnTerminators(for: NSLocale.current)
        var tabs = [NSTextTab]()

        for i in 1..<columnCount
        {
            let options = [NSTabColumnTerminatorsAttributeName: terms]
            let tab = NSTextTab(textAlignment:NSTextAlignment.left, location:CGFloat(entryLength * i), options:options)

            tabs.append(tab)
        }

        paraStyle.tabStops = tabs
        paraStyle.headIndent = 0
        paraStyle.alignment = NSTextAlignment.left

        return paraStyle
    }

    class func getFormattedPOIKeywordsPlaceHolder(_ columnCount : Int) -> NSAttributedString
    {
        let text                = NSMutableAttributedString()
        var currentColumnNumber = 0

        // Add POI Types with four columns.
        for poiKeyword in AppSettings.allPOIkeywords!
        {
            let lineText = poiKeyword
            let nextColumnNumber = currentColumnNumber + 1
            let terminator = (nextColumnNumber % columnCount) == 0 ? "\n" : "\t"

            currentColumnNumber = nextColumnNumber
            text.append(NSAttributedString(string: lineText + terminator))
        }

        let paraStyle = getPOIKeywordsPlaceHolderParagraphStyle(columnCount)

        text.addAttribute(NSParagraphStyleAttributeName, value:paraStyle, range:NSMakeRange(0, text.length))

        return text
    }
}
