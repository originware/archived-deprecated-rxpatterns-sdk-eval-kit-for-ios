//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppConstant: Constants related to the App operation.
///

struct AppConstant
{
    static let AppTag = "App"
    static let ReachabilityHostname = "google-public-dns-a.google.com"
    static let SimulatedPosition = CLLocation(latitude: 52.205, longitude: 0.119)
    static let LocationChangeUpdateDistance: CLLocationDistance = 100
    static let ConfigScenario = "production"

    static let InstanceMonTag = "InstanceMonTag"
}
