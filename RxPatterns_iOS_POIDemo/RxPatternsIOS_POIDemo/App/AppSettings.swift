//
// Created by Terry Stillone (http://www.originware.com) on 21/06/15.
// Copyright (c) 2016 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppSettings: The App Plist Settings.
///

class AppSettings
{
    /// The setting in the "Data.plist"
    class var settings : NSDictionary? {

        let path : String? = Bundle.main.path(forResource: "Data", ofType:"plist")

        return (path != nil) ? NSDictionary(contentsOfFile: path!) : nil
    }

    /// The POI Keywords stored in the Data.plist.
    class var allPOIkeywords : [String]? {

        if let poiKeywords = self.settings?.object(forKey: "POIKeywords") as? [String]
        {
            return poiKeywords
        }

        return nil
    }
}

