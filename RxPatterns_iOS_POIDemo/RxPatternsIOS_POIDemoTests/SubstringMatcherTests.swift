//
//  Created by Terry Stillone on 16/06/15.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import UIKit
import XCTest
@testable import RxPatternsIOS_POIDemo

class RxPatternsIOS_POIDemoTests: XCTestCase
{
    let minSubstringLength = 3

    func testSubStringMatcher_Basic()
    {
        let testString = "TestString"
        let testSubstring = "Test"

        let stringMatcher = SubstringMatcher(string: testString, minSubstringLength: minSubstringLength)
        let subStringMatcher = SubstringMatcher(string: testSubstring, minSubstringLength : minSubstringLength)

        let commonMatcher = stringMatcher.getCommonSubstrings(subStringMatcher)

        XCTAssertTrue(commonMatcher.poiKeyword == testString, "Expected poi keyword to be \(testString) but got: \(commonMatcher.poiKeyword)")
        XCTAssertTrue(commonMatcher.totalMatchLength == 4, "Expected totalMatchLength to be \(4) but got: \(commonMatcher.totalMatchLength)")
        XCTAssertTrue(commonMatcher.maxMatchLength == 4, "Expected maxMatchLength to be \(4) but got: \(commonMatcher.maxMatchLength)")
        XCTAssertTrue(commonMatcher.haveFullMatch == false, "Expected haveFullMatch to be false but got: \(commonMatcher.haveFullMatch)")
    }

    func testSubStringMatcher_ShortString()
    {
        let testString = "Cafe"
        let testSubstring = "Caf"

        let stringMatcher = SubstringMatcher(string: testString, minSubstringLength : minSubstringLength)
        let subStringMatcher = SubstringMatcher(string: testSubstring, minSubstringLength : minSubstringLength)

        let commonMatcher = stringMatcher.getCommonSubstrings(subStringMatcher)

        XCTAssertTrue(commonMatcher.poiKeyword == testString, "Expected poi keyword to be \(testString) but got: \(commonMatcher.poiKeyword)")
        XCTAssertTrue(commonMatcher.totalMatchLength == 3, "Expected totalMatchLength to be \(3) but got: \(commonMatcher.totalMatchLength)")
        XCTAssertTrue(commonMatcher.maxMatchLength == 3, "Expected maxMatchLength to be \(3) but got: \(commonMatcher.maxMatchLength)")
        XCTAssertTrue(commonMatcher.haveFullMatch == false, "Expected haveFullMatch to be false but got: \(commonMatcher.haveFullMatch)")
    }

    func testSubStringMatcher_FullMatch()
    {
        let testString = "TestString"

        let stringMatcher = SubstringMatcher(string: testString, minSubstringLength : minSubstringLength)
        let subStringMatcher = SubstringMatcher(string: testString, minSubstringLength : minSubstringLength)

        let commonMatcher = stringMatcher.getCommonSubstrings(subStringMatcher)

        XCTAssertTrue(commonMatcher.poiKeyword == testString, "Expected poi keyword to be \(testString) but got: \(commonMatcher.poiKeyword)")
        XCTAssertTrue(commonMatcher.totalMatchLength == 10, "Expected totalMatchLength to be \(10) but got: \(commonMatcher.totalMatchLength)")
        XCTAssertTrue(commonMatcher.maxMatchLength == 10, "Expected maxMatchLength to be \(10) but got: \(commonMatcher.maxMatchLength)")
        XCTAssertTrue(commonMatcher.maxMatchLength == 10, "Expected maxMatchLength to be \(10) but got: \(commonMatcher.maxMatchLength)")
        XCTAssertTrue(commonMatcher.haveFullMatch == true, "Expected haveFullMatch to be true but got: \(commonMatcher.haveFullMatch)")
    }
}
