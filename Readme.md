---
# README:    The RxPatterns SDK Evaluation Kit v1.2
Eval Kit Platforms | iOS
-------------------| ----
SDK Platforms      | iOS, OSX and Linux 
Eval Kit Platforms | iOS Simulator only
Language           | Swift 3.1.1
Kit Requirements:  | Xcode 8.3+
Origination        | [Originware.com](http://www.originware.com)


The RxPatterns SDK condenses and extends the classical Reactive Extensions concept, making it applicable to a wider domain set such as:

* iOS Apps (which require comprehensive asynchronous threading and processing).
* OSX Apps.
* IOT Applications.
* Device and instrument control and monitoring.

RxPatterns is concise, is the sense that it condenses the large classical Rx Library set into a small set of Design Patterns. It is extensive, in that it makes it easy to derive custom observables. It is pervasive
in that it provides a unified interaction model and so supports a vertical stack from embedded Linux devices, through to phone/tablet apps up to complex computer software.

### Evaluation Kit Purpose

Although RxPatterns supports the platforms of iOS, OSX and Linux this Eval Kit is targeted only for iOS.

The core is the SDK itself, provided as a Framework Library and the demonstration component is supplied as
a Source Tool Kit library called the **RxPatternsLib** and a Source iPad app.

The kit is intended to directly demonstrate the benefits of **RxPatterns** on software architecture and **RxDevice** technology for managing complex co-interactions. These are demonstrated through source examples.

An [RxPatterns White Paper](http://www.originware.com/doc/RxPatterns%20Whitepaper.pdf) is available and discusses the associated RxPatterns software design approach. The design method utilises the visualisation of the topology of interwoven event-notification channels which forms a fabric of RxNotification connections. The method opens the way to working on the adaptive and dynamic design level rather than on the static (Reactive Extension) expression level.

There are also product briefs available for **RxDevice**: the [RxDevice For Apps Product Brief](http://www.originware.com/doc/RxDevice%20For%20Apps%20Product%20Brief.pdf) and the [RxDevice For IOT Product Brief](http://www.originware.com/doc/RxDevice%20For%20IOT%20Product%20Brief.pdf) documents.

### Evaluation Kit Package

* Xcode Projects:	
	* The [RxPattermsLib](RxPatternsLib/RxPatternsLib.xcodeproj) Library.
	* The iPad [POI (Point Of Interest) Demonstration App](RxPatterns_iOS_POIDemo/RxPatternsIOS_POIDemo.xcodeproj)  full Swift app source (demonstrates the practical use of RxPatterns).
	* A Swift [Playground](RxPatterns_iOS_Playground/RxPatterns_iOS_Playground_Workspace.xcworkspace) for experimenting with the RxPatternsSDK and RxPatternsLib.

* Documentation:
	* The [RxPatternsSDK](Doc/RxPatternsSDK/index.html) and [RxPatternsLib](Doc/RxPatternsLib/index.html) Generated **API Documentation** as a HTML doc set.

* Notes:
	* [The Eval Kit Release Notes](ReleaseNotes.md).
	* [The Overview of the Eval Kit](EvalKitOverview.md) guide.


### Licensing

The source in this package is provided for under the Apache Version 2.0 license, see the [License.txt](License.txt) file.

### For More Information

See the Originware [RxPatterns Technology](http://www.originware.com/rxtechnology.html) and [Eval Kit](http://www.originware.com/rxpatevalkit.html) webpages. The [Blog](http://www.originware.com/blog) also has  ongoing articles.

Please direct comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com