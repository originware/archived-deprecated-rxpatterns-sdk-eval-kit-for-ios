 ---
# The [Originware](http://www.originware.com) RxPatterns SDK v0.9 Overview Of The Evaluation Kit.

## 1. Intent of The Guide.

This is a guide to the key features of RxPatterns as demonstrated in this Evaluation Kit. It is advisable to read RxPatterns the White Paper in conjunction with this document. See: The [RxPatterns White Paper](Doc/RxPatternsWhitepaper.pdf).

Note: It is assumed that the evaluator has a background knowledge of Reactive Extensions.

## 3. Core RxPattern Classes To Be Aware Of.

The Core classes from the RxPatternsLib to be familiar with are (note: source supplied in this eval kit):

* **RxObservableMap\<ItemInType, ItemOutType>**:  An Observable that maps input item notifications of type ItemInType and emits items of type ItemOutType.
* **RxObservable\<ItemType>**: An Observable which only operates on items of type ItemType, receiving item notifications of type ItemType and emitting item notifications of type ItemType.
* **RxSource\<ItemType>**: An Observable that only emits items of type ItemType.

The Core classes from the RxPatternsSDK Framework (source not supplied) to be familiar with are:

* **RxObservableNode<ItemInType, ItemOutType>** : The SDK base class for Observables. RxObservableMap and RxObservable directly and indirectly inherit from this class. It contains all the machinery to create an evaluation engine.
* **ARxObserver\<ItemType>**: An abstract base class for Observers that consume notifications of item type ItemType.
* **ARxNotifier\<ItemType>**: An abstract base class for notifiers that consume notifications of type ItemType and notify other notification consumers. This is the notification passing scheme.  
* **RxEvalNode\<ItemInType, ItemOutType>**: An evaluation node that corresponds to a particular RxObservable or RxObservableMap. Used to perform evaluation of the Observable.

See the RxPatternsSDK HTML API doc set for more information, see: [RxPatternsSDK API Doc](Doc/RxPatternsSDK/index.html)

## 4. RxPattern Concepts

### Representation of the Observable as the RxObservable class and its evaluation as the RxEvalNode class.

RxPatterns formalises the operation of Observables and Sources by employing the **RxRelay Pattern**. This scheme brings the developer closer to the operational level, aides comprehension of the evaluation process and speeds up development of custom observables. Below is an example of the implementation of the classic **takeWhile** Observable as implemented in the RxRelay pattern:

```
	
public class RxObservable<ItemType>
{
}

extension RxObservable
{
	public func takeWhile(predicate : (item : ItemType) -> Bool) -> RxObservable<ItemType>
  	{
		let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

			evalNode.itemDelegate = { (item: ItemType, notifier : ARxNotifier<ItemType>) in 
			      
	                if predicate(item)
	                {
	                    notifier.notifyItem(item)
	                }
	                else
	                {
	                    notifier.notifyCompleted()
	                }
        	}
        
        	return RxObservable<ItemType>(tag: "takeWhile", evalOp: evalOp).chainObservable(self)
       }
 }   
```

The **RxEvalNode** represents the evaluation mechanism of the Observable. The Observable has the capability to build its "evaluation behaviour" when requested to participate in subscription. This build process also includes the construction of evaluation resources. 

So in the example above, the **evalNode.itemDelegate** defines how the RxEvalNode processes item notifications and what it emits when it receives item notifications. The other notifications types such as completed notifications, take their default action which is to pass on their received notification.

### The RxEvalNode Evaluation States

The Eval Node has a number of states in its operational lifetime. These states include:

* Subscription Begin: 	The RxEvalNode has begun another subscription.
* Evaluation Begin:		The RxEvalNode is to begin evaluation (due to an initial subscription).
* Evaluation End:		The RxEvalNode is to stop evaluation (due to subscriptions disposing).
* Subscription End:		The RxEvalNode encountered a subscription disposal.

During subscription begin cycle, the RxEvalNode is constructed and given its behaviour by assign-age of delegates as defined in the corresponding Observable. The RxEvalNode is also linked by an ARxNotifier to the next consumer in the expression chain. The next consumer can be another RxEvalNode, Observer, or some ARxNotifier handling notification redirection. During evaluation, the Eval Node receives notifications by the invocation of its delegates. In the example above the evalNode.itemDelegate will be called for each item notification. The delegate is also passed a notifier parameter which it uses to emit its result to the next consumer.

The RxPatterns SDK employs RxEvalQueues (dispatch queues) for synchronisation rather than using traditional locks. This simplifies the implementation of Observables. In the above example, the **evalNode.itemDelegate** is run in a separate serial dispatch queue and thus all operations with RxEvalNode delegates are ordered and synchronised (with other operations being performed during evaluation).

Here is the implementation of the takeUntil Observable which gives a better view of synchronisation handling:

```
extension RxObservable
{
	public func takeUntil<OtherItemType>(stream : ARxProducer<OtherItemType>) -> RxObservable<ItemOutType>
	{
		let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            var subscription : RxSubscription? = nil

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in
                
                switch stateChange
                {
                case eRxEvalStateChange.eEvalBegin:

                    // Subscribe to the other stream.
                    // Note: errors on the other stream are ignored.
                    let streamNotifier = RxNotifier_NotifyDelegates<OtherItemType>(tag: self.tag)
                    
                    streamNotifier.onItem = { (item: OtherItemType) in

                        // When an item is notified on the other stream, 
                        //     complete the in-line subscription.
                        evalNode.evalQueue.dispatchSync({notifier.notifyCompleted()})
                    }
                    
                    subscription = stream.subscribe(streamNotifier)

                case eRxEvalStateChange.eEvalEnd:

                    // Unsubscribe to the other stream.
                    subscription?.unsubscribe()
			 subscription = nil
			 
                default:
                    // do nothing.
                    break
                }
            }
        }
        
        return RxObservable<ItemType>(tag: "takeUntil", evalOp: evalOp).chainObservable(self)
	}
}
```

Above, the **evalNode.itemDelegate** is not defined, so it takes its default behaviour which is to just pass the item onto the next consumer. The **evalNode.stateChangeDelegate**   handles subscription and unsubscription of the given stream. Note the use of the **evalNode.evalQueue.dispatchSync** to dispatch the notifier.notifyCompleted() in the dispatch queue of the current inline RxEvalNode. This ensures that a completed notification will be issued, in order and not interfere with other notifications going through the whole expression evaluation engine.

### The RxObservableMap class for mapping.

The **RxObservableMap** class provides the capability to map incoming item notifications of one type to another out item type. Here is an implementation of the **map** **RxObservableMap** (sometimes called **select** observable).

```
public RxObservableMap<ItemInType, ItemOutType>
{	}

extension RxObservableMap
{
public func map<TargetItemType>(mapAction : (ItemOutType) -> TargetItemType?) -> RxObservableMap<ItemOutType, TargetItemType>
{
	let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

		evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

			if let mappedItem = mapAction(item)
			{
				notifier.notifyItem(mappedItem)
			}
		}
	}

	return RxObservableMap<ItemOutType, TargetItemType>(tag: "map", evalOp: evalOp).chainObservable(self)
}
```    
Above, the **evalNode.itemDelegate** maps items from **ItemOutType** to the **TargetItemType**. If the map function returns nil, then no item is emitted.

In the method, **self** denotes the previous Observable and in our map operation we are chaining the new **map** observable to the previous Observable with an in-coming item type of **ItemInType** and out-going item type **ItemOutType**. This infers that our **map** receives **ItemOutType** and emits **TargetItemType**.

The **chainObservable(self)** statement performs the chaining (composition) of the previous Observable to our map Observable.

## 5. The RxPatterns
**RxPatterns** follows a design pattern approach to **Reactive Extensions** by synthesising a core set of of Reactive Extension Patterns. The total pattern set is small (less than ten). The set is concise and expressive in the breadth of cases that it handles.

The **RxSource**  RxPattern set includes: 

 * The **Synchronous Generator** **Pattern** (**syncGenPat**) generator source :  Generates item notifications from a given item generator. Items are generated as fast as possible

* The **Asynchronous Generator Pattern** (**asyncGenPat**) generator source: Generates item notifications at particular times from a given (timeOffset, item) tuple generator.
* The **Redirection Pattern** source (**redirectPat**): A source that takes its notifications from a given notifier.
* The **Tick Pattern** source (**tickGenPat**) : A source that generates notifications from a given item generator which is triggered by an external notifier. Hence the generator defines the items emitted and the notifier defines when they are issued.

The **RxObservable/RxObservableMap** RxPattern set includes:

* **RxRelay Pattern**, the basis for all RxPattern Observables.
* **Extract Pattern** Observable  (**extractPat**):  extract received notifications for external use.
* **Gate Streams Pattern** Observable  (**gateStreamsPat**):  Gates a collection of given streams to produce a single new stream.
* **Gate Streams Map Pattern** Observable  (**gateStreamsMapPat**) :  A variant of **gateStreamsMapPat** which also maps the result type.

The use of these patterns are demonstrated in the source code of the RxPatternsLib. **Gating** for example is used in the classic sequenceEqual, zip, merge, join and a few others. The **Extraction**  pattern is used in the classic **doOnNotify** (also also called the **do** or **forEach** ). 

Please view the provided RxPatternsLib source. 

## 6. RxPatterns Technology Benefits.

### Development Benefits
1. RxPatterns makes it easy to write your own custom observables, observers and sources. There are common situations where a single custom Observable is more advantageous than composing a long expression chain.

	* Look through the implementation of the classic Rx Library observables, sources and observers in the RxPatternsLib source. These observables and sources can be used directly or taken as source-code examples for your own implementations. (See: [rx-observables.swift](RxPatternsLib/RxPatternsLib/Expr/rx-observables.swift) and [rx-sources.swift](RxPatternsLib/RxPatternsLib/Expr/rx-sources.swift))
	* Look at the custom adaptors in the App demo source code: (See: [DeviceAdapter-Location.swift](Sample%20Code/RxPatternsIOS_POIDemo/RxPatternsIOS_POIDemo/RxAdapters/DeviceAdapter-Location.swift)) 

2. RxPattern patterns simplifies complex Sources and Observables by defining simple evaluation patterns which can be incorporated into custom Observables.

	These Source RxPattern patterns include:

	* syncGenPat:  	Generate synchronous item notifications.
	* asyncGenPat:	Generate timed (asynchronous) item notifications.
	* gateStreamsPat: Gate a number of streams together.
	* redirectPat: Redirect the notifications from a given ARxNotifier to the inline stream.
	* tickGenPat: Generate notification by a given generator which is triggered by an external notifier (called a tick source).

	Observable RxPattern patterns include:

	* extractPat: Extract notifications to a given ARxNotifier.
	* relayPat: For completeness only this represents the system of using RxEvalNode delegates to define Observable behaviour.
	* gateStreamsMapPat: The RxObservableMap version of the source gateStreamsPat. Gates a number of given streams with the inline stream.

	See the RxPatternsLib Observables and Sources for examples in their use.

3. The SDK supports monitoring of evaluation operation.

	This is formally called the RxMon system. It instruments the evaluation engine and allows the development of custom monitors that can inform of operation. 

	The SDK comes with a specific monitor that will print operational events to the console, such as instance creation, RxEvalNode state notifications, item notifications, completed notification and finally instance destruction. To use it simply compose **monitorAll(traceTitle : String)** on the observable. See the 
	 [Playground Project](RxPatterns_iOS_Playground/RxPatterns_iOS_Playground_Workspace.xcworkspace) for examples.
 
4. Support for timers, time-windows, synchronisation, enumerable-s is included. 

	See the Support for the RxPatternsSDK:[RxPatternsSDK API Support Doc](Doc/RxPatternsSDK/Support.html)

### Design Benefits

RxPatterns provides mechanisms to design and visualise the topology of notifications (events) flowing through an App. The Demo App demonstrates this in its **Operation View**. This view is accessed by moving the slider to the left **Operation View** position. The view visualises the observables and observers in the App, their interconnections and the flow of individual notifications through the app. 

This design process provides a decomposition method from app macro-behaviour to micro-behaviours. The visualisation scheme makes it easy to conceptualise app operation within the development group and convey operation in simple terms to non-technical parties.

The decomposition process separates boundary Observables and Observers from functional (working) Observables. The demo app employs this scheme by defining: 

* **InRxAdapter**: An Observable that converts data from some particular OS Service to notifications.
* **OutRxAdapter**: An Observer that converts notifications to some particular OS Service.
* **DeviceRxAdapter**: An InRxAdapter that converts device data (such as location services) to notifications.
* **ViewRxAdapter**: An OutRxAdapter that presents notifications to the presentation system (such as to a UILabel)

These adapters are small, reusable, testable and can be added to any project as required. 


### Optimisation Benefits.

The building of RxPattern observable behaviour is dynamic and can optimise runtime behaviour at build time. (See the [interval source](RxPatternsLib/RxPatternsLib/Expr/rx-sources.swift) observable)

### App Management Benefits.

Cold Configuration using the RxDirectory store allows the app to be reconfigured for different operational scenarios, such production, development and test. Each observable/observer is given run reference URI in the RxDirectory which then can be mapped to the actual URI of the scenario. When the app runs, it extracts from the RxDirectory the mapped object to use in the configured scenario (See the [App View Controller](RxPatterns_iOS_POIDemo/RxPatternsIOS_POIDemo/AppViewController.swift))

The coming versions of RxPatterns will include the active hot switching (i.e. the redirecting of multiple ARxProducers to sources on request).

## 6. Contact Details:

Please direct questions, comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com
